

import { IframeMessager } from './messager';
import 'babel-polyfill';


const { iframeResizer } = require('iframe-resizer');
declare var PROD : boolean;
declare var TEST : boolean;

{
	console.log(PROD, TEST);
	const url = `http://turavins${TEST ?'-test.dwpr' : '-dev.dwpr'}.ru/iframe-integration/travel/insurance`;
	const turavinsIframe = document.createElement('iframe');
	const iframeMessager = new IframeMessager('turavins-iframe');	
	const container = document.getElementById('turavins-iframe');
	// container.style.maxWidth = '576px';
	container.style.margin = '0 auto';
	turavinsIframe.src = url;
	turavinsIframe.scrolling = 'no';
	turavinsIframe.name = 'turavins-iframe';
	turavinsIframe.style.width = '1210px';
	turavinsIframe.style.paddingTop = '20px';
	turavinsIframe.style.border = 'none';
	turavinsIframe.style.margin = '0 auto';
	turavinsIframe.style.display = 'block';

	/**
	 * @desc добавляем прелоадер
	 */

	const turavinsPreloader = document.createElement('div');
	const turavinsPreloaderImage = document.createElement('img');

	turavinsPreloader.style.position = 'fixed';
	turavinsPreloader.style.top = '50%';
	turavinsPreloader.style.left = '50%';
	turavinsPreloader.style.transform = 'translate(-50%, -50%)';
	turavinsPreloader.style.display = 'flex';
	turavinsPreloader.style.alignItems = 'center';
	turavinsPreloader.style.justifyContent = 'center';
	turavinsPreloader.style.background = '#fff';

	turavinsPreloaderImage.style.width = '50px';
	turavinsPreloaderImage.style.maxWidth = '100%';
	turavinsPreloaderImage.style.height = '50px';
	turavinsPreloaderImage.setAttribute('src', `http://turavins${TEST ? '-test.dwpr' : '-dev.dwpr'}.ru/iframe-integration/img/preloader.gif`);

	turavinsPreloader.appendChild(turavinsPreloaderImage);
	
	 /**
	  * конец инициализации прелоадера.
	  */
	
	container.appendChild(turavinsPreloader);
	container.appendChild(turavinsIframe);

	iframeMessager.on('TURAV_INS_LOADED', (payload) => {
		container.removeChild(turavinsPreloader);

		iframeResizer({
			sizeHeight: true,
			sizeWidth: false,
			warningTimeout: 15000,
			bodyBackground: '#fff'
			
		}, turavinsIframe);
		iframeMessager.send('TURAV_INS_RESIZE', { width: innerWidth });
		iframeMessager.send('TURAV_INS_INIT', {
			url: window.location.href,
			partnerGuid: (window as any).partnerGuid
		});
		window.addEventListener('resize', (event) => iframeMessager.send('TURAV_INS_RESIZE', { width: innerWidth }) );
	});
	iframeMessager.on('TURAV_INS_REDIRECT', (event) => {
		window.location.replace(event.payload.url);
	});
	iframeMessager.on('TURAV_INS_LOCAL_REDIRECT', (event) => {
		window.location.replace(event.payload.url);
	});
	iframeMessager.on('TURAV_INS_SCROLL_TO', (event) => {
		window.scrollTo(window.scrollX, event.top);
	});
}
