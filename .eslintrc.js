module.exports = {
    "extends": [
      "google",
      "typescript",
      "typescript/react"
    ],
    "plugins": ["filenames"],
    "rules": {
      "typescript/type-annotation-spacing": [
          "error",
          {
              "before": true,
              "after": true
          }
      ],
      "typescript/interface-name-prefix": "always",
      "typescript/explicit-member-accessibility": 0,
      "no-useless-escape": 0,
      "semi": 0,
      "valid-jsdoc": 0,
      "react/react-in-jsx-scope": 0,
      "react/no-string-refs": 0,
      "typescript/member-ordering": 0 
    }
  }