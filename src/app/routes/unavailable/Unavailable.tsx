import './Unavailable.style.scss';


export const Unavailable = () => (
    <div className='unavailable'>
        <h1 className='unavailable__message'>503 Обновление данных</h1>
        <h1 className='unavailable__message'>Временное прекращение работы сервиса в связи с обновлением базы данных</h1>
    </div>
);