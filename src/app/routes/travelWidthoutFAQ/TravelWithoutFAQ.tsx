import { Redirect, Switch, Route } from "react-router";
import { Travel } from "../travel/Travel";
import { IAppProps } from "../Root";
import * as moment from 'moment';
import { bindActionCreators } from "redux";
import { settingsActions } from "../../store/actions/settings.actions";
import { connect } from "react-redux";


class TravelWithoutFAQ extends React.Component<any> {
    public state = {
		size: 'screen-md',
		isLoaded: parent == self
	}


    public componentWillMount() {
		if (parent == self) {
			frameManager.parentUrl = window.location.href;
			this.resize(innerWidth);
			
        }
        this.props.settingsActions.getProjectInfo();

		moment.locale('ru');
		moment.updateLocale('ru', {
			monthsShort: [
				"янв", "февр", "март", "апр", "мая", "июня", "июля", "авг", "сент", "окт", "нояб", "дек"
			]
		});
	}

    public componentDidMount() {
		
		if (parent != self) {
			parent.postMessage({ type: 'TURAV_INS_LOADED' }, '*');
			window.addEventListener('message', this.onMessage);
		} else {
			window.addEventListener('resize', this.onResize);
		}
	}


	public onMessage = (event) => {
		const { data } = event;
		if (typeof data == 'object') {
			switch (data.type) {
				case 'TURAV_INS_RESIZE':
					this.resize(data.payload.width);
					break;
				case 'TURAV_INS_INIT':
					frameManager.parentUrl = data.payload.url;
					frameManager.partnerGuid = data.payload.partnerGuid;
					this.props.settingsActions.getProjectInfo();
					this.setState({ isLoaded: true });
					break;
			}
		}
	}	

	public componentWillUnmount() {
		if (parent != self) window.removeEventListener('message', this.onMessage);
		else window.removeEventListener('resize', this.onResize);
	}

    public resize = (innerWidth) => {
		let size = 'screen-xs';
		// if (innerWidth > 500) size = 'screen-sm';
		// if (innerWidth >= 992) size = 'screen-md'
		// if (innerWidth >= 1366) size = 'screen-lg';
		// if (innerWidth >= 1900) size = 'screen-xlg';
		this.setState({ size });
	}

	public onResize = (event) => {
		this.resize(innerWidth);
	}

    render() {
        const { history, settings } = this.props;
		const { isLoaded } = this.state;
        if (!isLoaded && settings.isLoaded) return null;
		console.log(frameManager.url('travel'))
        return (
            <div className={this.state.size}>
                <Switch>
                    <Route component={Travel} path={frameManager.url('travel')} />
                    <Redirect to={frameManager.url('travel')} />
                </Switch>
            </div>       
        );
    }
} 

const mapStateToProps = (state: IAppProps, ownProps) => ({
	settings: state.settings
});

const mapDispatchToProps = (dispatch: any, ownProps) => ({
	settingsActions: bindActionCreators(settingsActions, dispatch)
});

export const TravelWithoutFAQContainer = connect(mapStateToProps, mapDispatchToProps)(TravelWithoutFAQ);