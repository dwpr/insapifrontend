import { DateInput } from '../../components/date-input/DateInput';
import { RangeDateInput } from '../../components/range-date-input/RangeDateInput';
import { RangeDatePicker } from '../../components/range-date-picker/RangeDatePicker';
import { InsuranceOption } from '../../components/insurance-option/InsuranceOption';
import { RangeInput } from '../../components/range-input/RangeInput';
import { DatePicker } from '../../components/datepicker/DatePicker';
import { Button } from '../../components/button/Button';
import { Validator } from '../../components/validator/Validator';
import { IControlEvent } from '../../lib/IControlEvent';
import { Field } from '../../components/field/Field';
import { Checkbox } from '../../components/checkbox/Checkbox';
import { Select } from '../../components/select/Select'
import { Radio } from '../../components/radio/Radio'
import { Tooltip } from './../../components/tooltip/Tooltip';
import { ExpandBtn } from './../../components/expand-btn/ExpandBtn';
import './Uikit.style.scss';
import { Price, CurrencyIcon } from '../../components/price/Price';
import { Switcher } from '../../components/switcher/Switcher';
import { Label } from '../../components/label/Label';
import * as moment from 'moment';
import { AgeField } from '../../components/age-field/AgeField';
import { RiskCheckbox } from '../travel/results/risk-checkbox/RiskCheckbox';
import { CustomRangeDatepicker } from '../../components/new-range-date-picker/CustomRangeDatepicker';
 


export class Uikit extends React.Component {

	state = {
		field1: '',
		field2: '',
		checkbox1: false,
		checkbox2: false,
		select1: 0,
		radio1: true,
		radio2: false,
		rangeInput1: 100,
		insuranceOptionCheckbox: false,
		datePickerValue: '2017-05-05',
		dateFrom: '',
		dateTo: '',
		dateInput: '',
		ageField: '',
		startDate: '',
		endDate: ''
	}

	onChange = (event: IControlEvent) => {
		const { name, value } = event;
		this.setState({ [name]: value });
	}

	// selectOptions = [
	// 	{ value: 0, label: 1, isDefault: true },
	// 	{ value: 1, label: 2 }
	// ]

	public get selectOptions() {
		const list = [];
		for (let i = 0; i < 1500; i++) list.push({ value: i, label: i });
		return list;
	}

	fastSelectOptions = [
		{ value: 1, label: 2 }
	]

	listOption = [100, 200, 300];

	sumOptions = [
		{
			value: '1',
			label: '35000'
		},
		{
			value: '2',
			label: '50000'
		},
		{
			value: '3',
			label: '100000'
		}
	]

	currencyOption = [
		{
			value: '1',
			label: '€'
		},
		{
			value: '2',
			label: '$'
		}
	];

	listCountry = [
		{
			id: 0,
			isDefault: true,
			label: "Шенген",
			value: "191"
		},
		{
			id: 0,
			isDefault: true,
			label: "Весь мир",
			value: "35"
		},
		{
			id: 0,
			isDefault: false,
			label: "Абхазия",
			value: "1"
		},
		{
			id: 0,
			isDefault: false,
			label: "Австралия",
			value: "2"
		},
		{
			id: 0,
			isDefault: false,
			label: "Австрия",
			value: "3"
		},
		{
			id: 0,
			isDefault: false,
			label: "Азия",
			value: "288"
		},

	]

	public coverageSumRender = (coverageSum) => {
		const currency = " €";
		return Utils.text.numberWithSpaces(coverageSum) + currency;
	}

	public mainOptions = [
		{	
			description: "Возмещение расходов, связанных с задержкой регулярного авиарейса с территории временн",
			id: 10,
			listCoverageSum: [ 500, 700 ],
			name: "Задержка рейса",
			typeId: 1
		},
		{	
			description: "Возмещение расходов, связанных с задержкой регулярного авиарейса с территории временн",
			id: 8,
			listCoverageSum: [ 300, 500 ],
			name: "Утрата багажа",
			typeId: 1
		},
		{	
			description: "Возмещение расходов, связанных с задержкой регулярного авиарейса с территории временн",
			id: 5,
			listCoverageSum: [ 1000, 2000 ],
			name: "Несчастный случай",
			typeId: 1
		},
		{	
			description: "Возмещение расходов, связанных с задержкой регулярного авиарейса с территории временн",
			id: 3,
			listCoverageSum: [ 10000, 20000 ],
			name: "Гражданская ответственность",
			typeId: 1
		},
		{	
			description: "Возмещение расходов, связанных с задержкой регулярного авиарейса с территории временн",
			id: 9,
			listCoverageSum: [ 500, 700 ],
			name: "Юридическая помощь",
			typeId: 1
		},
		{	
			description: "Возмещение расходов, связанных с задержкой регулярного авиарейса с территории временн",
			id: 4,
			listCoverageSum: [ 500, 700 ],
			name: "Отмена поездки",
			typeId: 1
		}
	]

	public advancedOptions = [
		{	
			description: "Возмещение расходов, связанных с задержкой регулярного авиарейса с территории временн",
			id: 12,
			listCoverageSum: [ 0 ],
			name: "Помощь при беременности",
			typeId: 1
		},
		{	
			description: "Возмещение расходов, связанных с задержкой регулярного авиарейса с территории временн",
			id: 11,
			listCoverageSum: [ 0 ],
			name: "Помощь при алкогольном опьянении",
			typeId: 1
		},
		{	
			description: "Возмещение расходов, связанных с задержкой регулярного авиарейса с территории временн",
			id: 7,
			listCoverageSum: [ 0 ],
			name: "Профессиональный спорт",
			typeId: 1
		},
		{	
			description: "Возмещение расходов, связанных с задержкой регулярного авиарейса с территории временн",
			id: 13,
			listCoverageSum: [ 0 ],
			name: "Хронические заболевания",
			typeId: 1
		},
		{	
			description: "Возмещение расходов, связанных с задержкой регулярного авиарейса с территории временн",
			id: 2,
			listCoverageSum: [ 0 ],
			name: "Активный отдых",
			typeId: 1
		},
		{	
			description: "Возмещение расходов, связанных с задержкой регулярного авиарейса с территории временн",
			id: 6,
			listCoverageSum: [ 0 ],
			name: "Любительский спорт",
			typeId: 1
		},
		{	
			description: "Возмещение расходов, связанных с задержкой регулярного авиарейса с территории временн",
			id: 14,
			listCoverageSum: [ 0 ],
			name: "Экстремальный спорт",
			typeId: 1
		}
	]


	render() {
		return (
			<div className='uikit' >
				<div className="container">
					<div className='uikit__group'>
						<div className='uikit__group-title'>Main colors</div>
						<div className='uikit__colors'>
							<div className='uikit__color uikit__color--blue'></div>
							<div className='uikit__color uikit__color--yellow'></div>
							<div className='uikit__color uikit__color--black'></div>
							<div className='uikit__color uikit__color--dark'></div>
							<div className='uikit__color uikit__color--grey'></div>
							<div className='uikit__color uikit__color--lightgrey'></div>
							<div className='uikit__color uikit__color--error'></div>
						</div>
					</div>
					<div className='uikit__group'>
						<div className='uikit__group-title'>Typography</div>
						<div className='uikit__group-subtitle'>Segoe UI (regular, semibold)</div>
						<h1 className=''>H1</h1>
						<h2 className=''>H2</h2>
						<h3 className=''>H3</h3>
					</div>
					<div className='uikit__group-title'>Buttons</div>
					<div className='uikit__group-flex'>
						<div className='uikit__group'>
							<div className='uikit__group-subtitle'>CTA</div>
							<Button disabled={true}>Disabled</Button>
							<Button>Default</Button>
						</div>
						<div className='uikit__group'>
							<div className='uikit__group-subtitle'>CTA + price</div>
							<div className='uikit__price-btn'>
								<Price currency={CurrencyIcon.RUB}>1435.98</Price>
								<Button>Default</Button>
							</div>
						</div>
						<div className='uikit__group'>
							<div className='uikit__group-subtitle'>Small</div>
							<Button size='small' disabled={true}>Disabled</Button>
							<Button size='small'>Default</Button>
						</div>
					</div>										
					<div className='uikit__group-flex'>
						<div className='uikit__group'>
							<div className='uikit__group-subtitle'>Checkbox</div>
							<div className='uikit__row'>
								<Checkbox id='checkbox1' name='checkbox1' value={this.state.checkbox1} onChange={this.onChange} label={'Default'}></Checkbox>
							</div>
							<div className='uikit__row'>
								<Checkbox id='checkbox2' name='checkbox2' value={this.state.checkbox2} onChange={this.onChange} label={'Default with help'}></Checkbox>
								<Tooltip tooltip='meow' />
							</div>
							<div className='uikit__row'>
								<Checkbox id='checkbox3' name='checkbox3' value={true} onChange={this.onChange} disabled={true} label={'Active'}></Checkbox>
							</div>
							<div className='uikit__row'>
								<Checkbox id='checkbox3' name='checkbox3' value={false} onChange={this.onChange} disabled={true} label={'Error'}></Checkbox>
							</div>
							<div className='uikit__row'>
								<Checkbox id='checkbox3' name='checkbox3' value={false} onChange={this.onChange} disabled={true} label={'Disabled'}></Checkbox>
							</div>
						</div>
						<div className='uikit__group'>
							<div className='uikit__group-subtitle'>Arrow slider</div>
						</div>
					</div>
					<div className='uikit__group'>
						<div className='uikit__group-title'>Links</div>
						<a href="#" className='link'>default link</a>
					</div>
					<div className='uikit__group'>
						<div className='uikit__group-title'>Links</div>
						<ExpandBtn text='Подробнее' isExpanded={true} onClick={() => {}} />
					</div>
					<div className='uikit__group'>
						<div className='uikit__group-title'>Tooltips</div>
						<div className='uikit__group-flex-jcc'>
							<Tooltip tooltip='meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow' tooltipPosition='top-left'/>
							<Tooltip tooltip='warning danger warning danger warning danger warning danger warning danger warning danger warning danger' className='warning-tooltip' tooltipIcon={<i className='icon-warning'></i>}  tooltipPosition='top'/>
						</div>						
					</div>
					<div className='uikit__group'>
						<div className='uikit__group-title'>Selectors</div>
						<div className='uikit__group'>
							<Switcher id='coverage-sum' name='insuranceSumId' value={this.sumOptions[0].value} options={this.sumOptions} onChange={e => { }} optionRenderer={this.coverageSumRender} />
						</div>
						<div className='uikit__group'>
							<Switcher id='coverage-sum' name='insuranceSumId' value={this.sumOptions[0].value} options={this.currencyOption} onChange={e => { }} className='switcher--currency' />
						</div>					
					</div>
					<div className='uikit__group'>
						<div className='uikit__group-title'>Input</div>
						<Field id='field1' name='field1' value={this.state.field1} label="Default" />
					</div>
					<div className='uikit__group'>
						<Select fastSelectOptions={[{ value: 191, label: 'Шенген' }, { value: 81, label: 'Кипр' }, { value: 162, label: 'США' }]}
							clearable={true}
							onFastOptionClick={e => { }}
							className='travel-insurance-form__country-select'
							multi={true}
							id='destination-country-select'
							name='listCountryId'
							value={this.listCountry[0].id}
							onChange={() => { }}
							options={this.listCountry}
							searchable={true}
							onBlur={() => { }}
							onFocus={() => { }}
							onInputChange={() => { }}
						/>
					</div>
					<div className='uikit__group'>
						<RangeDateInput className='travel-insurance-form__start-date'
							label={<Label caption={'Даты поездки'} />}
							from={{ name: 'startDate', value: moment().format('YYYY-MM-DD') }}
							to={{ name: 'endDate', value: moment().add(5, 'days').format('YYYY-MM-DD') }}
							onChange={() => { }}
							startDate={moment().format('YYYY-MM-DD')}
							endDate={moment().add(5, 'days').format('YYYY-MM-DD')}
							minDate={moment().format('YYYY-MM-DD')} />
					</div>
					<div className='uikit__group'>
						<InsuranceOption
							className='travel-results-add-option'
							id={`insurance-option`}
							index={1}
							optionProps={{
								name: 'isSelected',
								id: `isSelected`,
								value: true
							}}
							label='Задержка рейса'
							inputProps={{
								name: 'selectedSum',
								value: '500'
							}}
							onChange={() => { }}
							listOption={['300', '500']}
							currency={CurrencyIcon['EUR']}
							tooltip='ТултипТултип'
						/>
					</div>
					<div className='uikit__group mb-74'>
						<div className='uikit__group-title'>Main options with icons</div>
						<div className='uikit__options'>
							{this.mainOptions.map(item => (
								<RiskCheckbox 	riskModel= { {
									description: item.description,
									id: item.id,
									listCoverageSum: item.listCoverageSum,		
									name: item.name,
									typeId: item.typeId
								}} 
								currency=" €"
								isActive={false} />
							))}						
						</div>
					</div>
					<div className='uikit__group'>
						<div className='uikit__group-title'>Advanced options with icons</div>
						<div className='uikit__options'>
							{this.advancedOptions.map(item => (
								<RiskCheckbox 	riskModel= { {
									description: item.description,
									id: item.id,
									listCoverageSum: item.listCoverageSum,		
									name: item.name,
									typeId: item.typeId
								}} 
								currency=" €"
								isActive={false} />
							))}						
						</div>
					</div>
					<div className='uikit__group'>
						<AgeField id='ageField' name='ageField' value={this.state.ageField} onChange={e => this.setState({ ageField: e.value })} />
					</div>
					<div>
						<CustomRangeDatepicker onChange={this.onChange} 
							minDate={moment().add(1, 'days').format('YYYY-MM-DD')} 
							maxDate={moment().add(364, 'days').format('YYYY-MM-DD')}
							label={<Label caption={'Даты поездки'} tooltip='Даты выезда за пределы Российской Федерации и возвращения обратно' tooltipWidth='300' />}
							from={{ name: 'startDate', value: this.state.startDate }}
							to={{ name: 'endDate', value: this.state.endDate }}/>
					</div>
					<div style={{ height: '300px', width: '100%' }}></div>
				</div>
			</div>
		)
	}
}