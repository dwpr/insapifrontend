import { connect } from "react-redux";
import { image } from '../../app';
import { Link } from "react-router-dom";




export class Main extends React.Component<any> {




    public getListSection = () => {
        const { Travel, Realty } = this;
        return new Map([
            ['travel', <Travel />],
            ['realty', <Realty />]
        ])
    }



    public Travel = () => (
        <Link to={frameManager.url('travel')} key='travel'>
            <div className='main-page-section__content main-page-section__content--travel'>
                <img src={image('/icons/vzr-mobile.png')} alt="" width='35' className='main-page-section__img-mobile' />
                <img src={image('/icons/vzr.png')} alt="" width='36' className='main-page-section__img-desktop' />
                <div className='main-page-section__title'>Туристическая<br />страховка (ВЗР)</div>
            </div>
        </Link>
    )


    public Realty = () => (
        <Link to={frameManager.url('realty')} key='realty'>
            <div className='main-page-section__content main-page-section__content--realty'>
                <img src={image('/icons/realty-mobile.png')} alt="" width='50' className='main-page-section__img-mobile' />
                <img src={image('/icons/realty.png')} alt="" width='50' className='main-page-section__img-desktop' />
                <div className='main-page-section__title'>Страхование<br />квартиры</div>

            </div>
        </Link>
    )


    render() {
        const { settings } = this.props;
        const { isLoaded, params } = settings;
        const { getListSection } = this;

        if (!isLoaded) return null;

        const listSection = getListSection();

        const listPartnerLogo = [
            {
                name: 'gaide',
                widthMobile: 63,
                widthDesktop: 80
            },
            {
                name: 'rosgosstrah',
                widthMobile: 80,
                widthDesktop: 100
            },
            {
                name: 'absolut',
                widthMobile: 80,
                widthDesktop: 119
            },
            {
                name: 'soglasie',
                widthMobile: 80,
                widthDesktop: 103
            },
            {
                name: 'alfa',
                widthMobile: 80,
                widthDesktop: 100
            },
            {
                name: 'liberty',
                widthMobile: 63,
                widthDesktop: 93
            },
            {
                name: 'renessans',
                widthMobile: 80,
                widthDesktop: 100
            },
            {
                name: 'uralsib',
                widthMobile: 80,
                widthDesktop: 120
            },
            {
                name: 'insurion',
                widthMobile: 90,
                widthDesktop: 100
            },
            {
                name: 'ingosstrah',
                widthMobile: 90,
                widthDesktop: 110
            },
            {
                name: 'vtb',
                widthMobile: 90,
                widthDesktop: 100
            },
            {
                name: 'tinkoff',
                widthMobile: 90,
                widthDesktop: 100
            },
            {
                name: 'allianz',
                widthMobile: 80,
                widthDesktop: 100
            },
        ];

        return (
            <div className='main-page'>
                <h1 className='main-page__title'>Страхование от ведущих страховых компаний</h1>
                <div className='main-page__sections'>
                    {params.listPage.map((item, index) => {
                        return (
                            <div className='main-page-section' key={`page-section${index}`}>
                                { listSection.get(item.name) }
                            </div>
                    )})}
                </div>
                <div className='main-page-partners'>
                    {listPartnerLogo.map((item, index) => {
                        return (
                        <div className={`main-page-partners__partner main-page-partners__partner--${item.name}`} key={`partner${index}`}>
                            <img src={image(item.name + '.svg')} alt="" width={Utils.browser.isMobile() ? item.widthMobile : item.widthDesktop} />
                        </div>
                    )})}
                </div>
            </div >
        )
    }
}



const mapStateToProps = (state, ownProps) => ({
    settings: state.settings
});

const mapDispatchToProps = (dispatch, ownProps) => ({

});


export const MainContainer = connect(mapStateToProps, mapDispatchToProps)(Main);