//import './App.style.scss';
import { Switch, Redirect, Route, Link } from 'react-router-dom';
import { ISettingsState } from '../store/reducers/settings';
import { settingsActions, SettingsActions } from '../store/actions/settings.actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as moment from 'moment';

import { Travel } from './travel/Travel';
import { FAQ } from './faq/FAQ';


export interface IAppProps {
	/**
	 * Объект истории для роутера
	 * @type {*}
	 * @memberof IAppProps
	 */
	history: any;
	/**
	 * State of settings reducer
	 */
	settings: ISettingsState;
	/**
	 * 
	 */
    settingsActions: SettingsActions;
    
    location?: any;
}


class Root extends React.Component<IAppProps> {

    public state = {
		size: 'screen-md',
		isLoaded: parent == self
	}


    public componentWillMount() {
		if (parent == self) {
			frameManager.parentUrl = window.location.href;
			this.resize(innerWidth);
			
        }
        this.props.settingsActions.getProjectInfo();

		moment.locale('ru');
		moment.updateLocale('ru', {
			monthsShort: [
				"янв", "февр", "март", "апр", "мая", "июня", "июля", "авг", "сент", "окт", "нояб", "дек"
			]
		});
	}

    public componentDidMount() {
		
		if (parent != self) {
			parent.postMessage({ type: 'TURAV_INS_LOADED' }, '*');
			window.addEventListener('message', this.onMessage);
		} else {
			window.addEventListener('resize', this.onResize);
		}
	}


	public onMessage = (event) => {
		const { data } = event;
		if (typeof data == 'object') {
			switch (data.type) {
				case 'TURAV_INS_RESIZE':
					this.resize(data.payload.width);
					break;
				case 'TURAV_INS_INIT':
                    frameManager.parentUrl = data.payload.url;
                    frameManager.partnerGuid = data.payload.partnerGuid;
					this.props.settingsActions.getProjectInfo();
					this.setState({ isLoaded: true });
					break;
			}
		}
	}	

	public componentWillUnmount() {
		if (parent != self) window.removeEventListener('message', this.onMessage);
		else window.removeEventListener('resize', this.onResize);
	}

    public resize = (innerWidth) => {
		let size = 'screen-xs';
		// if (innerWidth > 500) size = 'screen-sm';
		// if (innerWidth >= 992) size = 'screen-md'
		// if (innerWidth >= 1366) size = 'screen-lg';
		// if (innerWidth >= 1900) size = 'screen-xlg';
		this.setState({ size });
	}

	public onResize = (event) => {
		this.resize(innerWidth);
	}


    render() {
        const { history, settings } = this.props;
		const { isLoaded } = this.state;
        if (!isLoaded && settings.isLoaded) return null;
        return (
            <div className={this.state.size} >
				<header className="header">
                    <div className="container header__wrap">
                        <a href='/' className="header__title">Страховой Партнер</a>
                        {this.props.location.pathname.indexOf('/travel') != -1 ? (
                            <Link className='header__questions-link' to='/faq'><i className='icon-support'/>У вас есть вопросы?</Link>
                        ) : null}
                    </div>							
                </header>
                
                <main className='main'>
                    {/* <Route component={Uikit} path={frameManager.url('uikit')} /> */}
                    
                    <div>
                        <Switch>
                            {/* <Route component={Travel} path={frameManager.url('travel')} /> */}                            
                            <Route component={Travel} path={frameManager.url('travel')} />
                            <Route component={FAQ} path='/faq' />
                            <Redirect to={frameManager.url('travel')} />                            
                        </Switch>
                        <div>
                            {/* <ErrorBlock />
                            <Switch>
                                <Route component={Travel} path={frameManager.url('travel')} />
                                <Route component={Realty} path={frameManager.url('realty')} />
                                <Route component={MainContainer} path={frameManager.url('iframe-landing')} />
                                <Route component={Unavailable} path={frameManager.url('unavailable')} />
                            </Switch> */}
                        </div>
                    </div>
                </main>
                
                {this.props.location.pathname.indexOf('/travel') != -1 ? (
                    <div className="service">
                        <div className="container">
                            <span className="service__text">Лучший сервис</span>
                            <ul className="service__list">
                                <li className="service__item">
                                	<img src={require('img/appreciate-your-time.svg')} alt="" className='service__best-servise'/>
                                    <span className="service__title">Ценим ваше время</span>
                                    <span className="service__dis">
                                        Экономьте свое время, оформляя тур и страховку
                                        в одном месте.
                                    </span>
                                </li>
                                <li className="service__item">
                                    <img src={require('img/looking-for-you.svg')} alt="" className='service__looking-for-you'/>
                                    <span className="service__title">Ищем для вас</span>
                                    <span className="service__dis">
                                        Наполняйте полис опциями, подходящими только вам.
                                    </span>
                                </li>
                                <li className="service__item">
                                    <img src={require('img/working-with-the-best.svg')} alt="" className='service__working-with-the-best'/>
                                    <span className="service__title">Работаем с лучшими</span>
                                    <span className="service__dis">
                                        Выбирайте среди ведущих
                                        страховых компаний.
                                    </span>
                                </li>
                            </ul>
                        </div>							
                    </div>
                ) : null}					
                <footer className="footer">
                    <div className="container">
                        <div>
                            <div className="footer__copyrights">
                                <span>© 2018 ООО «Страховой партнер».</span>  
                                <span>143500, Московская область, г. Истра, ул. Морозова, д. 1</span> 
                            </div>
                            <a href="mailto:support@stpartner.ru" className="footer__support-link">support@stpartner.ru</a>
                            <div className="footer__info">
                                Содержание сайта не является рекомендацией или офертой и носит информационно-справочный характер.
                                Условия <a href="#">пользовательского соглашения</a>
                            </div>
                        </div>
                        <div>
                            <span className="footer__developed-by">Разработано</span>
                            <a href="http://dwpr.ru/" target="_blank">
                                <img src={require('img/dwpr.png')} alt="" className='footer__company-logo'/>
                            </a>                            
                        </div>
                    </div>													
                </footer>
            </div>
        )
    }
}


const mapStateToProps = (state: IAppProps, ownProps) => ({
	settings: state.settings
});

const mapDispatchToProps = (dispatch: any, ownProps) => ({
	settingsActions: bindActionCreators(settingsActions, dispatch)
});

export const RootContainer = connect(mapStateToProps, mapDispatchToProps)(Root);