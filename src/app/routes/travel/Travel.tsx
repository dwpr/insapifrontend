import { ConnectedTravelResponse, TravelResponse } from './response/TravelResponse';
import {  TravelOrderContainer } from './order/TravelOrder';
import { ConnectedTravelResults, TravelResults } from './results/TravelResults';
import { TravelInsuranceContainer, TravelInsurance } from './insurance/TravelInsurance';
import { Tabs } from '../../components/tabs/Tabs';

import { Route, Switch, Redirect } from 'react-router-dom';
import { match } from 'react-router';
import { TravelPaymentContainer } from './payment/TravelPayment';

export interface ITravelProps {

	match: match<any>;
}


export class Travel extends React.Component<ITravelProps> {

	componentWillMount() {
		this.props.match;
	}

	public listSteps = [
		{
			route: 'insurance',
			label: 'Данные для расчета'
		},
		{
			route: 'results',
			label: 'Выбор опций и страховки'
		},
		{
			route: 'order',
			label: 'Оформление полиса'
		},
		{
			route: 'response',
			label: 'Оплата и получение'
		}
	]

	render() {

		const { match } = this.props;
		console.log('here', match.path)
		return (
			<div>
				<div className='main-header'>
					<div className='container' >
						<h1 className='main-header__title'>
							Оформление страховки путешественника
						</h1>
						<Tabs className='main-header__steps' tabList={this.listSteps}></Tabs>
					</div>
				</div>
				<div>
					<Switch>
						<Route component={TravelInsuranceContainer} path={match.path + '/insurance'} />
						<Route component={ConnectedTravelResults} path={match.path + '/results'} />
						<Route component={TravelOrderContainer} path={match.path + '/order'} />
						<Route component={ConnectedTravelResponse} path={match.path + '/response'} />
						<Route component={TravelPaymentContainer} path={match.path + '/payment'} />
						<Redirect to={match.path + '/insurance'} />
					</Switch>
				</div>
				
			</div>
		)
	}
}