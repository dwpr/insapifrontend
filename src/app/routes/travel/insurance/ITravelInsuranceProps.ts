import { TravelActions } from "../../../store/actions/travel.actions";
import { ITravelModel } from "../../../store/logic/travel/interfaces/ITravelModel";
import { ITravelListModel } from "../../../store/logic/travel/interfaces/ITravelListModel";




export interface ITravelInsuranceProps {
}

export interface ITravelContainerProps {
	travelModel: ITravelModel,
	dictionaryModel: ITravelListModel,
	isLoaded: boolean
}

export interface ITravelInsuranceDispatchProps {
	travelActions : TravelActions;
}






