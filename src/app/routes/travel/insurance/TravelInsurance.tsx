import { ITravelInsuranceProps, ITravelInsuranceDispatchProps, ITravelContainerProps } from "./ITravelInsuranceProps";
import { IRootState } from "../../../store/reducers";
import { bindActionCreators } from "redux";
import { travelActions } from "../../../store/actions/travel.actions";
import { connect } from "react-redux";
import { Select } from "../../../components/select/Select";
import { RangeDateInput } from "../../../components/range-date-input/RangeDateInput";
import { Label } from "../../../components/label/Label";
import { Validator } from "../../../components/validator/Validator";
import * as moment from 'moment';
import { Checkbox } from "../../../components/checkbox/Checkbox";
import { Tooltip } from "../../../components/tooltip/Tooltip";
import { DateInput } from "../../../components/date-input/DateInput";
import { Switcher } from "../../../components/switcher/Switcher";
import { AgeField } from "../../../components/age-field/AgeField";
import { Button } from "../../../components/button/Button";
import Slider from "../../../components/slider/Slider";
import { history } from '../../../app';
import { CustomRangeDatepicker } from "../../../components/new-range-date-picker/CustomRangeDatepicker";
import MainLoader from '../../../components/main-loader/MainLoader';


export class TravelInsurance extends React.Component<ITravelInsuranceProps & ITravelInsuranceDispatchProps & ITravelContainerProps> {

	componentWillMount() {
		const { travelActions, travelModel } = this.props;
		if (!travelModel) {
			travelActions.modelGet();
			travelActions.listModelGet();
		}
		if (localStorage.getItem('orderModel')) {
			localStorage.removeItem('model');
			localStorage.removeItem('orderModel');
		}
	}

	// валидация на блюре, еслиф форма уже была провалидирована

	componentDidUpdate(prevProps, prevState) {
		if (!Utils.validation.isValid && !this.state.isFieldFocused)  Utils.validation.validate('', false);
	}

	public state: any = {
		isFieldFocused: false,
		oldEndDate: ''
	}

	public listDays = [
		{ value: 30, label: '30' },
		{ value: 45, label: '45' },
		{ value: 60, label: '60' },
		{ value: 90, label: '90' },
		{ value: 180, label: '180' }
	];

	public onFocus = () => { this.setState({ isFieldFocused: true }) }

	public onBlur = () => { this.setState({ isFieldFocused: false }) }

	public onFastOptionClick = (event) => {
		const { travelModel, travelActions } = this.props;
		//если страна уже в списке - не добавляем
		if (travelModel.country.find(country => country.id == event.id)) return;
		let newValue = (travelModel.country as Array<any>).map(item => item.id);
		travelActions.countrySet({ value: [...newValue, event.id]});
	}
	/**
	 * Тайланд -> Таиланд
	 */
	public onInputChange = (inputValue) => {
		const { dictionaryModel, travelActions, travelModel } = this.props;
		const isBeginsWithThai = inputValue.toLowerCase().substr(0,3) == 'тай';
		const isSomeItemThai = dictionaryModel.listCountry.some(item => item.name == 'Тайланд');
		if ( isBeginsWithThai && !isSomeItemThai && travelModel.country.every(item => item.id != 166)) {
			travelActions.travelAddThailand();
		} else if (!isBeginsWithThai && isSomeItemThai) travelActions.travelRemoveThailand();
	}

	public onMultipleCheckboxChange = (event) => {
		const { travelActions, travelModel } = this.props;
		const newStartDate = moment(travelModel.startDate);
		travelActions.set(event);

		let coveredDayEvent = {};
		if (event.value) {
			coveredDayEvent = {name: 'coveredDay', value: 45};
		} else {
			// coveredDayEvent = {name: 'coveredDay', value: null};
			coveredDayEvent = {name: 'coveredDay', value:  moment(this.state.oldEndDate).diff(travelModel.startDate, 'days') + 1};
		}
		travelActions.set(coveredDayEvent);

		if (newStartDate.isValid()) {
			if (event.value) {
				this.setState({ oldEndDate: travelModel.endDate });
				travelActions.set({ name: 'endDate', value: newStartDate.add(1, 'year').format('YYYY-MM-DD') });
			}
			else {
				travelActions.set({ name: 'endDate', value: this.state.oldEndDate });
			}
		}
	}

	public onStartDateChange = (event) => {
		const { travelActions, travelModel } = this.props;
		const newStartDate = moment(event.value);
		travelActions.set(event);
		if (travelModel.isMultiple && newStartDate.isValid()) travelActions.set({ name: 'endDate', value: newStartDate.add(1, 'year').format('YYYY-MM-DD') });
	}

	onRangeDateInputChange = (event) => {
		this.props.travelActions.set(event);

		//обновляем coveredDay в модели
		let { endDate, startDate } = this.props.travelModel;

		let momentStartDate = moment(event.name == 'startDate' ? event.value : startDate);
		let momentEndDate = moment(event.name == 'endDate' ? event.value : endDate);

		if(momentStartDate.isValid() && momentEndDate.isValid()) {
			let coveredDay : number = momentEndDate.diff(momentStartDate, 'days') + 1;
			this.props.travelActions.set({ name: 'coveredDay', value: coveredDay });
		}
	}

	onAddTourist = async() => {
		const { travelModel, travelActions } = this.props;
		if (travelModel.insured.length == 5) return;
		await travelActions.touristAdd();
		if (this.refs[`ageField${travelModel.insured.length}`]) {
			(this.refs[`ageField${travelModel.insured.length}`] as any).focus();
		}
	}

	onSearchClick = () => {
		Utils.validation.validate().then(() => {
			history.push(frameManager.url('travel/results'));
		});
	}

	render() {
		const { isLoaded, dictionaryModel, travelModel, travelActions } = this.props;

		if (!isLoaded || !dictionaryModel) return (
			<div className='overlay travel-insurance__overlay'>
				<MainLoader />
			</div>
		);

		const { isMultiple, startDate, endDate, insured, coveredDay, country } = travelModel;
		const { listCountry } = dictionaryModel;

		const {  listDays, onMultipleCheckboxChange, onFastOptionClick, onInputChange, onSearchClick, onRangeDateInputChange, onFocus, onBlur, onAddTourist } = this;
		return (
			<div className='travel-insurance' >
				<div className='travel-insurance__lightblue-wrap'>
					<div className="container">
						<div className="travel-insurance__inner">
							<Validator listRule={[{ name: 'isLengthGreater', value: 0, message: '' }]} validationIndex={0}>
								<Select fastSelectOptions={[{ id: 188, name: 'Шенген' }, { id: 81, name: 'Кипр' }, { id: 163, name: 'Таиланд' }, { id: 159, name: 'США' }]}
									clearable={true}
									onFastOptionClick={onFastOptionClick}
									className='travel-insurance__country-select'
									multi={true}
									id='destination-country-select'
									name='listCountryId'
									value={country}
									onChange={travelActions.countrySet}
									options={listCountry}
									searchable={true}
									onInputChange={onInputChange}
									onFocus={onFocus}
									onBlur={onBlur}
								/>
							</Validator>
							<div className='travel-insurance__dates'>
								{
									isMultiple ?
										<div className="travel-insurance__insurance-days-wrap">
											<Validator listRule={[{ name: 'required', message: '' },
											{ name: 'isNotIncludes', value: '_', message: '' },
											{ name: 'isDateGreaterOrSame', value: moment().add(1, 'days').format('YYYY-MM-DD'), message: '' }]} validationIndex={1}>
												<DateInput className='travel-insurance-form__start-date'
													label={<Label caption='Первая поездка' tooltip='Дата первого пересечения границы' tooltipWidth='300'  />}
													name='startDate'
													value={startDate}
													onChange={travelActions.set}
													minDate={moment().add(1, 'days').format('YYYY-MM-DD')}
													maxDate={moment().add(364, 'days').format('YYYY-MM-DD')} 
													onFocus={onFocus}
													onBlur={onBlur}/>
											</Validator>
											<div className='travel-insurance__insurance-switcher'>
												<span className='travel-insurance__switcher-label'>Страховых дней</span>
												<Switcher id='coveredDay' name='coveredDay' value={coveredDay} options={this.listDays} onChange={travelActions.set} className='travel-insurance__days' />
											</div>											
										</div>
										:
										<Validator listRule={[{ name: 'required', property: 'startDate', message: '' },
										{ name: 'isNotIncludes', value: '_', property: 'startDate', message: '' },
										{ name: 'required', property: 'endDate', message: '' },
										{ name: 'isNotIncludes', value: '_', property: 'endDate', message: '' },
										{ name: 'isDateGreaterOrSame', value: moment().add(1, 'days').format('YYYY-MM-DD'), property: 'startDate', message: '' },
										{ name: 'isDateGreaterOrSame', value: startDate, property: 'endDate', message: '' }]} validationIndex={1}
										>
											<CustomRangeDatepicker className='travel-insurance__dates'
												label={<Label caption={'Даты поездки'} tooltip='Даты выезда за пределы Российской Федерации и возвращения обратно' tooltipWidth='300' />}
												from={{ name: 'startDate', value: startDate }}
												to={{ name: 'endDate', value: endDate }}
												onChange={onRangeDateInputChange}
												minDate={moment().add(1, 'days').format('YYYY-MM-DD')}
												maxDate={moment().add(364, 'days').format('YYYY-MM-DD')}
												startDate={startDate}
												endDate={endDate}
												onFocus={onFocus}
												onBlur={onBlur} 
												/>
										</Validator>
								}
								<Checkbox id='isMultiple' name='isMultiple' value={isMultiple} onChange={onMultipleCheckboxChange} label='Нужен годовой полис на несколько поездок' onFocus={onFocus} onBlur={onBlur}></Checkbox>								
							</div>
						</div>
						<div className="travel-insurance__travelers">
							<span className="travel-insurance__travelers-header">Путешественники</span>
							<Tooltip tooltip='Введите возраст туристов, на которых оформляется страховка' tooltipPosition='top'/>
							<div className="travel-insurance__inputs-wrap">
								{insured.map((item, index) => {
									return <Validator key={`ageField${index}`}  listRule={[{ name: 'required' }]} validationIndex={2}>
										<AgeField
											ref={`ageField${index}`}
											id='ageField' 
											name='ageField' 
											value={item.age} 
											onChange={e => travelActions.touristAgeSet(index, e.value)} 
											allowRemove={index != 0}
											onRemoveValue={e => travelActions.touristRemove(index)} 
											placeholder="Возраст"
											onFocus={onFocus}
											onBlur={onBlur}
											className='age-field--index'/>
									</Validator>
								})}
								{insured.length < 5 && <div onClick={onAddTourist} className='travel-insurance__add-tourist'>Добавить путешественника</div>}
							</div>							
						</div>
					</div>
				</div>
				<div className="travel-insurance__slider-wrap">
					<div className="container">
						<span className="travel-insurance__slider-header">Наши партнеры</span>
						<Slider className="travel-insurance__slider">
							<div className="travel-insurance__slide travel-insurance__slide--guide"><img src="/img/gaide.svg"/></div>
							<div className="travel-insurance__slide travel-insurance__slide--rosgosstrakh"><img src="/img/rosgosstrah.svg"/></div>
							<div className="travel-insurance__slide travel-insurance__slide--absolut"><img src="/img/absolut.svg"/></div>
							<div className="travel-insurance__slide travel-insurance__slide--compliance"><img src="/img/soglasie.svg"/></div>
							<div className="travel-insurance__slide travel-insurance__slide--alpha"><img src="/img/alfa.svg"/></div>
							<div className="travel-insurance__slide travel-insurance__slide--liberty"><img src="/img/liberty.svg"/></div>
							<div className="travel-insurance__slide travel-insurance__slide--renessans"><img src="/img/renessans.svg"/></div>
							<div className="travel-insurance__slide travel-insurance__slide--uralsib"><img src="/img/uralsib.svg"/></div>
							<div className="travel-insurance__slide travel-insurance__slide--insurion"><img src="/img/insurion.svg"/></div>
							<div className="travel-insurance__slide travel-insurance__slide--ingosstrakh"><img src="/img/ingosstrah.svg"/></div>
							<div className="travel-insurance__slide travel-insurance__slide--vtb"><img src="/img/vtb.svg"/></div>
							<div className="travel-insurance__slide travel-insurance__slide--tinkoff"><img src="/img/tinkoff.svg"/></div>
							<div className="travel-insurance__slide travel-insurance__slide--alliance"><img src="/img/allianz.svg"/></div>
							<div className="travel-insurance__slide travel-insurance__slide--zetta"><img src="/img/zetta.svg"/></div>							
						</Slider>
					</div>
				</div>
				<div className='travel-insurance__lightblue-wrap'>	
					<div className="container">
						<Button className="travel-insurance__insurance-btn" onClick={onSearchClick} >Подобрать страховку</Button>
					</div>		
				</div>		
			</div>
		)
	}
}




const mapStateToProps = (state: IRootState, ownProps) => ({
	travelModel: state.travel.model,
	dictionaryModel: state.travel.dictionaryModel,
	isLoaded: state.travel.isLoaded
});

const mapDispatchToProps = (dispatch: any, ownProps) => ({
	travelActions: bindActionCreators(travelActions, dispatch)
});



export const TravelInsuranceContainer = connect(mapStateToProps, mapDispatchToProps)(TravelInsurance);