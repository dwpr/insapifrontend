import { ITravelOrderDispatchProps, ITravelOrderContainerProps } from "./ITravelOrder.props";
import { IRootState } from "../../../store/reducers";
import { bindActionCreators } from "redux";
import { travelActions } from "../../../store/actions/travel.actions";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Price, CurrencyIcon } from "../../../components/price/Price";
import { Button } from "../../../components/button/Button";
import { Traveler } from "./traveler/Traveler";
import { Checkbox } from "../../../components/checkbox/Checkbox";
import { InsuranceOption } from '../../../components/insurance-option/InsuranceOption';
import * as moment from 'moment';
import { history } from '../../../app';
import { enumInsuranceCompanyId, enumInsuranceCompany } from "../../../store/logic/common";
import { Validator } from "../../../components/validator/Validator";
import { RiskCheckbox } from "../results/risk-checkbox/RiskCheckbox";
import { ITravelRiskModel } from "../../../store/logic/travel/interfaces/ITravelListModel";
import { ExpandBtn } from '../../../components/expand-btn/ExpandBtn';
import MainLoader from "../../../components/main-loader/MainLoader";


export class TravelOrder extends React.Component<ITravelOrderDispatchProps & ITravelOrderContainerProps> {

	public state = {
		isInsuranceRuleChecked: false,
		isUserAgreementChecked: false,
		isHeaderFixed: false,
		startHeaderPosition: 0,
		isHeaderFolded: false,
		isFieldFocused: false,
		isHiddenSpecialCondition: false,
		isVisibleValidMessage: false,
		isVisibleDetailsBtn: false
	}

	// приоритет отображения рисков
	priorityRisks = [2, 3, 8, 5, 4, 10, 11, 12, 6, 7, 13, 9];

	tinkoffTouristMaxAge = 75;
	tinkoffInsurerMaxAge = 80;

	componentWillMount() {
		let { orderModel, dictionaryModel, travelActions } = this.props;
		if (orderModel && dictionaryModel) {
			return travelActions.initChoosedBeforeListRisk();
		}
		// если пользователь вернулся на страницу с помощью кнопки "назад" с шага оплаты, достаем модель из кэша
		if (localStorage.getItem('orderModel')) {
			travelActions.setModelsFromCache();
			travelActions.listModelGet();
			return travelActions.initChoosedBeforeListRisk();
		}
		return history.push(frameManager.url('travel/insurance'))
	}

	componentDidMount() {
		let { orderModel } = this.props;
		let { specialCondition } = orderModel;
		let { risk } = orderModel.calculationModel;
		// стартовая позиция шапки с основной информацией в ордере = высоте хедера для всех страниц
		let headerHeight = document.querySelector('.main-header').getBoundingClientRect().height || 162;

		// следим за скроллом, дабы фиксировать хедер в видимой области экрана
		
		document.addEventListener('wheel', this.hideHeader);
		this.setState({ startHeaderPosition: headerHeight });
		if (this.refs.specialCondition && (this.refs.specialCondition as any).getBoundingClientRect().height > 45) {
			this.setState({ isHiddenSpecialCondition: true });
		}
		if (specialCondition && specialCondition.length > 0 || risk.length > 1) {
			this.setState({ isVisibleDetailsBtn: true });
		}
	}

	// валидация на блюре, если форма уже была провалидирована

	componentDidUpdate(prevProps, prevState) {
		if (!Utils.validation.isValid && prevState.isFieldFocused && !this.state.isFieldFocused) Utils.validation.validate('', false);
	}

	componentWillUnmount() {
		document.removeEventListener('wheel', this.hideHeader);
	}

	onFocus = () => {
		this.setState({ isFieldFocused: true });
	}

	onBlur = () => { this.setState({ isFieldFocused: false }) }

	hideSpecialConditions = () => {
		// сворачиваем спец условия при каждом скролле страницы
		if (!this.state.isHiddenSpecialCondition && this.refs.specialCondition && (this.refs.specialCondition as any).getBoundingClientRect().height > 45) {
			this.setState({ isHiddenSpecialCondition: true });
		}
	}

	showDetailsBtn = () => {
		let { specialCondition } = this.props.orderModel;
		let { risk } = this.props.orderModel.calculationModel;
		/**
		 * сворачиваем шапку и показываем кнопку "Детали", если:
		 * 1) блок стран больше 450px
		 * 2) есть доп условия
		 * 3) есть доп риски
		 */
		if ((this.refs.countries as any).getBoundingClientRect().width > 470 || specialCondition && specialCondition.length > 0 || risk.length > 1) {
			this.setState({ isVisibleDetailsBtn: true });
		}
	}

	hideHeader = () => {

		// если хедер находится за вьюпортом, фиксируем её к верхней границе экрана
		if ((this.refs.orderHeader as any).getBoundingClientRect().top < 0 && !this.state.isHeaderFixed) {
			this.setState({ isHeaderFixed: true, isHeaderFolded: true }, () => this.showDetailsBtn());
		}
		// если изначальное положение хедера на странице уже находится во вьюпорте, возвращаем его в поток
		if ((window.pageYOffset < this.state.startHeaderPosition) && this.state.isHeaderFixed) {
			this.setState({ isHeaderFixed: false, isHeaderFolded: false });
			this.hideSpecialConditions();
		}
		// если хедер был развернут, на скролле сворачиваем его снова
		if (this.state.isHeaderFixed && !this.state.isHeaderFolded) {
			this.setState({ isHeaderFolded: true }, () => this.showDetailsBtn());
		}
	}

	get listRisk() {
		let { orderModel, listChoosedBeforeRisk } = this.props;

		let listTravelRisk = [];

		let listTravelRiskNumbers = this.priorityRisks.map(priorityRisk => {
			if (orderModel.listTravelRisk.some(risk => risk.id == priorityRisk)) {
				let elem = (orderModel.listTravelRisk.find(item => {
					return item.id == priorityRisk;
				}));
				listTravelRisk.push(elem);
			}
		});
		// 13.06.2018 поменял проверку. Теперь смотрим не в props.model а в props.orderModel.calculationModel
		listTravelRisk = listTravelRisk.filter(risk => {
			if (!listChoosedBeforeRisk.some(chosenRisk => chosenRisk.id == risk.id)) {
				return risk;
			}
		})
		if (listTravelRisk.length > 4) {
			listTravelRisk = listTravelRisk.splice(0, 4);
		}
		return listTravelRisk;
	}

	// ограничения по числу туристов дл разных СК
	get isTouristAddDisabled() {
		const { insuranceCompanyId, insured } = this.props.orderModel.calculationModel;
		return (insured.length == 5);
		// return ((insuranceCompanyId == enumInsuranceCompanyId.tinkoff || insuranceCompanyId == enumInsuranceCompanyId.soglasie) && insured.length == 5);
	}

	// true для страховых, принимающих для покупателя только кириллицу
	public get isCyrillicOnly() {
		const { dictionaryModel, orderModel } = this.props;
		const insuranceCompany = dictionaryModel.listInsuranceCompany.find(item => item.id == orderModel.calculationModel.insuranceCompanyId);
		if (insuranceCompany) return insuranceCompany.isCyrillicOnly;
		return false;
	}

	// при смене паспорта на российский сброс флага "Он же покупатель", ибо турист всегда с загран паспортом
	public onPassportTypeChange = (isRussianPassport: boolean) => {
		const { travelActions, orderModel, indexTouristIsBuyer } = this.props;
		const { insured, insurer } = orderModel.calculationModel;
		const fullPassport = insurer.documentSeries + insurer.documentNo;
		travelActions.travelOrderInfoSet({ name: 'isRussianPassport', value: isRussianPassport });
		if (isRussianPassport && !this.isCyrillicOnly) {
			if (indexTouristIsBuyer != -1) {
				/**
				 * если застрахованный был покупателем и его поле паспорта не заполнено
				 * сбрасываем поле паспорта у застрахованного, ибо там не отрабатываем blur и в поле остается маска
				 */
				const { documentNo, documentSeries } = insured[indexTouristIsBuyer];
				if (!documentNo && !documentSeries || documentNo.replace(/\D/gi, '').length == 0 && documentSeries.replace(/\D/gi, '').length == 0) {
					this.onTouristChange({ name: 'documentNo', value: '', index: indexTouristIsBuyer });
					this.onTouristChange({ name: 'documentSeries', value: '', index: indexTouristIsBuyer });
				}
				// сбрасываем галочку
				travelActions.setIndexTouristIsBuyer(-1);
			}
		}
		// при смене типа паспорта на "загран" принудительно обрезаем лишний последний символ, ибо самостоятельно меняется только маска
		if (!isRussianPassport && fullPassport.replace(/\D/gi, '').length == 10) {
			this.onTouristChange({ name: 'documentSeries', value: fullPassport.substr(0, 2), index: indexTouristIsBuyer });
			this.onTouristChange({ name: 'documentNo', value: fullPassport.substr(2, 7), index: indexTouristIsBuyer });
		}
	}



	onInsurerChange = async (event) => {
		let { travelInsurerSet, touristOrderModelSet, travelRecalculate } = this.props.travelActions;
		const { insuranceCompanyId } = this.props.orderModel.calculationModel;
		let { indexTouristIsBuyer } = this.props;

		let maxAge = 100;
		if (insuranceCompanyId == enumInsuranceCompanyId.tinkoff) {
			maxAge = this.tinkoffInsurerMaxAge;
		}

		travelInsurerSet(event);

		// не копируем поля имени и фамилии с признаком isCyrillicOnly
		if ((indexTouristIsBuyer > -1 && !this.isCyrillicOnly) ||
			(indexTouristIsBuyer > -1 && this.isCyrillicOnly && event.name != 'lastName' && event.name != 'firstName')) {
			touristOrderModelSet({ ...event, index: indexTouristIsBuyer });
			// отправка запроса на перерасчет при, когда покупатель=застрахованный и введен возраст покупателя
			if (event.name == 'birthDate') {
				if (event.value.replace(/\D/gi, '').length == 8) {
					let newAge = moment().diff(event.value, 'years');
					await touristOrderModelSet({ ...event, name: 'age', value: newAge, index: indexTouristIsBuyer });
					// отправляем запрос только на корректный возраст
					if (this.state.isFieldFocused && newAge > 0 && newAge < maxAge) {
						travelRecalculate(this.props.orderModel.calculationModel);
					}
				}
			}
		}
	}

	public onTouristChange = async (event) => {
		const { travelInsurerSet, touristOrderModelSet, travelRecalculate } = this.props.travelActions;
		const { insured, insuranceCompanyId } = this.props.orderModel.calculationModel;
		let { indexTouristIsBuyer } = this.props;

		let maxAge = 100;
		if (insuranceCompanyId == enumInsuranceCompanyId.tinkoff) {
			maxAge = this.tinkoffTouristMaxAge;
		}

		// не копируем поля имени и фамилии с признаком isLatOnly
		if ((event.index == indexTouristIsBuyer && !this.isCyrillicOnly) ||
			(event.index == indexTouristIsBuyer && this.isCyrillicOnly && event.name != 'lastName' && event.name != 'firstName')) {
			travelInsurerSet(event);
		}
		touristOrderModelSet(event);
		if (event.name == 'birthDate') {
			if (event.value.replace(/\D/gi, '').length == 8) {
				let newAge = moment().diff(event.value, 'years');
				await touristOrderModelSet({ ...event, value: event.value });
				await touristOrderModelSet({ ...event, name: 'age', value: newAge });
				// отправляем запрос только на корректный, новый возраст
				if (newAge > 0 && newAge < maxAge) {
					travelRecalculate(this.props.orderModel.calculationModel);
				}
			}
		}
	}

	public onCheckboxIsBuyerChange = (touristIndex: number) => {
		const { indexTouristIsBuyer, travelActions, orderModel } = this.props;
		const { insured } = orderModel.calculationModel;
		//Если этот турист уже является покупателем - убираем.
		if (touristIndex === indexTouristIsBuyer) {
			travelActions.setIndexTouristIsBuyer(-1);
		}
		else {
			travelActions.setIndexTouristIsBuyer(touristIndex)
			this.props.travelActions.travelOrderInfoSet({ name: 'isRussianPassport', value: false });
			//копируем поля из модели туриста в модель страхователя.
			const touristModel = this.props.orderModel.calculationModel.insured[touristIndex];
			const { birthDate, firstName, lastName, documentNo, documentSeries } = touristModel;
			for (let fieldName in touristModel) {
				// если разрешена латиница, то копируем все поля
				// если только кириллица, то паспорт и др. Имя, фамилию не копируем
				if (!this.isCyrillicOnly || (this.isCyrillicOnly && fieldName != 'firstName' && fieldName != 'lastName')) {
					this.props.travelActions.travelInsurerSet({ name: fieldName, value: touristModel[fieldName] });
				}
			}
		}
	}

	// при добавлении туриста, запускаем перерасчет

	public onAddTourist = () => {
		const { travelActions } = this.props;
		if (this.isTouristAddDisabled) return;
		travelActions.travelOrderAddTourist();
		setTimeout(() => travelActions.travelRecalculate(this.props.orderModel.calculationModel), 200);
	}

	// при удалении туриста, запускаем перерасчет

	public onRemoveTourist = (index) => {
		const { travelActions } = this.props;
		travelActions.travelOrderRemoveTourist(index);
		setTimeout(() => travelActions.travelRecalculate(this.props.orderModel.calculationModel), 200);
	}

	onTravelOrderStateChange = ({ name, value }) => {
		if (!name) return;
		this.setState({ [name]: value });
	}


	/**
	 * @desc если риск уже в модели, убираем его, если в модели его нет - добавляем.
	 */
	onRiskToggle = (riskModel: ITravelRiskModel, isActive: boolean) => {
		let { addOrderModelRisk, removeOrderModelRisk, travelRecalculate } = this.props.travelActions;
		if (isActive) {
			addOrderModelRisk({ riskModel });
		} else {
			removeOrderModelRisk({ id: riskModel.id });
		}
		setTimeout(() => travelRecalculate(this.props.orderModel.calculationModel), 200);
	}

	changeRiskCoverage = async ({ id, coverageSum }) => {
		let { changeOrderRiskCoverage, travelRecalculate } = this.props.travelActions;
		await changeOrderRiskCoverage({ id, coverageSum });
		travelRecalculate(this.props.orderModel.calculationModel);
	}

	onBuyBtnClick = () => {
		this.setState({ isVisibleValidMessage: true });
		Utils.validation.validate('', true).then(() => {
			this.props.travelActions.policySave((isOk) => {
				if (!isOk && this.refs.orderErrorMessage) {
					// const coords = (this.refs.orderErrorMessage as HTMLElement).getBoundingClientRect();
					// frameManager.scrollTo(coords.top - 30);
				}
			});
		})
	}

	/** @desc скачиваем пользовательское соглашение с BE
	 */
	onAgreementClick = (event) => {
		event.preventDefault();
		const { travelActions, orderModel } = this.props;
		const { quotationGuid, insurer } = orderModel.calculationModel;
		const { firstName, lastName } = insurer;
		travelActions.getUserAgreement(quotationGuid, firstName || '', lastName || '');
	}

	private logoSizeModifier(insuranceCompanyId: number): string {
		let insuranceCompanyName = (enumInsuranceCompany.get(insuranceCompanyId) as any).name;
		return `selected-insurance__logo--${insuranceCompanyName}`
	}

	public render() {
		if (!this.props.orderModel || !this.props.dictionaryModel) return '';
		const { onBuyBtnClick, onInsurerChange, onTouristChange, onAddTourist, onRemoveTourist, onTravelOrderStateChange, onCheckboxIsBuyerChange, onPassportTypeChange, isCyrillicOnly, listRisk, changeRiskCoverage, onRiskToggle, onBlur, onFocus } = this;
		const { orderModel, dictionaryModel, isLoaded, orderInfo, travelActions, isRecalculated, showWarningMessage, indexTouristIsBuyer, model } : ITravelOrderDispatchProps & ITravelOrderContainerProps = this.props;
		const { specialCondition, price, calculationModel, docLink } = orderModel;
		const { insuranceCompanyId, country, isMultiple, startDate, endDate, coveredDay, risk, currencyId, insured, insurer } = orderModel.calculationModel;

		const { isInsuranceRuleChecked, isUserAgreementChecked, isHiddenSpecialCondition, isVisibleDetailsBtn, isHeaderFixed } = this.state;

		const { isRussianPassport } = orderInfo;

		// определяем валюту страхования
		let currency;
		dictionaryModel.listCurrency.forEach(item => {
			if (item.id == currencyId) currency = item.name;
		});


		return (
			<div className='travel-order'>
				{
					!isLoaded &&
					<div className='overlay travel-order__overlay'>
						<MainLoader />
					</div>
				}
				<div className={`selected-insurance ${isHeaderFixed ? 'selected-insurance--fixed' : ''} ${this.state.isHeaderFolded ? 'selected-insurance--folded' : ''}`} ref='orderHeader' id='orderHeader'>
					<div className='container'>
						<div className='selected-insurance__back-btn' onClick={() => travelActions.navigate(2, 3)}>
							<i className='icon-arrow-back'></i>
							<div className='selected-insurance__back-btn-text'>Выбрать другую страховку</div>
						</div>
						{orderInfo.isPaymentError && <div ref='orderErrorMessage' className='travel-order__warning'><i className='icon-warning'></i>Платежная система сообщила о проблеме с оплатой страховки. Попробуйте повторить оплату с другой карты.</div>}
						{!Utils.validation.isValid && this.state.isVisibleValidMessage && <div ref='orderErrorMessage' className='travel-order__warning'>Проверьте заполненность всех полей</div>}
						{showWarningMessage && <div ref='orderErrorMessage' className='travel-order__warning'>При обращении к выбранной страховой компании возникла ошибка. Пожалуйста, попробуйте позднее или выберите полис другой компании</div>}
						<div className='selected-insurance__flex-wrap'>
							<div className='selected-insurance__logo-wrap'>
								<div className={`selected-insurance__logo ${this.logoSizeModifier(insuranceCompanyId)}`}>
									<img src={Utils.text.getInsuranceLogoById(insuranceCompanyId)} />
								</div>
								{isVisibleDetailsBtn && isHeaderFixed &&
									<ExpandBtn text='Детали страховки' className='selected-insurance__details-btn' onClick={() => this.setState({ isHeaderFolded: false, isVisibleDetailsBtn: false }, () => this.hideSpecialConditions())} />
								}
							</div>

							<ul className='selected-insurance__parameter-list'>
								<li className='selected-insurance__parameter-item'>
									<div className='selected-insurance__parameter-inner'>
										<div className='selected-insurance__parameter-title'>Страны путешествия</div>
									</div>
									<div>
										<div ref='countries' className={`selected-insurance__parameter selected-insurance__parameter--countries ${isVisibleDetailsBtn ? 'selected-insurance__parameter--countries-folded' : ''}`}>
											{country.map((item, index) => {
												return <span key={`country${index}`}>{item.name}{index != country.length - 1 && ', '}</span>
											})}
										</div>
									</div>
								</li>
								<li className='selected-insurance__parameter-item'>
									<div className='selected-insurance__parameter-inner'>
										<div className='selected-insurance__parameter-title'>{isMultiple ? 'Годовой полис' : 'Даты поездки'}</div>
									</div>
									<div>
										<div className='selected-insurance__parameter'>
											{isMultiple ? `c ${moment(startDate).format('D MMM YYYY')} (${coveredDay} ${Utils.text.getWordForm(coveredDay, ['страховой день', 'страховых дня', 'страховых дней'])})`
												:
												<span>
													{Utils.date.getPeriodDate(startDate)} &mdash; {Utils.date.getPeriodDate(endDate)}
												</span>
											}
										</div>
									</div>
								</li>
								{risk.length > 1 && <li className='selected-insurance__parameter-item selected-insurance__risks'>
									<div className='selected-insurance__parameter-inner'>
										<div className='selected-insurance__parameter-title'>Дополнительные риски</div>
									</div>
									<div>
										{risk.map((item, index) => {
											if (item.id != 1) {
												return <div key={`risk${index}`} className='selected-insurance__parameter'>
													<span>{item.name}</span>
													<span className='selected-insurance__parameter-sum'>{item.coverageSum ? ` (${item.coverageSum} ${CurrencyIcon[currency]})` : ''}</span>
												</div>
											}
										})}
									</div>
								</li>}
								<li className='selected-insurance__parameter-item'>
									<div className='selected-insurance__parameter-inner'>
										<div className='selected-insurance__parameter-title'>Страховая сумма</div>
									</div>
									<div>
										<div className='selected-insurance__parameter'>
											{risk.map((item, index) => {
												if (item.id == 1) {
													return <div key={`summ${index}`} className='selected-insurance__parameter'>{Utils.text.numberWithSpaces(item.coverageSum)} {CurrencyIcon[currency]}</div>
												}
											})}
										</div>
									</div>
								</li>
								{specialCondition && specialCondition.length > 0 &&
									<li ref='specialCondition' className='selected-insurance__parameter-item selected-insurance__special'>
										<div className='selected-insurance__parameter-inner'>
											<div className='selected-insurance__parameter-title'>Особые условия</div>
										</div>
										<div>
											{specialCondition.map((item, index) => {
												return <div key={`special${index}`} className={`selected-insurance__parameter ${isHiddenSpecialCondition ? 'selected-insurance__parameter--short' : ''}`}>{item.value}</div>
											})}
										</div>
									</li>
								}
								{specialCondition && isHiddenSpecialCondition && <ExpandBtn text='Подробнее' className='expand-btn selected-insurance__special-more' onClick={() => this.setState({ isHiddenSpecialCondition: false })} />}
							</ul>
							<div className='selected-insurance__price-btn'>
								<Price currency={CurrencyIcon['RUB']} isNeedAnimation={!isRecalculated}>{price}</Price>
								<Button onClick={onBuyBtnClick} disabled={!isRecalculated || !isLoaded}>Купить страховку</Button>
							</div>
						</div>
					</div>
				</div>

				<div className={`travel-order__travelers ${this.state.isHeaderFixed ? 'travel-order__travelers--head-fixed' : ''}`}>
					<div className='container'>
						<h2 className='travel-order__header'>Путешественники</h2>
						<ul className='travel-order__travelers-list'>
							{insured.map((item, index) => {
								return <Traveler key={`traveler${index}`}
									index={index}
									model={item}
									onChange={onTouristChange}
									onRemoveTourist={onRemoveTourist}
									isBuyer={indexTouristIsBuyer == index}
									onCheckboxIsBuyerChange={e => onCheckboxIsBuyerChange(index)}
									onFocus={onFocus}
									onBlur={onBlur}
									scrollRef={`traveler${index + 1}`}
									birthdayValidationRules={insuranceCompanyId == enumInsuranceCompanyId.tinkoff && [{ name: 'isDateGreater', value: moment().subtract(this.tinkoffTouristMaxAge, 'years').format('YYYY-MM-DD'), message: `Туристу должно быть до ${this.tinkoffTouristMaxAge} лет` }]}
									/>
							})}
						</ul>
						<div className='travel-order__add-traveler'>
							<i className='icon-adult'></i>
							<div className='travel-order__add-btn' onClick={onAddTourist}>
								Добавить путешественника
							</div>
						</div>
						<h2 className='travel-order__header'>Покупатель полиса</h2>
						<Traveler isInsurer={true}
							model={insurer}
							index={-1}
							onChange={onInsurerChange}
							allowChangePassport={!(indexTouristIsBuyer != -1 && insuranceCompanyId == enumInsuranceCompanyId.tinkoff)}
							onPassportTypeChange={onPassportTypeChange}
							isRussianPassport={isRussianPassport}
							isCyrillicOnly={isCyrillicOnly}
							onFocus={onFocus}
							onBlur={onBlur}
							scrollRef={`insurer`}
							ref='insurer'
							birthdayValidationRules={insuranceCompanyId == enumInsuranceCompanyId.tinkoff && [{ name: 'isDateGreater', value: moment().subtract(this.tinkoffInsurerMaxAge, 'years').format('YYYY-MM-DD'), message: `Покупателю должно быть до ${this.tinkoffInsurerMaxAge} лет` }]} 
						/>
						{listRisk.length > 0 &&
							<div>
								<h2 className='travel-order__header'>Рекомендуем включить в страховку</h2>
								<div className='travel-order__options'>
									{listRisk.map(riskModel => {
										let isActive = Utils.array.checkIsObjectInListById(calculationModel.risk, riskModel.id);
										return (
											<RiskCheckbox
												riskModel={riskModel}
												currency={CurrencyIcon[currency]}
												onChangeSum={({ id, value }) => changeRiskCoverage({ id, coverageSum: value })}
												onChange={(payload) => onRiskToggle(riskModel, payload.value)}
												isActive={isActive}
												key={riskModel.id}
											/>
										)
									})}
								</div>
							</div>
						}
						<div className='travel-order__checkbox-wrap' ref='agreement'>
							<Validator listRule={[{ name: 'required' }]} validationIndex={1000} scrollRef={this.refs.agreement}>
								<Checkbox id='isUserAgreementChecked' name='isUserAgreementChecked' value={isUserAgreementChecked} onChange={onTravelOrderStateChange} onFocus={onFocus} onBlur={onBlur} label={
									<div>
										Я принимаю условия <span onClick={event => this.onAgreementClick(event)} className='link'>пользовательского соглашения</span><br />и даю согласие на обработку персональных данных
									</div>} />
							</Validator>
							<Validator listRule={[{ name: 'required' }]} validationIndex={1001} scrollRef={this.refs.agreement}>
								<Checkbox id='isInsuranceRuleChecked' name='isInsuranceRuleChecked' value={isInsuranceRuleChecked} onChange={onTravelOrderStateChange} onFocus={onFocus} onBlur={onBlur} label={
									<div>
										Я принимаю <span onClick={() => travelActions.insuranceRules(orderModel.calculationModel.quotationGuid)} className='link'>условия страхования</span>
									</div>} />
							</Validator>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state: IRootState, ownProps) => ({
	orderModel: state.travel.orderModel,
	dictionaryModel: state.travel.dictionaryModel,
	model: state.travel.model,
	isLoaded: state.travel.isLoaded,
	orderInfo: state.travel.orderInfo,
	listChoosedBeforeRisk: state.travel.listChoosedBeforeRisk,
	isRecalculated: state.travel.isRecalculated,
	showWarningMessage: state.travel.showWarningMessage,
	indexTouristIsBuyer: state.travel.indexTouristIsBuyer
});

const mapDispatchToProps = (dispatch: any, ownProps) => ({
	travelActions: bindActionCreators(travelActions, dispatch)
});



export const TravelOrderContainer = connect(mapStateToProps, mapDispatchToProps)(TravelOrder as any);