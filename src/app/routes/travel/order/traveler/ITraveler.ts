import { IControlEvent } from "../../../../lib/IControlEvent";


export interface ITravelerProps {
    /**
	 * модель для заполнения данных туриста
	 */
    model ?: any;
    /**
	 * страхователь
	 */
    isInsurer ?: boolean;
    /**
	 * индекс туриста в списке
	 */
    index ?: number;
    /**
	 * событие на onChange
	 */
    onChange(event: IControlEvent): any;
    /**
	 * удаление туриста из списка
	 */
    onRemoveTourist?(index: number): any;
    /**
	 * турист = страхователь
	 */
    isBuyer?: boolean;
    /**
	 * событие на клик по чекбоксу "он же покупатель"
	 */
    onCheckboxIsBuyerChange?(event: IControlEvent): any;
    /**
	 * есть ли возможность смены типа паспорта
	 */
    allowChangePassport?: boolean;
    /**
	 * событие на смену типа паспорта
	 */
    onPassportTypeChange?(isRussian: boolean): any;
    /**
	 * признак российского паспорта
	 */
    isRussianPassport?: boolean;
    /**
	 * страховая принимает только киириллицу для страхователя
	 */
    isCyrillicOnly?: boolean;
    /**
	 * событие на onBlur
	 */
    onBlur?(): any;
    /**
	 * событие на onFocus
	 */
    onFocus?(): any;
    /**
	 * ref, к которому необходим скролл на валидации
	 */
    scrollRef ?: string;
    /**
	 * дополнительные валидации на поле даты рождения
	 */
    birthdayValidationRules?: Array<any>;
}