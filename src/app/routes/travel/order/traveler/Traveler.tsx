import './Traveler.style.scss';
import { ITravelerProps } from "./ITraveler";
import { Field } from "../../../../components/field/Field";
import { Checkbox } from "../../../../components/checkbox/Checkbox";
import { spawn } from 'child_process';
import * as moment from 'moment';
import { Validator } from '../../../../components/validator/Validator';
import { BirthdateInput } from '../../../../components/birthdate-input/BirthdateInput';



export class Traveler extends React.Component<ITravelerProps> {

    transformToIso = (dateString: string) => {
        let value = dateString;
        if (dateString) {
            const [day, month, year] = value.split('.');
            value = `${year}-${month}-${day}`;
        }
        return value;
    }


    transformIsoToRus = (dateString: string) => {
        if (!dateString && dateString != '0') return '';
        if (dateString.match(/\./gi)) return dateString;
        if (dateString.indexOf('-') > -1) {
            const [year, month, day] = dateString.split('-');
            return `${day}.${month}.${year}`;
        }
    }

    // для страхователя валидация на возраст от 18 до 99
    // для застрахованного от 0 до 99

    get listBirthDateValidationRules() {
        const { isInsurer, model, birthdayValidationRules } = this.props;
        let listRules;
        if (isInsurer) {
            listRules = [{ name: 'required', message: 'Укажите дату рождения' },
            { name: 'isNotIncludes', value: '_', message: 'Укажите дату рождения' },
            { name: 'isDateValid', message: 'Укажите дату рождения'},
            { name: 'isDateGreaterOrSame', value: moment().subtract(100, 'years').format('YYYY-MM-DD'), message: 'Укажите корректное значение' },
            { name: 'isDateLessOrSame', value: moment().subtract(18, 'years').format('YYYY-MM-DD'), message: 'Покупателю должно быть 18 лет' }
            ];
        }
        listRules = [{ name: 'required', message: 'Укажите дату рождения туриста' },
        { name: 'isNotIncludes', value: '_', message: 'Укажите дату рождения туриста' },
        { name: 'isDateValid', message: 'Укажите дату рождения туриста'},
        { name: 'isDateGreaterOrSame', value: moment().subtract(100, 'years').format('YYYY-MM-DD'), message: 'Укажите корректное значение' },
        { name: 'isDateLessOrSame', value: moment().format('YYYY-MM-DD'), message: 'Укажите корректное значение' }
        ];
        if (birthdayValidationRules) {
            listRules = listRules.concat(birthdayValidationRules);
        }
        return listRules;
    }

    /**
	 * Если выбранный тип паспорта - российский => ставим валидацию на кирилицу
	 * Если выбранный тип паспорта - иностранный => ставим валидацию на латиницу
	 */
    listInsurerValidationRules = (errorMessage) => {
        const { isRussianPassport, isCyrillicOnly, isInsurer } = this.props;
        if (isRussianPassport || isCyrillicOnly && isInsurer) {
            return [{ name: 'required', message: errorMessage }, { name: 'isCyrillicAndHyphen', message: 'Поле должно содержать только кириллические символы' }];
        }
        return [{ name: 'required', message: errorMessage }, { name: 'isLatinAndHyphen', message: 'Поле должно содержать только латинские символы' }];
    }
    // для российского паспорта 10 цифр
    // для загран 9
    get listPassportValidationRules() {
        const { isRussianPassport, isInsurer } = this.props;
        let errorMessage = 'Укажите полностью серию и номер заграничного паспорта';
        if (isRussianPassport && isInsurer) {
            errorMessage = 'Укажите полностью серию и номер российского паспорта';
            return [{ name: 'required', message: errorMessage }, { name: 'isLengthEqual', value: 10, message: errorMessage }, { name: 'isNotIncludes', value: '_', message: errorMessage }]
        }
        return [{ name: 'required', message: errorMessage }, { name: 'isLengthEqual', value: 9, message: errorMessage }, { name: 'isNotIncludes', value: '_', message: errorMessage }]
    }

    get fullPassport() {
        const { model } = this.props;
        return (model.documentSeries || '') + (model.documentNo || '');
    }

    get fullPassportMask() {
        return this.props.isInsurer && this.props.isRussianPassport ? '9999 999999' : '99 9999999';
    }

    onFullPassportChange = (event, index) => {
        const { onChange } = this.props;
        let documentNo = '', documentSeries = '';
        let valueWithoutSpace = event.value.replace(' ', '');
        if (valueWithoutSpace.length <= 9) {
            documentSeries = valueWithoutSpace.substr(0, 2);
            documentNo = valueWithoutSpace.substr(2, valueWithoutSpace.length);
        } else {
            documentSeries = valueWithoutSpace.substr(0, 4);
            documentNo = valueWithoutSpace.substr(4, valueWithoutSpace.length);
        }
        onChange({
            name: 'documentNo',
            value: documentNo,
            props: event.props,
            index
        });
        onChange({
            name: 'documentSeries',
            value: documentSeries,
            props: event.props,
            index
        });
    }

    onBirthDateChange = (event, index) => {
        if (event.value.replace(/\D/gi, '').length == 8) {
            let newBirthDate = this.transformToIso(event.value);
            this.props.onChange({ ...event, index, value: newBirthDate });
        } else {
            this.props.onChange({ ...event, index });
        }
    }
    

    get isFilledTraveler() {
        const { model } = this.props;
        // заполненность всех полей
        for (let item in model) {
            if (!model[item]) return false;
        }
        // заполненность полностью полей с масками
        if (model.documentNo.indexOf('_') != -1 || model.birthDate.indexOf('_') != -1) {
            return false;
        }
        return true;
    }
    onPassportTypeChange = (isRussian) => {
        const { onPassportTypeChange } = this.props;
        (this.refs.passportField as any).refs.mask.input.focus(); 
        onPassportTypeChange(isRussian);
    }
    onPassportTypeClick = (isRussian) => {
        
        const { isRussianPassport } = this.props;
        if (this.fullPassport.replace(/\D/gi, '').length == 0) {
            return (this.refs.passportField as any).refs.mask.input.setSelectionRange(0,0);
        }
        if (isRussianPassport == isRussian) return;
        this.onPassportTypeChange(isRussian);
    }
    onPassportMouseDown = (event, isRussian) => {
        if(event.target.classList.contains('traveler__passport--active')) {
            return
        }
        if (this.fullPassport.replace(/\D/gi, '').length != 0) return;
        event.preventDefault();
        this.onPassportTypeChange(isRussian);
    }


    render() {
        const { fullPassport, fullPassportMask, onFullPassportChange, listPassportValidationRules, onBirthDateChange, isFilledTraveler, onPassportTypeChange } = this;
        const { model, isInsurer, index, onChange, onRemoveTourist, onCheckboxIsBuyerChange, isBuyer, isRussianPassport, isCyrillicOnly, onBlur, onFocus, allowChangePassport, scrollRef } = this.props;

        const listFirstNameValidationRules = [{ name: 'required', message: 'Укажите имя' }, { name: 'isLatinAndHyphen', message: 'Поле должно содержать только латинские символы' }];
        const listLastNameValidationRules = [{ name: 'required', message: 'Укажите фамилию' }, { name: 'isLatinAndHyphen', message: 'Поле должно содержать только латинские символы' }];

		const validationIndex = index == -1 ? 50 : index;

        return (
            <li ref={scrollRef} className={`traveler ${isInsurer ? 'traveler--buyer' : ''} ${isFilledTraveler ? 'traveler--filled' : ''}`}>
                {isInsurer ? null :
                    <div className='traveler__close-btn' onClick={event => onRemoveTourist(index)}>
                        <i className='icon-close'></i> Удалить
                </div>}
                {isInsurer ?
                    <div className='traveler__contacts'>
                        <Validator listRule={[{ name: 'required', message: 'Укажите адрес электронной почты' }, { name: 'email', message: 'Некорректный email' }]} validationIndex={4 * validationIndex - 2} scrollRef={this.refs[`${scrollRef}`]}>
                            <Field id='email' name='email' value={model.email} onChange={event => onChange({ ...event, index })} label={<span><i>Email</i> для отправки полиса</span>} onFocus={onFocus} onBlur={onBlur} />
                        </Validator>
                        <Validator listRule={[{ name: 'required', message: '	Укажите номер телефона' }, { name: 'isNotIncludes', value: '_' }]} validationIndex={4 * validationIndex - 1} scrollRef={this.refs[`${scrollRef}`]}>
                            <Field id='phone' name='phone' mask={"+7(999)999-99-99"} value={model.phone} className='traveler__phone' onChange={event => onChange({ ...event, index })} label={<span><i>Телефон</i> для экстренной связи</span>} onFocus={onFocus} onBlur={onBlur} />
                        </Validator>
                    </div>
                    :
                    <div className='traveler__head'>
                        <div className='traveler__icon'>
                    <i className={model.age < 16 && model.age > 0 ? 'icon-kid' : 'icon-adult'}></i>
                    {/* <i className={model.birthDate && model.birthDate.indexOf('_') == -1 && moment().diff(moment(model.birthDate), 'years') >= 16 || model.birthDate && model.birthDate.indexOf('_') != -1 && model.age >= 16 || !model.birthDate ? 'icon-adult' : 'icon-kid'}></i> */}
                        </div> 
                        <div className='traveler__flex-wrap'>
                            {index + 1} Путешественник
                            {model.age < 0 || model.age > 100 || isNaN(model.age) ? '' : ','}
                            {model.age < 0 || model.age > 100 || isNaN(model.age) ?
                                ''
                                :
                                model.birthDate && model.birthDate.indexOf('_') == -1 ?
                                    <span> {moment().diff(moment(model.birthDate), 'years')} {Utils.text.getWordForm(moment().diff(moment(model.birthDate), 'years'), ['год', 'года', 'лет'])}</span>
                                    :
                                    <span> {model.age} {Utils.text.getWordForm(model.age, ['год', 'года', 'лет'])}</span>
                            }
                            {(moment().diff(moment(model.birthDate), 'years') >= 18 || model.age >= 18) && <Checkbox id={`isBuyer-${index}`} name='isBuyer' value={isBuyer} onChange={onCheckboxIsBuyerChange} label='Покупатель страховки' onFocus={onFocus} onBlur={onBlur} />}
                        </div>
                    </div>
                }
                <div className='traveler__inputs'>
                    <div className='traveler__passports-wrap'>
                        <Validator listRule={listPassportValidationRules} validationIndex={validationIndex * 4} scrollRef={this.refs[`${scrollRef}`]}>
                            <Field id='passport' name='passport' value={fullPassport}
                                ref='passportField'
                                label={isInsurer && isRussianPassport ? 'Серия, № паспорта РФ' : 'Серия, № загранпаспорта'}
                                inputMode='numeric'
                                mask={fullPassportMask}
                                onChange={event => onFullPassportChange(event, index)}
                                onFocus={onFocus}
                                onBlur={onBlur} />
                        </Validator>
                        {isInsurer && allowChangePassport &&
                            <div className='traveler__passports'>
                                <span className={`traveler__passport ${!isRussianPassport && 'traveler__passport--active'}`} onMouseDown={event => this.onPassportMouseDown(event, false)} onClick={event => this.onPassportTypeClick(false)}>Заграничный</span>
                                <span className={`traveler__passport ${isRussianPassport && 'traveler__passport--active'}`} onMouseDown={event => this.onPassportMouseDown(event, true)} onClick={event => this.onPassportTypeClick(true)}>Паспорт РФ</span>
                            </div>
                        }
                    </div>
                    {/* <Validator listRule={this.listBirthDateValidationRules} validationIndex={3}>
                        <Field id='birthDate' name='birthDate' isDate={true} value={model.birthDate} mask={'99.99.9999'} className='traveler__birthdate' label='Дата рождения' onChange={event => onBirthDateChange(event, index)} onFocus={onFocus} onBlur={onBlur} />
                    </Validator> */}
                    <Validator name='birthDate' listRule={this.listBirthDateValidationRules} validationIndex={validationIndex * 4 + 1} scrollRef={this.refs[`${scrollRef}`]}>
                        <BirthdateInput id='birthDate' name='birthDate' 
                        value={model.birthDate} 
                        defaultAge={model.age}
                        className='traveler__birthdate' 
                        label='Дата рождения' 
                        onChange={event => onChange({ ...event, index })} 
                        onFocus={onFocus} 
                        onBlur={onBlur} />
                    </Validator>
                    <Validator listRule={isInsurer ? this.listInsurerValidationRules('Укажите имя') : listFirstNameValidationRules} validationIndex={validationIndex * 4 + 2} scrollRef={this.refs[`${scrollRef}`]}>
                        <Field id='firstName' name='firstName' value={model.firstName} onChange={event => onChange({ ...event, index })} onFocus={onFocus} onBlur={onBlur}
                            label={isRussianPassport || isCyrillicOnly && isInsurer ? 'Имя как в паспорте РФ' : 'Имя как в загранпаспорте'} />
                    </Validator>
                    <Validator listRule={isInsurer ? this.listInsurerValidationRules('Укажите фамилию') : listLastNameValidationRules} validationIndex={validationIndex * 4 + 3} scrollRef={this.refs[`${scrollRef}`]}>
                        <Field id='lastName' name='lastName' value={model.lastName} onChange={event => onChange({ ...event, index })} onFocus={onFocus} onBlur={onBlur}
                            label={isRussianPassport || isCyrillicOnly && isInsurer ? 'Фамилия как в паспорте РФ' : 'Фамилия как в загранпаспорте'} />
                    </Validator>
                </div>
            </li>
        )
    }
}