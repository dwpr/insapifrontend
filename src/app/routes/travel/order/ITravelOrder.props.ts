import { TravelActions } from '../../../store/actions/travel.actions';
import { IOrderModel, ITravelModel, IRiskModel } from '../../../store/logic/travel/interfaces/ITravelModel';
import { ITravelListModel } from '../../../store/logic/travel/interfaces/ITravelListModel';

export interface ITravelOrderContainerProps {
	orderModel: IOrderModel,
	dictionaryModel: ITravelListModel,
	isLoaded: boolean,
	model: ITravelModel,
	orderInfo: { quotationGuid: string, isPaymentError: boolean, isRussianPassport: boolean };
	listChoosedBeforeRisk: IRiskModel[];
	isRecalculated: boolean;
	showWarningMessage: boolean;
	indexTouristIsBuyer: number
}

export interface ITravelOrderDispatchProps {
	travelActions : TravelActions;
}






