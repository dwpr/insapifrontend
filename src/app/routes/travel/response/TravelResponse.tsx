import { ITravelResponseProps, ITravelResponseDispatchProps } from "./ITravelResponseProps";
import { bindActionCreators } from "redux";
import { travelActions } from "../../../store/actions/travel.actions";
import { connect } from "react-redux";
import { Button } from "../../../components/button/Button";
import { Field } from "../../../components/field/Field";
import { SectionPreloader } from "../../../components/section-preloader/SectionPreloader";
import * as moment from 'moment';
import { CurrencyIcon, Price } from "../../../components/price/Price";
import { Validator } from "../../../components/validator/Validator";
import { history } from '../../../app';
import { enumInsuranceCompany } from "../../../store/logic/common";
import { IRootState } from "../../../store/reducers";






export class TravelResponse extends React.Component<ITravelResponseProps & ITravelResponseDispatchProps> {

	componentWillMount() {
		moment.locale('ru');

		const { travelActions, orderInfo, orderModel } = this.props;
		let currentQuotationGuid = orderInfo.quotationGuid;

		let urlParams : any = Utils.text.getJsonFromUrl();
		let { quotationGuid, result } = urlParams;

		if (localStorage.getItem('orderModel')) {
			localStorage.removeItem('model');
			localStorage.removeItem('orderModel');
		}

		// если редирект был выполнен не в ручную внутри TravelPayment, а бекендом, то берем гуид из url

		if (!currentQuotationGuid) {
			travelActions.travelOrderInfoSet({ name: 'quotationGuid', value: quotationGuid });
			currentQuotationGuid = quotationGuid;
		}

		if (result == 'error' || result == 'cancel') {
			travelActions.travelOrderInfoSet({ name: 'isPaymentError', value: true });
			travelActions.convertPolicyInformation({ policyData: { quotationGuid }, callback: () => {
				history.push(frameManager.url('travel/order'));
			}});
		} else {
			// под вопросом, нужен ли
			// берем только соответствие id и валюты
			travelActions.listModelGet();
			travelActions.travelPolicyInformationGet(currentQuotationGuid);
		}

	}

	state = {
		defaultEmail: '',
		isEmailChanging: false,
	}

	onDownloadClick = () => {
		const { travelActions, orderInfo } = this.props;
		travelActions.travelPolicyDownload(orderInfo.quotationGuid);
		 
		// listPolicyFileUrl.map(item => {
		// 	window.open(item, '_blank');
		// });
	}

	onCreateNewInsuranceClick = () => {
		const { travelActions } = this.props;
		travelActions.createNewInsurance();
		history.push(frameManager.url('travel/insurance'));
	}

	onEditClick = () => {
		const { policyModel } = this.props;
		this.setState({ isEmailChanging: true, defaultEmail: policyModel.insurer.email });
	}
	/**
	 * Отправка полиса на новый email
	 */
	onChangeEmailClick = () => {
		const { travelActions, policyModel, orderInfo } = this.props;
		if (policyModel.insurer.email == this.state.defaultEmail) {
			this.setState({ isEmailChanging: false });
			return;
		}
		Utils.validation.validate().then(() => {
			travelActions.policyPrint(orderInfo.quotationGuid, policyModel.insurer.email);
			this.setState({ isEmailChanging: false, defaultEmail: policyModel.insurer.email });
		});
	}

	onEmailChange = (event) => {
		this.props.travelActions.travelPolicySetEmail(event);
	}

	logoSizeModifier(insuranceCompanyId: number): string {
		let insuranceCompanyName = (enumInsuranceCompany.get(insuranceCompanyId) as any).name;
		return `travel-response__company-logo--${insuranceCompanyName}` 
    }

	render() {
		const { onDownloadClick, onEmailChange, onChangeEmailClick, onEditClick, onCreateNewInsuranceClick } = this;
		const { isEmailChanging } = this.state;
		const { policyModel, isLoaded, dictionaryModel, isPolicyAccepted } = this.props;

		if (!isLoaded || !policyModel || !dictionaryModel) return <SectionPreloader />;

		const { country, isMultiple, startDate, endDate, coveredDay, insured, risk, currencyId, price, insurer, insuranceCompany } = policyModel;
		const { isMailSend } = insuranceCompany;

		// определяем валюту страхования
		let currency;
		dictionaryModel.listCurrency.forEach(item => {
			if (item.id == currencyId) currency = item.name;
		});

		return (
			<div className='travel-response'>
				<div className='container'>
					<div className='travel-response__wrapper'>
						<div className='travel-response__policy-info'>
							<h1 className='travel-response__header' >Поздравляем, <br />ваша страховка оформлена!</h1>
							<div>
								<div className='travel-response__policy-info-block'>
									<div className='travel-response__policy-info-title'>Страны путешествия</div>
									<div className='travel-response__policy-info-value'>
										{country.map((item, index) => {
											return <span key={`country${index}`}>{item.name}{index != country.length - 1 && ', '}</span>
										})}
									</div>
								</div>
								<div className='travel-response__policy-info-block'>
									<div className='travel-response__policy-info-title'>{isMultiple ? 'Годовой полис' : 'Даты поездки'}</div>
									<div className='travel-response__policy-info-value'>
										{isMultiple ? `c ${moment(startDate).format('D MMM YYYY')} (${coveredDay} ${Utils.text.getWordForm(coveredDay, ['страховой день', 'страховых дня', 'страховых дней'])})`
											:
											`c ${moment(startDate).format('D MMM YYYY')} по ${moment(endDate).format('D MMM YYYY')}`}
									</div>
								</div>
								<div className='travel-response__policy-info-block'>
									<div className='travel-response__policy-info-title'>
										{insured.length > 1 ? 'Путешественники' : 'Путешественник'}
									</div>
									<div className='travel-response__policy-info-value'>
										{`${insured.length} ${Utils.text.getWordForm(insured.length, ['человек', 'человека', 'человек'])}: `}
										{insured.map((item, index) => {
											if (index != insured.length - 1) return `${item.age}, `
											return `${item.age} `;
										})}
										{Utils.text.getWordForm(insured[insured.length - 1].age, ['год', 'год', 'лет'])}
									</div>
								</div>
								<div className='travel-response__policy-info-block'>
									<div className='travel-response__policy-info-title'>Страховая сумма</div>
									<div className='travel-response__policy-info-value'>
										{risk.map((item, index) => {
											if (item.id == 1) {
												return <div key={`summ${index}`} className='selected-insurance__parameter'>{Utils.text.numberWithSpaces(item.coverageSum)} {CurrencyIcon[currency]}</div>
											}
										})}
									</div>
								</div>
								<div className='travel-response__policy-info-block'>
									<div className='travel-response__policy-info-title'>Стоимость полиса</div>
									<Price className='travel-response__policy-info-value' currency={CurrencyIcon['RUB']}>{price}</Price>
								</div>
							</div>
							<div className='travel-response__buttons'>
								{isPolicyAccepted && <Button className='travel-response__download-button' onClick={onDownloadClick}><i className='icon-download'></i>Скачать полис</Button>}
								<Button className='travel-response__new-insurance-button' theme='secondary' onClick={onCreateNewInsuranceClick}>Создать новый расчет</Button>
							</div>
							<div className='travel-response__email-label'>Файл страховки отправится на почту:</div>
							{isEmailChanging ?
								<div className='travel-response__edit-email'>
									<Validator listRule={[{ name: 'required' }, { name: 'email' }]} validationIndex={0}>
										<Field id='email' name='email' value={insurer.email} label='Новый email' onChange={onEmailChange}  className='travel-response__edit-field'/>
									</Validator>
									<Button size='small' className='button--170' onClick={onChangeEmailClick}>Сохранить</Button>
								</div>
								:
								<div className='travel-response__insurer-email'>
									<div className='link'>{insurer.email}</div>
									{isMailSend && <div className='icon-edit' onClick={onEditClick}></div>}
								</div>
							}
						</div>
						<div className='travel-response__insurance-company-info'>
							<div className={`travel-response__company-logo ${this.logoSizeModifier(insuranceCompany.id)}`}>
								<img src={Utils.text.getInsuranceLogoById(insuranceCompany.id)} alt=""/>
							</div>							
							<div className='travel-response__insurance-company-phone'>{insuranceCompany.phone}</div>
							<a href={`mailto:${insuranceCompany.email}`} className='link' >{insuranceCompany.email}</a>
							<img src={require('img/final.svg')} alt=""  className='travel-response__insurance-pic'/>
							<div className='travel-response__insurance-message'>Если вы не получили письмо<br/> со страховкой в течение одного часа, свяжитесь с «{insuranceCompany.name}»</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}


const mapStateToProps = (state, ownProps) => ({
	policyModel: state.travel.policyModel,
	isLoaded: state.travel.isLoaded,
	dictionaryModel: state.travel.dictionaryModel,
	orderInfo: state.travel.orderInfo,
	orderModel: state.travel.orderModel,
	isPolicyAccepted: state.travel.isPolicyAccepted
});

const mapDispatchToProps = (dispatch: any, ownProps) => ({
	travelActions: bindActionCreators(travelActions, dispatch)
});

export const ConnectedTravelResponse = connect(mapStateToProps, mapDispatchToProps)(TravelResponse);