import { TravelActions } from '../../../store/actions/travel.actions';
// import { ITravelListModel } from '../../../store/logic/travel/interfaces/ITravelListModel';
import { ITravelResultListModel } from '../../../store/logic/travel/interfaces/ITravelResultListModel';
import { IPolicyModel } from '../../../store/logic/travel/interfaces/IPolicyModel';
import { ITravelListModel } from '../../../store/logic/travel/interfaces/ITravelListModel';
import { IOrderModel } from '../../../store/logic/travel/interfaces/ITravelModel';



export interface ITravelResponseProps {
    isLoaded: boolean;
    policyDTO: ITravelResultListModel;
    // listModel: ITravelListModel;
    isPolicySending: boolean;
    policyModel: IPolicyModel;
    dictionaryModel: ITravelListModel;
    orderInfo: { quotationGuid: string };
    orderModel: IOrderModel;
    isPolicyAccepted: boolean
}

export interface ITravelResponseDispatchProps {
    /**
     * Гет полиса 
     * @param {string} orderGuid 
     * @returns {*}  
     * @memberOf ITravelResponseDispatchProps
     */
    travelPolicyGet(orderGuid : string) : any;
    /**
     * Сеттер для policyDTO 
     * @param {string} name 
     * @param {*} value 
     * @returns {*} 
     * @memberOf ITravelResponseDispatchProps
     */
    travelPolicyDTOset(name : string, value : any) : any;
    /**
     * Показать индикатор отправки
     * @returns {*}
     * @memberof ITravelResponseDispatchProps
     */
    travelPolicySendEmail() : any;
    /**
     * Отправка полиса на новый email
     * @param {policySendModel} payload 
     * @returns {*} 
     * 
     * @memberOf ITravelResponseDispatchProps
     */
    travelPolicySend(payload: policySendModel): any;

    travelActions: TravelActions;

}

export interface policySendModel {
    orderGuid: string;
    email: string;
}