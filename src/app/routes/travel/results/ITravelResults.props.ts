import { TravelActions } from '../../../store/actions/travel.actions';
// import { ITravelListModel } from '../../../store/logic/travel/interfaces/ITravelListModel';
import { IResultCalculateModel, ITravelModel } from '../../../store/logic/travel/interfaces/ITravelModel';
import { ITravelState } from '../../../store/reducers/travel/travel';
import { ITravelListModel, ITravelRiskModel } from '../../../store/logic/travel/interfaces/ITravelListModel';


export interface ITravelResultsProps extends ITravelState { 
    
    
}

export interface ITravelResultsState {
    sortTypeId: number;
	/**
	 * @desc риски, которые отображаются в блоках с возможностью поменять сумму покрытия.
	 */
	listCoverageRisk: ITravelRiskModel[];
    
    /**
     * @desc риски, которые отображаются чекбоксами с тултипами.
     */
    listOptionRisk: ITravelRiskModel[];
    
    /**
     * @desc
     */

    isShowEveryCoverageRisk: boolean;
    /**
     * @desc
     */

    isShowEveryOptionRisk: boolean;
    /**
     * @desc общее окличество загружаемых страховых и число уже загруженых страховых.
     */
    resultLoadData: { 
        loaded: number, 
        total:number 
    };

    isСountriesRowFull: boolean;

    isСountriesExpanded: boolean;
}

export interface ITravelResultsDispatchProps {
    /**
     * Запрос результатов
     * @returns {*} 
     * @memberOf ITravelResultsDispatchProps
     */
    travelSingleQuoteGet?(id): any;

    travelActions : TravelActions;
}