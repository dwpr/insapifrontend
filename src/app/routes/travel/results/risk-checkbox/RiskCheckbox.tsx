import { Tooltip } from '../../../../components/tooltip/Tooltip';
import './RiskCheckbox.style.scss';
//import { IRiskCheckboxProps, IRiskCheckboxState } from './IRiskCheckbox';
import { Price } from '../../../../components/price/Price';

/**
 * @desc возвращает нужный класс icomon для данного риска.
 * @param riskId id риска
 * допустимые id : 10, 8, 5, 3, 4, 9, 12
 */
const getLogoClassName = (riskId: number) => {
    let className : string = 'icon-';

    switch(riskId) {
        //Активный отдых
        case 2: {
            className = className + 'active';
            break;
        }
        //Гражданская ответственность
        case 3: {
            className = className + 'handshake';
            break;
        }
        //Отмена поездки
        case 4: {
            className = className + 'ticket';
            break;
        }
        //Несчастный случай
        case 5: {
            className = className + 'mountain';
            break;
        }
        //Любительский спорт
        case 6: {
            className = className + 'bike';
            break;
        }
        //Профессиональный спорт
        case 7: {
            className = className + 'sport';
            break;
        }
        //Утрата богажа
        case 8: {
            className = className + 'baggage';
            break;
        }
        //юридическая помощь
        case 9: {
            className = className + 'lawyer';
            break;
        }
        //Задержка рейса
        case 10: {
            className = className + 'delay';
            break;
        }
        //Алкогольное опьянение
        case 11: {
            className = className + 'alcohol';
            break;
        }
        //беременность
        case 12: {
            className = className + 'pregnancy';
            break;
        }
        //Хронические заболевания
        case 13: {
            className = className + 'illness';
            break;
        }
        //Экстремальный спорт
        case 14: {
            className = className + 'extreme_sport';
            break;
        }
    }

    return className;
}
export class RiskCheckbox extends React.Component<any> {

    public static defaultProps : any = {
        riskModel: null,
        currency: null,
        onChangeSum: null,
        onChange: null,
        isActive: false,
        disabled: false,
        defaultCoverageSum: null
    }


    public state: any = {
        sum: 0
    }

    /**
     * @desc если этот риск активировн при маунте, есть defaultCoverageSum и в списке сумм покрытий есть значение, 
     * которое совпадает с defaultCoverageSum, то устанавливаем выбранную сумму в это значение.
     * В ином случае, просто устанавливаем сумму на самую маленькую из сумм покрытия
     */
    componentWillMount() {
        let { isActive, defaultCoverageSum, riskModel } = this.props;

        if(isActive && defaultCoverageSum && riskModel.listCoverageSum.some(sum => sum == defaultCoverageSum)) {
            this.setState({ sum: defaultCoverageSum });
        } else {
            this.setSumToLowest();
        }
    }

    setSumToLowest = () => {
        this.setState({ sum: this.props.riskModel.listCoverageSum[0] });
    }

    onToggle = () => {
        if(this.props.disabled) return null;

        let { isActive, riskModel } = this.props;

        if(isActive) { this.setSumToLowest(); }
        
        this.props.onChange({ value: !isActive, id: riskModel.id });
    }

    decreaseCoverage = (event) => {
        if(this.props.disabled) return null;

        event.stopPropagation();

        let { currentCoverageSumIndex } = this;
        let { riskModel } = this.props;

        if(currentCoverageSumIndex == 0) return;

        let newSum = riskModel.listCoverageSum[currentCoverageSumIndex - 1];

        this.setState({sum: newSum});
        this.props.onChangeSum({ id: riskModel.id, value: newSum });
    }

    increaseCoverage = (event) => {
        if(this.props.disabled) return null;

        event.stopPropagation();

        let { currentCoverageSumIndex } = this;
        let { riskModel } = this.props;

        if(currentCoverageSumIndex == (riskModel.listCoverageSum.length - 1)) return;

        let newSum = riskModel.listCoverageSum[currentCoverageSumIndex + 1];

        this.setState({sum: newSum});
        this.props.onChangeSum({ id: riskModel.id, value: newSum });
    }

    get currentCoverageSumIndex() {
        return this.props.riskModel.listCoverageSum.findIndex(riskSum => riskSum === this.state.sum);
    }

    public render() {
        let { riskModel, currency, isActive, disabled } = this.props;
        let { onToggle, decreaseCoverage, increaseCoverage } = this;
        let { sum } = this.state;

        return (
            <div className={`risk-checkbox ${isActive ? 'risk-checkbox--checked' : ''} ${disabled ? 'risk-checkbox--disabled' : ''} ${riskModel.listCoverageSum.length > 1 ? 'risk-checkbox--with-sum' : ''}`} onClick={onToggle}>
                  <div className='risk-checkbox__flex-wrap'>
                    <div className="risk-checkbox__heading">
                        {/* каждое слово в названии риска на новой строке */}
                        <div className='risk-checkbox__title'>
                            {riskModel.name.split(' ').map((nameWord, index) => (
                                <div key={`riskName${index}`} className='risk-checkbox__name-row'>
                                    {nameWord}
                                    {riskModel.description && index == 0 && <Tooltip tooltip={riskModel.description}  tooltipPosition='top-left'/>}
                                </div>
                            ))}
                        </div>
                    </div>
                    <i className={'risk-checkbox__logo ' + getLogoClassName(riskModel.id)}></i>
                </div>
                { riskModel.listCoverageSum.length > 1 && (
                    <div className="risk-checkbox__coverage">
                        <div className={`risk-checkbox__coverage-with-controls ${isActive && sum ? 'risk-checkbox__coverage-with-controls--visible' : ''}`}>
                            <div className='risk-checkbox__decrease-coverage' onClick={decreaseCoverage}></div>
                            <Price noDecimal={true} currency={currency}>{sum}</Price>
                            <div className='risk-checkbox__increase-coverage' onClick={increaseCoverage}></div>
                        </div>
                        <div className={`risk-checkbox__coverage-from ${!isActive && riskModel.listCoverageSum[0] ? 'risk-checkbox__coverage-from--visible' : ''}`}>
                            от <Price noDecimal={true} currency={currency} >{riskModel.listCoverageSum[0]}</Price>
                        </div>

                        {/* {
                            !isActive && riskModel.listCoverageSum[0] && (
                                <div className="risk-checkbox__coverage-from">
                                    от <Price noDecimal={true} currency={currency} >{riskModel.listCoverageSum[0]}</Price>
                                </div>
                            )
                        } */}
                        
                    </div>
                ) }
            </div>
        )
    }
}

// { isActive && <div className='risk-checkbox__decrease-coverage' onClick={decreaseCoverage}>-</div> } 
// { currencyId == 1 ? 'USD' : 'EUR' } 
// { isActive && <div className='risk-checkbox__increase-coverage' onClick={increaseCoverage}>+</div> } 