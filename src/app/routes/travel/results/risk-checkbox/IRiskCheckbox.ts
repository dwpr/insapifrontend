import { ITravelRiskModel } from '../../../../store/logic/travel/interfaces/ITravelListModel';


export interface IRiskCheckboxProps {
    riskModel: ITravelRiskModel;
    //id используемой валюты (доллар или евро)
    currency: string | JSX.Element;
    onChangeSum: any;
    onChange: any;
    //активен ли блок
    isActive: boolean;
    disabled?:boolean;
}

export interface IRiskCheckboxState {
    sum: number;
}