import { CountryBlock } from '../../../components/country-block/CountryBlock';
import { Switcher } from '../../../components/switcher/Switcher';
import { IResultCalculateModel, IRiskModel } from '../../../store/logic/travel/interfaces/ITravelModel';
import { ResultBlock } from '../../../components/result-block/ResultBlock';
import { ITravelRiskModel, ICountryModel } from '../../../store/logic/travel/interfaces/ITravelListModel';
import { Tooltip } from '../../../components/tooltip/Tooltip';
import { Checkbox } from '../../../components/checkbox/Checkbox';
import { ITravelResultsProps, ITravelResultsDispatchProps, ITravelResultsState } from "./ITravelResults.props";
import { IRootState } from "../../../store/reducers";
import { bindActionCreators } from "redux";
import { travelActions } from "../../../store/actions/travel.actions";
import { connect } from "react-redux";
import { RiskCheckbox } from './risk-checkbox/RiskCheckbox';
import { Button } from "../../../components/button/Button";
import { PlanePreloader } from '../../../components/plane-preloader/PlanePreloader';
import { quotationCalculateMock } from '../mock-get';
import { ExpandSelect } from '../../../components/expand-select/ExpandSelect';
import { history } from '../../../app';
import * as moment from 'moment';
import { CurrencyIcon } from '../../../components/price/Price';
import { ExpandBtn } from '../../../components/expand-btn/ExpandBtn';
import { SmoothPlanePreloader } from '../../../components/smooth-plane-preloader/SmoothPlanePreloader';






export class TravelResults extends React.Component<ITravelResultsProps & ITravelResultsDispatchProps> {

	state: ITravelResultsState = {
		sortTypeId: 0,
		listCoverageRisk: [],
		listOptionRisk: [],
		isShowEveryCoverageRisk: false,
		isShowEveryOptionRisk: false,
		resultLoadData: {
			total: 0,
			loaded: 0
		},
		isСountriesExpanded: false,
		isСountriesRowFull: false
	}

	async componentWillMount() {

		let { dictionaryModel, travelActions } : ITravelResultsProps & ITravelResultsDispatchProps = this.props;

		let recalculate = this.recalculate;

		let listCoverageRisk, listOptionRisk, resultLoadData;

		if (!dictionaryModel) return history.push(frameManager.url('travel/insurance'));

		listCoverageRisk = Utils.array.generateListByListId(dictionaryModel.listTravelRisk, [10, 8, 5, 3, 4, 9]);
		listOptionRisk = Utils.array.generateListByListId(dictionaryModel.listTravelRisk, [2, 7, 6, 11, 12, 13]);

		this.setState({ listCoverageRisk, listOptionRisk });
		await travelActions.listModelGet();
		recalculate();
	}

	/**
	 * @desc функция является колбеком, который вызывается при пересчете каждой страховой. Нужна для правильного отображения прелоадера.
	 */
	handleCalculateProgress = (loaded: number) => {
		let { resultLoadData } = this.state;
		this.setState({ resultLoadData: { ...resultLoadData, loaded } });
	}

	listSortType = [
		{ id: 0, name: 'По стоимости' },
		{ id: 1, name: 'По страховщику' }
	]

	get sortedResultModel() {
		let sortTypeId = this.state.sortTypeId;

		let listResultCalculateModel = this.props.listResultCalculateModel;

		let sortedResultModel = { listSuccessResult: [], listLoadingResult: [], listFailureResult: [] };

		if (!listResultCalculateModel.length) return sortedResultModel;

		let listSuccessResult = listResultCalculateModel.filter(model => (model.isSuccess && !model.isLoading));

		let listLoadingResult = listResultCalculateModel.filter(model => model.isLoading);

		let listFailureResult = listResultCalculateModel.filter(model => (!model.isSuccess && !model.isLoading));

		listLoadingResult.sort((prevResultModel, nextResultModel) => {
			if (prevResultModel.name.toLowerCase() > nextResultModel.name.toLowerCase()) return 1;
			if (prevResultModel.name.toLowerCase() < nextResultModel.name.toLowerCase()) return -1;
		});

		listFailureResult.sort((prevResultModel, nextResultModel) => {
			if (prevResultModel.name.toLowerCase() > nextResultModel.name.toLowerCase()) return 1;
			if (prevResultModel.name.toLowerCase() < nextResultModel.name.toLowerCase()) return -1;
		});

		//по алфавиту
		if (sortTypeId == 1) {
			listSuccessResult.sort((prevResultModel, nextResultModel) => {
				if (prevResultModel.name.toLowerCase() > nextResultModel.name.toLowerCase()) return 1;
				if (prevResultModel.name.toLowerCase() < nextResultModel.name.toLowerCase()) return -1;
			});
		}

		//по стоимости
		if (sortTypeId == 0) {
			listSuccessResult.sort((prevResultModel, nextResultModel) => {
				if (prevResultModel.price > nextResultModel.price) return 1;
				if (prevResultModel.price < nextResultModel.price) return -1;
			});
		}

		sortedResultModel = { listSuccessResult, listLoadingResult, listFailureResult };

		return sortedResultModel;
	}

	SortButton = (sortTypeId) => {
		let buttonText = this.listSortType[sortTypeId].name;

		return (
			<div className="sort-button">
				{buttonText}
				<span className="sort-button__arrow"></span>
			</div>
		)
	}

	/**
	 * @desc если риск уже в модели, убираем его, если в модели его нет - добавляем.
	 */
	onRiskToggle = (riskModel: ITravelRiskModel, isActive: boolean) => {
		let { addRisk, removeRisk } = this.props.travelActions;
		// debugger

		if (isActive) {
			addRisk({ riskModel });
		} else {
			removeRisk({ id: riskModel.id });
		}

		this.recalculate()
	}

	changeRiskCoverage = ({ id, coverageSum }) => {
		this.props.travelActions.changeRiskCoverage({ id, coverageSum });

		this.recalculate();
	}

	/**
	 * @обычный сеттер в модель travel.model
	 */
	public set = (event) => {
		let { isListCalculateResultLoading, recalculate } = this;

		// во время выполнения расчета можно менять опции => убрала эту проверку
		// if (isListCalculateResultLoading) return;

		this.props.travelActions.set(event);
		recalculate();
	}

	recalculate = () => {
		let { travelActions } = this.props;

		this.setState({ resultLoadData: { total: this.props.dictionaryModel.listInsuranceCompany.length, loaded: 0 } });
		travelActions.calculateListResult(this.handleCalculateProgress);
	}

	onIssuePolicy = (resultCalculateModel: IResultCalculateModel) => {
		let { travelActions } = this.props;

		travelActions.issuePolicy(resultCalculateModel);

		history.push(frameManager.url('travel/order'));
	}

	onPrevStepClick = () => {
		history.push(frameManager.url('travel/insurance'));
	}

	public currencyOptionRenderer = (optionLabel) => {
		return CurrencyIcon[optionLabel];
	}

	/**
	 * @desc возвращает true, если котировка минимум одной страховой находится в процессе загрузки.
	 */
	get isListCalculateResultLoading() {
		return this.props.listResultCalculateModel.some((resultCalculateModel: IResultCalculateModel) => resultCalculateModel.isLoading);
	}

	get isListCalculateResultSuccess() {
		return this.props.listResultCalculateModel.some((resultCalculateModel: IResultCalculateModel) => resultCalculateModel.isSuccess);
	}

	get listFilteredCoverageRisk() {
		let { isShowEveryCoverageRisk, listCoverageRisk } = this.state;

		if (this.state.isShowEveryCoverageRisk) return listCoverageRisk;

		return listCoverageRisk.slice(0, 3);
	}

	get listMainOptionRisk() {
		return this.state.listOptionRisk.slice(0, 3);
	}

	get listAdvancedOptionRisk() {
		return this.state.listOptionRisk.slice(3);
	}

	get medicalExpensesCoverage() {
		return Number(this.props.model.risk.find(({ id }) => id == 1).coverageSum);
	}

	/**
	 * @desc вернет true, если хотя бы одна из выбранных на 1 шаге стран входит в Шенгенскую зону
	 */
	get isSchengenCountryChoosed() {
		let { dictionaryModel, model } = this.props;
		//пока захардкодил "Шенген"
		let listSchengenCountry = dictionaryModel.listCountry.filter((countryModel: ICountryModel) => countryModel.isShengen);

		let result: boolean = listSchengenCountry.some(schengenCountryModel => model.country.some(choosedCountryModel => schengenCountryModel.id == choosedCountryModel.id));

		return result;
	}

	onClickCountries = () => {
		this.setState({ isСountriesExpanded: !this.state.isСountriesExpanded })
	}

	countriesWrap;

	componentDidMount() {

		let wrapWidth, countriesElementsArr;

		let innerRowWidth = 0;

		const HTMLCollectionToArray = (HTMLCollection) => Array.prototype.slice.call(HTMLCollection);

		// обернуто в if для случая, когда пользователь обновил страницу и мы редиректим его на 1 шага, но componentDidMount все равно отрабатывает

		if (this.countriesWrap) {
			wrapWidth = this.countriesWrap.clientWidth;
			countriesElementsArr = HTMLCollectionToArray(this.countriesWrap.children);

			countriesElementsArr.map(item => {
				innerRowWidth += item.clientWidth + 24;
				if (innerRowWidth > wrapWidth - 30) {
					item.classList.add('results-header__item--hidden');
				}
			});

			if (innerRowWidth > wrapWidth - 30) {
				this.setState({ isСountriesRowFull: true })
			}
		}
	}

	/**
	 * @desc возвращает сумму покрытия у данного риска из списка выбранных 
	 */
	getChoosedRiskCoverageSum = (riskModel: ITravelRiskModel) => {
		let sum: number = null;

		let choosedRisk: IRiskModel = this.props.model.risk.find((choosedRiskModel: IRiskModel) => choosedRiskModel.id == riskModel.id);

		if (choosedRisk) { sum = choosedRisk.coverageSum };

		return sum;
	}

	public render() {
		let { model, dictionaryModel, isLoaded, listResultCalculateModel, travelActions } = this.props;

		if (!isLoaded || !dictionaryModel) return null;

		let { isShowEveryCoverageRisk, isShowEveryOptionRisk, sortTypeId, isСountriesExpanded, isСountriesRowFull } = this.state;
		let { isSchengenCountryChoosed, onPrevStepClick, isListCalculateResultLoading,
			currencyOptionRenderer, changeRiskCoverage, listFilteredCoverageRisk, listMainOptionRisk,
			listAdvancedOptionRisk, onRiskToggle, medicalExpensesCoverage,
			sortedResultModel, listSortType, onIssuePolicy, set, onClickCountries, getChoosedRiskCoverageSum } = this;

		let total = this.state.resultLoadData.total;
		let loaded = this.state.resultLoadData.loaded;

		return (
			<div className='travel-results'>

				{/* 1. Данные с прошлого шага */}

				<div className={`results-header ${isСountriesExpanded ? 'results-header--countries-expanded' : ''}`}>
					<div className='container'>
						<div className="results-header__flex-wrap">
							<div className="results-header__wrap">
								<div className="results-header__title">{model.country.length < 2 ? 'Страна путешествия' : 'Страны путешествия'}</div>
								<ul className={`results-header__item-list ${isСountriesExpanded ? 'results-header__item-list--expanded' : ''}`} ref={(node) => { this.countriesWrap = node; }}>
									{
										model.country.map(countryModel => (
											<li className='results-header__item' key={countryModel.id}>
												<CountryBlock countryId={countryModel.id} listCountry={dictionaryModel.listCountry} />
											</li>
										))
									}
									{
										isСountriesRowFull && <li className='results-header__expand-btn-wrap'>
											<i className='icon-arrow-down' onClick={this.onClickCountries} />
										</li>
									}
								</ul>
							</div>
							<div className="results-header__wrap">
								<div className="results-header__title">{model.isMultiple ? 'Годовой полис' : 'Период страхования'}</div>
								<div className="results-header__term">
									<i className="icon-calendar"></i>
									{
										model.isMultiple ?
											(
												<div className="results-header__term-info-period">
													с {moment(model.startDate).format('ll').slice(0, -3)} ({model.coveredDay} {Utils.text.getWordForm(model.coveredDay, ['страховой', 'страховых', 'страховых'])} {Utils.text.getWordForm(model.coveredDay, ['день', 'дней', 'дней'])})
											</div>
											) :
											(
												<div className="results-header__term-info-year">
													<div className='results-header__term-info-year-dates'>
														{Utils.date.getPeriodDate(model.startDate)} &mdash; {Utils.date.getPeriodDate(model.endDate)}
													</div>
													<div className="results-header__term-info-year-days">
														({model.coveredDay} {Utils.text.getWordForm(model.coveredDay, ['страховой', 'страховых', 'страховых'])} {Utils.text.getWordForm(model.coveredDay, ['день', 'дней', 'дней'])})
												</div>
												</div>
											)
									}
								</div>
							</div>
							<div className="results-header__wrap">
								<div className="results-header__title">{model.insured.length + ' ' + Utils.text.getWordForm(model.insured.length, ['путешественник', 'путешественника', 'путешественников'])}</div>
								<div className="results-header__travelers">
									<i className="icon-adult"></i>
									<div className="results-header__age">{model.insured.map((item, index) => index == model.insured.length - 1 ? (item.age + ' ' + Utils.text.getWordForm(Number(item.age), ['год', 'года', 'лет'])) : item.age + ', ')}</div>
								</div>
							</div>
							<div className="results-header__to-previous-btn" onClick={onPrevStepClick}>
								<i className='icon-uniE91C'></i>
							</div>
						</div>
					</div>
				</div>

				{/* 2. прелоадер загрузки результатов расчета */}
				<div className='travel-results__plane'>
					{/* <PlanePreloader total={this.state.resultLoadData.total} loaded={this.state.resultLoadData.loaded} /> */}
					<SmoothPlanePreloader total={this.state.resultLoadData.total} loaded={this.state.resultLoadData.loaded} isNeedResetLoading={this.props.isRequestsAborted} onResetLoading={() => this.props.travelActions.resetLoading()} />
				</div>

				<div className='travel-results__main'>
					<div className='container'>

						{/* Если хотябы одна страховая предоставила успешных результат, показываем кнопку сортировки */}
						{							
							listResultCalculateModel.some(({ isLoading, isSuccess }) => (isLoading || isSuccess)) && (
								<ExpandSelect buttonText={listSortType[sortTypeId].name}
									listOption={listSortType}
									isDisabled={!this.isListCalculateResultSuccess}
									onOptionSelect={({ id }) => this.setState({ sortTypeId: id })}
									className={`travel-results__sort-results ${!this.isListCalculateResultSuccess ? 'travel-results__sort-results--disabled' : ''}`} />
							)
						}

						{/* <ExpandSelect buttonText={listSortType[sortTypeId].name}
									listOption={listSortType}
									onOptionSelect={({ id }) => this.setState({ sortTypeId: id })}
									className='travel-results__sort-results' /> */}


						{/* если не найдено страховок, показываем текст */}
						{
							listResultCalculateModel.every(({ isLoading, isSuccess }) => (!isLoading && !isSuccess)) && (
								<p className="travel-results__not-found">
									Мы не смогли подобрать подходящие варианты.  Попробуйте изменить даты поездки, сумму медицинского страхования или дополнительные опции
								</p>
							)
						}

						<div className="travel-results__flex-wrap">
							<div className="travel-results__results-list">
								{
									sortedResultModel.listSuccessResult.map((resultModel: IResultCalculateModel) => {

										let { calculationModel, price, specialCondition, isLoading, isSuccess, insuranceCompanyId, docLink, assistance } = resultModel;

										return (
											<ResultBlock calculationModel={calculationModel}
												price={price}
												specialCondition={specialCondition}
												onIssue={() => onIssuePolicy(resultModel)}
												isLoading={isLoading}
												isSuccess={isSuccess}
												insuranceCompanyId={insuranceCompanyId}
												key={insuranceCompanyId}
												docLink={docLink}
												assistance={assistance}
												listCountry={dictionaryModel.listCountry}
												getInsuranceRule={travelActions.insuranceRules}
												getInsuranceSample={travelActions.insurancePolicySample}
											/>
										)
									}
									)
								}

								{
									!isListCalculateResultLoading && sortedResultModel.listSuccessResult.length < 4 && sortedResultModel.listSuccessResult.length > 0 &&
									<div className='travel-results__change-search'>
										Чтобы получить больше предложений, попробуйте изменить даты поездки, сумму
										медицинского страхования или дополнительные опции
									</div>
								}

								{
									sortedResultModel.listLoadingResult.map((resultModel: IResultCalculateModel) => {

										let { calculationModel, price, specialCondition, isLoading, isSuccess, insuranceCompanyId, docLink, assistance } = resultModel;

										return (
											<ResultBlock calculationModel={calculationModel}
												price={price}
												specialCondition={specialCondition}
												onIssue={() => onIssuePolicy(resultModel)}
												isLoading={isLoading}
												isSuccess={isSuccess}
												insuranceCompanyId={insuranceCompanyId}
												key={insuranceCompanyId}
												docLink={docLink}
												assistance={assistance}
												listCountry={dictionaryModel.listCountry}
												getInsuranceRule={travelActions.insuranceRules}
												getInsuranceSample={travelActions.insurancePolicySample}
											/>
										)
									}
									)
								}

								{
									sortedResultModel.listFailureResult.map((resultModel: IResultCalculateModel) => {

										let { calculationModel, price, specialCondition, isLoading, isSuccess, insuranceCompanyId, docLink, assistance } = resultModel;

										return (
											<ResultBlock calculationModel={calculationModel}
												price={price}
												specialCondition={specialCondition}
												onIssue={() => onIssuePolicy(resultModel)}
												isLoading={isLoading}
												isSuccess={isSuccess}
												insuranceCompanyId={insuranceCompanyId}
												key={insuranceCompanyId}
												docLink={docLink}
												assistance={assistance}
												listCountry={dictionaryModel.listCountry}
												getInsuranceRule={travelActions.insuranceRules}
												getInsuranceSample={travelActions.insurancePolicySample}
											/>
										)
									}
									)
								}
							</div>

							{/* 4. Список добавочных конфигураций. */}

							<div className="travel-results__configuration">

								{/* Страховая сумма */}

								{/* <div className={`travel-results__configuration-block ${isListCalculateResultLoading ? 'travel-results__configuration-block--disabled' : ''}`}> */}
								<div className={`travel-results__configuration-block`}>
									<div className="travel-results__choose-sum">
										<div className="travel-results__configuration-block-heading">Страховая сумма</div>
										<div className='travel-results__currency-choose'>
											<Switcher id='coverage-type'
												name='currencyId'
												value={model.currencyId}
												//метод reverse() меняет исходный массив, поэтому необходимо разворачивать именно новый массив, а не тот что лежит в сторе.
												options={[...dictionaryModel.listCurrency].reverse().map(({ id, name }) => ({ label: name, value: id, disabled: id == 1 && isSchengenCountryChoosed, tooltip: id == 1 && isSchengenCountryChoosed && 'В странах Шенгенского союза страхование осуществляется только в евро' }))}
												onChange={set}
												optionRenderer={currencyOptionRenderer}
												className='switcher--currency' />
										</div>
									</div>

									<div className="travel-results__coverage-choose">
										<Switcher id='coverage-sum-choose'
											name='coverage-sum'
											options={dictionaryModel.listTravelRisk[0].listCoverageSum.map((sum, indexSum) => ({ label: Utils.text.numberWithSpaces(sum) + CurrencyIcon[model.currencyId == 1 ? 'USD' : 'EUR'], value: indexSum }))}
											value={dictionaryModel.listTravelRisk[0].listCoverageSum.findIndex(sum => sum == model.risk[0].coverageSum)}
											onChange={({ value }) => changeRiskCoverage({ id: 1, coverageSum: dictionaryModel.listTravelRisk[0].listCoverageSum[value] })} />
									</div>

									{
										listFilteredCoverageRisk.map(riskModel => {
											let isActive = Utils.array.checkIsObjectInListById(model.risk, riskModel.id);

											return (
												<RiskCheckbox
													riskModel={riskModel}
													currency={model.currencyId == 1 ? CurrencyIcon.USD : CurrencyIcon.EUR}
													onChangeSum={({ id, value }) => changeRiskCoverage({ id, coverageSum: value })}
													onChange={(payload) => onRiskToggle(riskModel, payload.value)}
													isActive={isActive}
													key={riskModel.id}
													defaultCoverageSum={isActive ? getChoosedRiskCoverageSum(riskModel) : null}
												/>
											)
										})
									}

									{!isShowEveryCoverageRisk && (
										<div className="travel-results__show-all-coverage-risk" onClick={() => this.setState({ isShowEveryCoverageRisk: true })}>
											<i className='icon-arrow-down'></i>  Сделайте вашу страховку еще лучше
										</div>
									)}

									{/* виды отдыха для страхования */}

									<div className="travel-results__configuration-rest-types">
										<span className='travel-results__configuration-kinds'>Виды отдыха</span>
										{
											listMainOptionRisk.map(riskModel => {
												let isActive = Utils.array.checkIsObjectInListById(model.risk, riskModel.id);

												return (
													<div className='travel-results__risk-checkbox' key={riskModel.id}>
														<Checkbox id={riskModel.id.toString()}
															name={riskModel.id.toString()}
															value={isActive}
															onChange={(payload) => { onRiskToggle(riskModel, payload.value) }}
															label={riskModel.name}>
														</Checkbox>

														{riskModel.description && <Tooltip tooltip={riskModel.description} tooltipPosition='top-left' />}

													</div>
												)
											})
										}

										<ExpandBtn className='travel-results__show-advanced-risk-btn' text='Показать все опции' isExpanded={isShowEveryOptionRisk} onClick={() => this.setState({ isShowEveryOptionRisk: !isShowEveryOptionRisk })} />

										{
											isShowEveryOptionRisk && listAdvancedOptionRisk.map(riskModel => {
												let isActive = Utils.array.checkIsObjectInListById(model.risk, riskModel.id);

												return (
													<div className='travel-results__risk-checkbox' key={riskModel.id}>
														<Checkbox id={riskModel.id.toString()}
															name={riskModel.id.toString()}
															value={isActive}
															onChange={(payload) => { onRiskToggle(riskModel, payload.value) }}
															label={riskModel.name}>
														</Checkbox>

														{riskModel.description && <Tooltip tooltip={riskModel.description} tooltipPosition='top-left' />}

													</div>
												)
											})
										}



									</div>
								</div>
							</div>
						</div>
					</div>

					{/* 3. грид результатов  */}
					<div className="travel-results__sort-button"></div>


				</div>
			</div>
		)
	}
}


const mapStateToProps = (state: IRootState, ownProps) => ({
	isLoaded: state.travel.isLoaded,
	dictionaryModel: state.travel.dictionaryModel,
	model: state.travel.model,
	listResultCalculateModel: state.travel.listResultCalculateModel,
	isRequestsAborted: state.travel.isRequestsAborted
})

const mapDispatchToProps = (dispatch: any, ownProps) => ({
	travelActions: bindActionCreators(travelActions, dispatch)
});

export const ConnectedTravelResults = connect(mapStateToProps, mapDispatchToProps)(TravelResults);