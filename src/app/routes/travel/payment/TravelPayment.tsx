import { IRootState } from "../../../store/reducers";
import { bindActionCreators } from "redux";
import { travelActions } from "../../../store/actions/travel.actions";
import { connect } from "react-redux";
import { ITravelPaymentProps } from "./ITravelPayment";
import { history } from '../../../app';


export class TravelPayment extends React.Component<ITravelPaymentProps> {

	componentWillMount() {
		const { isPayment, handlePayment } = this;
		
		if(isPayment) handlePayment();
	}

	public handlePayment = () => {
		let urlParams : any = Utils.text.getJsonFromUrl();
		let { travelOrderInfoSet, convertPolicyInformation, listModelGet } = this.props.travelActions;

		let { result, quotationGuid } = urlParams;

		travelOrderInfoSet({ name: 'quotationGuid', value: quotationGuid });

		if(result == 'success') {
			history.push(frameManager.url('travel/response'));
		}

		//url для теста неудачной оплаты. 
		// http://localhost:8288/#/travel/payment/catchresponse?result=error&quotationGuid=ae36af7fe22b4176b4bdd0ef51542ff3&lang=ru

		if(result == 'error' || result == 'cancel') {
			listModelGet();
			travelOrderInfoSet({ name: 'isPaymentError', value: true });
			convertPolicyInformation({ policyData: { quotationGuid }, callback: () => {
				history.push(frameManager.url('travel/order'));
			}});
		}
	}

	

	get isPayment() {
		return location.href.indexOf('/payment') > -1;
	}

	public render() {
		return <div>payment</div>
	}
}

const mapStateToProps = (state: IRootState, ownProps) => ({
});

const mapDispatchToProps = (dispatch: any, ownProps) => ({
	travelActions: bindActionCreators(travelActions, dispatch)
});



export const TravelPaymentContainer = connect(mapStateToProps, mapDispatchToProps)(TravelPayment as any);