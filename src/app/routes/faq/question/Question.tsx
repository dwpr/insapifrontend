import './Question.style.scss';

interface IQuestionProps {
    question: string;

    answer: string;

    isOpen?: boolean;

    onQuestionClick(index: number): void;

    index: number;
}

export class Question extends React.Component<IQuestionProps, any> {

    render() {
        return (
            <li className={`question${this.props.isOpen ? ' question--active' : ''}`} >
                <div className="question__head" onClick={() => this.props.onQuestionClick(this.props.index)}>
                    <i className="icon-tooltip"></i>
                    <span className='question__question'>{this.props.question}</span>
                </div>
                <p className="question__answer">{this.props.answer}</p>               
            </li>
        )
    }
}