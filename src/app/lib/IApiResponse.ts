
export class IApiResponse<T = any> {
	/**
	 * 
	 * @type {boolean}
	 * @memberof IApiResponse
	 */
	isOk : boolean;
	/**
	 * Модель
	 * @type {T}
	 * @memberof IApiResponse
	 */
	data : T;
	/**
	 * Текст с доп инфо
	 * @type {string}
	 * @memberof IApiResponse
	 */
	resultText : string;
}