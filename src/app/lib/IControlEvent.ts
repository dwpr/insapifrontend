




export interface IControlEvent {
	/**
	 * name
	 * @type {string}
	 * @memberof IControlEvent
	 */
	name : string;
	/**
	 * value 
	 * @type {*}
	 * @memberof IControlEvent
	 */
	value : any;
	/**
	 * Пропы компонента эмитнувшего эвент
	 * @type {*}
	 * @memberof IControlEvent
	 */
	props : any;
	/**
	 * Other 
	 */
	[x : string] : any;
}