




export class DateHelper {
	/**
	 * Check is string in format 9999-99-99
	 * @memberof DateHelper
	 */
	checkIsIsoString = (stringToTest: string) => new RegExp(/^\d{4}-\d{2}-\d{2}$/, 'gi').test(stringToTest);

}