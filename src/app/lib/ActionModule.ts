import { IApiResponse } from './IApiResponse';





export interface ISetterAction {
	<M = any>(payload: { name: string, value: any, meta?: M }): {
		type: string;
		payload: {
			name: string;
			value: any;
			meta?: M;
			[x: string]: any;
		}
	}
}



export interface IActionModuleTypes {
	[x: string]: string | IActionModuleTypes;
}

/**
 * 
 * Модуль redux-actions с базовыми хелперами и 
 * @export
 * @abstract
 * @class ActionModule
 */
export abstract class ActionModule {
	/**
	 * Костыль для bindActionCreators (не принимает без индексатора)
	 */
	[index: string]: any;
	/**
	 * Внутренний хелпер для создания функция сеттеров
	 * @protected
	 * @memberof ActionModule
	 */
	protected createSetter = (type): ISetterAction => (payload) => ({
		type: type,
		payload
	});

	/**
	 * Константы с названиями action's
	 * @type {IActionModuleTypes}
	 * @memberof ActionModule
	 */
	public types: IActionModuleTypes = {

	};



	/**
	 * Шорткат для создания { type, payload } action'a
	 * @protected
	 * @memberof ActionModule
	 */
	protected createPayload = (type, payload = {}) => ({
		type,
		payload
	});

	/**
	 * Сеттер с параметрами index, name, value
	 * @protected
	 * @memberof ActionModule
	 */
	protected createArraySetter = (type) => (payload : { index: number, name: string, value : any }) => ({ type, payload })


}



