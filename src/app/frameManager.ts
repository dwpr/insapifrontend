import { enumInsuranceCompanyId, enumInsuranceCompany } from "./store/logic/common";
import { IPartnerCommonConfiguration } from "./store/logic/configuration/interface/PartnerCommonConfiguration";
import { IPartnerConfiguration } from "./store/logic/configuration/interface/PartnerConfiguration";
import { IPageConfiguration } from './store/logic/configuration/interface/PageConfiguration';
import * as parseUrl from 'parse-url';
const $ = require('jquery');



export class FrameManager {
	/**
	 * Returns path string with prefix for build
	 * @param {string} url 
	 * @returns 
	 * @memberof frameManager
	 */
	public url ( url : string ) : string {
		return `${__DEV__ ? '/iframe-integration' : '/iframe-integration'}/${url}`;
		// return `${__DEV__ ? '' : ''}/${url}`;
	}	

	/**
	 * Returns api url
	 * @readonly
	 * @type {string}
	 * @memberof FrameManager
	 */
	public get apiPath () : string {
		let remoteUrl = `http://turavins${__TEST__ ? '-test.dwpr': '-dev.dwpr'}.ru`;
		if (__LOCAL__) remoteUrl = 'http://localhost:1549';
		return `${__DEV__ ? remoteUrl : ''}`;
	}	
	/**
	 * Outer url where iframe was loaded
	 * @type {string}
	 * @memberof frameManager
	 */
	public parentUrl : string =  window.location.href;

	public partnerGuid : string = '';

	/**
	 * Configuration for current outer page
	 * @type {IPageConfiguration}
	 * @memberof FrameManager
	 */
	public pageConfiguration : IPageConfiguration = null;

	/**
	 * Get initial url for iframe
	 * @readonly
	 * @memberof FrameManager
	 */
	public get initialUrl () {
		if (!this.pageConfiguration) return '';
		let section : string = '';
		if (this.pageConfiguration.name == 'realty') section = 'realty';
		else if (this.pageConfiguration.name == 'travel') section = 'travel';
		if (!section) return this.url('/404');
		return this.url(`${section}/insurane`); 
	}
	/**
	 * Setup params for current page
	 * @param {string} url 
	 * @param {IPartnerConfiguration} configuration 
	 * @memberof FrameManager
	 */
	public setupParams (url : string, configuration : IPartnerConfiguration) : string | string[] {
		if (!configuration) return '';
		let matchedPage = { name: '', params: {}, url: '/' };
		let listPage = configuration.listPage.filter(item => this.parentUrl.indexOf(item.url.toLowerCase()) > -1);
		if (listPage.length == 1) {
			matchedPage = listPage[0];
		} else {
			if (this.parentUrl.indexOf('turavins') > -1) {
				matchedPage = configuration.listPage.find(item => { 
					return this.parentUrl.indexOf(item.url) > -1;
				})
			}
		}
		this.pageConfiguration = matchedPage;
		const listPageName = ['realty', 'travel'];
		const listConfiguredPage = [];
		listPageName.map((name) => {
			if (matchedPage.name == name) listConfiguredPage.push(name);
		});
		if (listConfiguredPage.length == 1) return listConfiguredPage[0];
		return listConfiguredPage;


		// return '';
	}
	/**
	 * Redirect localy on outer site
	 * @param {string} url 
	 * @memberof frameManager
	 */
	public localRedirect(url : string) {
		console.log('localRedirect')
		parent.postMessage({
			type: 'TURAV_INS_LOCAL_REDIRECT',
			payload: {
				url: (this.parentUrl.indexOf('turavins') > -1 ? '' : 'http://turavins-dev.dwpr.ru' ) + url 	
			}
		}, '*');
	}			


	/** 
	* Metrics submit
	*/
	public yaEvent = (eventName: string, insuranceCompanyId = null) => {
		let insuranceCompanyName = enumInsuranceCompany.get(insuranceCompanyId) ? '_' + (enumInsuranceCompany.get(insuranceCompanyId) as any).name : '';
		let yaGoal = eventName + insuranceCompanyName;
		let ya_Counters = Object.keys(window).filter(item => item.indexOf('yaCounter') > -1 && item.length);
        ya_Counters.map(item => {
            try {
                eval(item + ".reachGoal('" + yaGoal + "')");
            } catch (err) { }
        });
	}
	


	/**
	 * @desc возвращает true, если id соответствует названию кампании.
	 * @argument {string | Array} companyName - название или массив названий страховых кампаний из списка enumInsuranceCompany в store/logic/common.ts
	 * Если companyName - вернет true, если insuranceCompanyId совпадает с id одной из указанных страховых
	 */
	public isCompanyById = (companyName: string | string[], insuranceCompanyId: number) => {
		if (companyName instanceof Array) return companyName.some(name => enumInsuranceCompanyId[name] === insuranceCompanyId);
		return enumInsuranceCompanyId[companyName] === insuranceCompanyId;
	}



	/**
	 * 
	 * @memberof Configuration
	 */
	public separateNotPaymentRedirectParams = (url : string) : {url, query}=> {
		const listSplitedByQuery = url.split('?');
		let queryString = '';
		const listRequiredQueryField = ['type', 'pagetype', 'orderId', 'result'];
		const urlModel = parseUrl(url);
		

		Object.keys(urlModel.query)
			.filter(key => {
				return !listRequiredQueryField.some(field => field == key.replace('amp;', ''));
			})
			.map(key => {
				const param = key.replace('amp;', '');
				queryString += `&${param}=${urlModel.query[key]}`;
			});

		return {
			url: listSplitedByQuery[0],
			query:  queryString
		}
	}

	public scrollTo = (y) => {
		if (window.parent != self) {
            window.parent.postMessage({
                type: 'TURAV_INS_SCROLL_TO',
                top: y
            }, '*');
        } else {
            $('html, body').animate({ scrollTop: y }, 300);
        }
	}

}


export const configuration = new FrameManager();