import { ErrorBlock } from '../components/error-block/ErrorBlock';
import { Validator } from '../components/validator/Validator';
const $ = require('jquery');




export class Validation {
	/**
	 * Массив компонентов валидаторов
	 * @memberof Validation
	 */
	public listValidator: Validator[] = [];
	/**
	 * 
	 * @type {boolean}
	 * @memberof Validation
	 */
	public isValid: boolean = true;
	/**
	 * Проходит по всем валидаторам и если validate каждого вернул true, возвращает true
	 * @memberof Validation
	 */
	public validate = (name = '', isScrollNeeded = false) => {
		let invalid = null;
		const validation = (resolve, reject) => {
			// вызывает validate у каждого
			// возвращает массив Promise<boolean>
			const listValidationResult = this.listValidator.map(async validator => { 
				try {
					const result = await validator.validate();
					return true;
				} catch(err) {
					// если еще нет не валидного то задаем блоку координаты и срколим если нужно
					if (!invalid) {
						invalid = validator;
						this.isValid = false;
							// если необходим скролл, берем элемент к которому нужно проскролить из scrollRef
							if (isScrollNeeded) {
								const invalidBlock = invalid.props.scrollRef;
								const coords = invalidBlock.getBoundingClientRect();
								const bodyCoord = document.body.getBoundingClientRect();
								let scrollTop = coords.top - bodyCoord.top - 200;
								// при скролле из начального положения страницы (скролл страницы = 0), шапка становится фиксированной => прокрутка должна уменьшиться на высоту шапки
								if (document.querySelector('.selected-insurance').getBoundingClientRect().top != 0) {
									scrollTop = scrollTop - 170;
								}
								if (window.parent != self) {
									window.parent.postMessage({
										type: 'TURAV_INS_SCROLL_TO',
										top: scrollTop
									}, '*');
								} else {
									$('html, body').animate({ scrollTop }, 300);
								}
							}
						// });
					}
					return false;
				}
			});
			Promise.all(listValidationResult)
				.then(res => res.every(ok => ok))
				.then(res => {
					// если все валидны то убираем блок 
					if (res) { 
						// window['errorBlock'].setState({isVisible: false});
						this.isValid = true;
						resolve();
					} else {
						this.isValid = false;
					}
				});
					
		}

		


		return new Promise((resolve, reject) => {
			new Promise(validation)
				.then(resolve)
				.catch(() => {});
		});

	}

}