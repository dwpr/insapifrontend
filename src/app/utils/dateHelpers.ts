import * as moment from 'moment';

export class DateHelpers {

    /**
     * Дата -> Строку (DD.MM.YYYY)
     * @memberOf DateHelpers
     */
    public toLocalString = (date) => {
        if (date.match('-')) return date.split("-").reverse().join(".");
    }
    /**
     * Дата -> Строка (YYYY-MM-DD)
     * @memberOf DateHelpers
     */
    public toString = (date : Date) => {
        if (!date) return date;
        return date.getFullYear().toString() + '-' + (date.getMonth() + 1).toString() + '-' + date.getDate().toString();
    }

    //конвертирует дату с формата YYYY-MM-DD к формату DD-MM-YYYY
    public dateToRus = (date) => {
        let momentDate = moment(date);
        if(!momentDate.isValid()) return;

        return momentDate.format('DD-MM-YYYY');
    }

    //  конвертация в формат "24 янв 2015"

    public getPeriodDate = (date: string) => {
		let fullDate = moment(date).format('D MMM YYYY');

		//обрезаем лишние точки после сокращений месяцев.

		let dateSplited = fullDate.split(' ');

		//если в конце месяца точка.
		if (dateSplited[1].indexOf('.') > 1) {
			//удаляем её.
			dateSplited[1] = dateSplited[1].slice(0, -1);
		}

		return dateSplited.join(' ');
    }
    
    // получение аббревиатуры месяца
    
    public getShortMonth = (date: string) => {
		let shortDate = moment(date).format('MMM');

		//если в конце месяца точка, удаляем её
		if (shortDate.indexOf('.') > 1) {
			shortDate = shortDate.slice(0, -1);
        }
        
		return shortDate;
    }
    
    public getNumberDays = (month, year) => {
        
        var date = new Date(year, month, 1);
        var days = [];
        while (date.getMonth() === month) {
           days.push(new Date(date));
           date.setDate(date.getDate() + 1);
        }
        return days.length;
    }
}