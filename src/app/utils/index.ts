import { ArrayHelpes } from './array';
import { BrowserHelpers } from './browserHelpers';
import { DateHelpers } from './dateHelpers';
import { Validation } from './validation';
import { TextHelpers } from './textHelpers';


export class AppUtils {
	/**
	 * Все что касается валидации и взаимодействия с компонентом Validator
	 * @type {Validation}
	 * @memberof AppUtils
	 */
	validation : Validation = new Validation()
	/**
	 * Хелперы для работы с текстом 
	 * @type {TextHelpers}
	 * @memberOf AppUtils
	 */
	text : TextHelpers = new TextHelpers();
	/**
	 * Хелперы для работы с датами 
	 * @type {DateHelpers}
	 * @memberOf AppUtils
	 */
	date : DateHelpers = new DateHelpers();

	browser : BrowserHelpers = new BrowserHelpers();
	array : ArrayHelpes = new ArrayHelpes();
}


export default new AppUtils();