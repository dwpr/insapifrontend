

export class ArrayHelpes {
	/**
	 * Returns array with element if it not in array or without if in array
	 * @param {*} value 
	 * @param {any[]} list 
	 * @returns 
	 * @memberof ArrayHelpes
	 */
	public toggle(value : any, list : any[]) {
		
		if (list.indexOf(value) > -1)  return list.filter(item => item != value);
		return [...list, value];
	}

	/**
	 * @desc принимает массив id нужного порядка и массив обьектов с id.
	 * Вернет новый обьект с нужным порядком id. 
	 */
	public generateListByListId(listObject : { id: number, [ prop: string ]: any }[], listId : number[]) {
		let result = [];

		if(!listId.length) {
			throw new Error('listId в функции generateListByListId не может быть пустым');	
		}

		listId.forEach(objectId => {
			let foundObject = listObject.find(({ id }) => id == objectId);

			if(foundObject) {
				result.push(foundObject);
			} else {
				console.warn('в listId передан id, не соответствующий ни одному из обьектов listObject');
			}
		})

		return result;
	}

	/**
	 * @desc проверяет есть ли обьект с данным id в массиве обьектов
	 */
	public checkIsObjectInListById = (listObject : { id: number, [ prop: string ]: any }[], objectId : number) => {
		return listObject.findIndex(({ id }) => id == objectId) > -1;
	}
}