





export class TextHelpers {
    /**
     * массив для getOrderNumberWord
     * @memberof TextHelpers
     */
    public listWordNumber = ['Первый', 'Второй', 'Третий', 'Четвертый', 'Пятый', 'Шестой', 'Седьмой', 'Восьмой', 'Девятый', 'Десятый'];

    /**
     * Получаем форму слова
     * @param num - число
     * @param word - массив форм слова, например, ['год', 'года', 'лет'] 
     * @memberOf TextHelpers
     */
    public getWordForm = (num : number | string, word : string[]) => {
        let cases = [2, 0, 1, 1, 1, 2];
        num = Number(num);
        return word[(num % 100 > 4 && num % 100 < 20) ? 2 : cases[(num % 10 < 5) ? num % 10 : 5]];
    }
    /**
     * Проставляем "и" или ","
     * @param index - номер элемента в списке
     * @param list - список
     * @memberOf TextHelpers
     */
    public getListDelimeter = (index : number, list : any[]) => {
        if (index == list.length - 2) return ' и ';
        if (index == list.length - 1) return '';
        return ', ';
    }
    /**
     * Пробелы для разрядов
     *  
     * @param {(number | string)} number - число
     * @param {string} delimeter - разделитель
     * @memberOf TextHelpers
     */
    public numberWithSpaces = (number : string | number, delimeter = '.') => {
        number = number.toString();
        let delimiter = number.indexOf(',') > -1 ? ',' : '.';
        let parts = number.split(delimiter);
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
        return parts.join(delimiter);
    }
    /**
     * Слово соответствующее номеру
     * @memberof TextHelpers
     */
    public getOrderNumberWord = (order : number) => {
        if (order < this.listWordNumber.length) return this.listWordNumber[order];
        return `${order + 1}-ый`;
    }

    public getCurrencySymbolById = (currencyId) => {
        if(currencyId == 1) return '$';
        if(currencyId == 2) return '€';
    }

    public getInsuranceLogoById = (id:number) => {
        let logoLink = '';

        switch(id){
            case 1: {
                logoLink = '/img/renessans.svg';
                break;
            }
            case 2: {
                logoLink = '/img/alfa.svg';
                break;
            }
            case 3: {
                logoLink = '/img/gaide.svg';
                break;
            }
            case 4: {
                logoLink = '/img/soglasie.svg';
                break;
            }
            case 5: {
                logoLink = '/img/absolut.svg';
                break;
            }
            case 6: {
                logoLink = '/img/tinkoff.svg';
                break;
            }
            case 7: {
                logoLink = '/img/allianz.svg';
                break;
            }
            case 8: {
                logoLink = '/img/liberty.svg';
                break;
            }
            case 9: {
                logoLink = '/img/ingosstrah.svg';
                break;
            }
            case 10: {
                logoLink = '/img/vtb.svg';
                break;
            }
            case 11: {
                logoLink = '/img/rosgosstrah.svg';
                break;
            }
            case 12: {
                logoLink = '/img/zetta.svg';
                break;
            }
            case 13: {
                logoLink = '/img/uralsib.svg';
                break;
            }
        }

        return logoLink;
    }

    public  getJsonFromUrl = (hashBased = true) => {
        var query;
        if(hashBased) {
          var pos = location.href.indexOf("?");
          if(pos==-1) return [];
          query = location.href.substr(pos+1);
        } else {
          query = location.search.substr(1);
        }
        var result = {};
        query.split("&").forEach(function(part) {
          if(!part) return;
          part = part.split("+").join(" "); // replace every + with space, regexp-free version
          var eq = part.indexOf("=");
          var key = eq>-1 ? part.substr(0,eq) : part;
          var val = eq>-1 ? decodeURIComponent(part.substr(eq+1)) : "";
          var from = key.indexOf("[");
          if(from==-1) result[decodeURIComponent(key)] = val;
          else {
            var to = key.indexOf("]",from);
            var index = decodeURIComponent(key.substring(from+1,to));
            key = decodeURIComponent(key.substring(0,from));
            if(!result[key]) result[key] = [];
            if(!index) result[key].push(val);
            else result[key][index] = val;
          }
        });
        return result;
      }
}