
require('iframe-resizer');

import './../styles/main.scss';
import { ThemeProvider } from 'react-jss';
import * as reduceReducers from 'reduce-reducers';
import { configureStore } from './store/store';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { createBrowserHistory, createHashHistory } from 'history';
import { RootContainer } from './routes/Root';
import { ConnectedRouter } from 'react-router-redux';
import { Route, Switch, Redirect } from 'react-router';
import { Unavailable } from './routes/unavailable/Unavailable';
import { Travel } from './routes/travel/Travel';
import { TravelWithoutFAQContainer }  from './routes/travelWidthoutFAQ/TravelWithoutFAQ';


if (__DEV__) {
	window['Utils'] = Utils;
}



const createHistory = () => __DEV__ ? createHashHistory() : createBrowserHistory();
// const createHistory = () => createBrowserHistory();
export const history = createHistory();
const images = require.context('./../public/img', true);

history.listen((locatiom, action) => {
	window.scroll(window.scrollX, 0);
});

export const image = (path :  any) : any=> {
	let correctPath : string = path.toString();
	if (!correctPath.startsWith('.') ) { 
		if (!correctPath.startsWith('/')) correctPath = '/' + correctPath;
		correctPath = '.' + correctPath;
	}
	return images(correctPath);
}


const store = configureStore(history);


const renderApp = (RootContainer) => {

	ReactDOM.render(
		<Provider store={store} >
			<ConnectedRouter history={history} >	
				<Switch>					
					<Route component={Unavailable} path={'/unavailable'} />
					<Route component={RootContainer} path={'/'} />
					{/* <Route component={RootContainer} path='/' />	роут с хедером/футером/FAQ  */}
				</Switch>					
			</ConnectedRouter>	
		</ Provider>,
		document.getElementById('root')
	);
	
};

renderApp(RootContainer);


if (module.hot) {
	module.hot.accept('./routes/Root.tsx', () => {
		renderApp(require('./routes/Root'));
	});
}

