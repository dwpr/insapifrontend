
export interface IValidatorRule {
	/**
	 * Имя правила валидатора
	 * @type {string}
	 * @memberof IValidatorRule
	 */
	name: string;
	/**
	 * Значение которое нужно для некоторых правил
	 * @type {*}
	 * @memberof IValidatorRule
	 */
	value?: any;
	/**
	 * Текст или элемент который будет отображен
	 * @type {(string | JSX.Element)}
	 * @memberof IValidatorRule
	 */
	message?: string | JSX.Element
	/**
	 * Название свойства потомка, на которое должен смотреть валидатор.
	 * Если в обьекте rule указать property - валидатор будет проверять children[property] а не по children.value
	 * @type {(string | number)}
	 */
	property?: string | number
}
