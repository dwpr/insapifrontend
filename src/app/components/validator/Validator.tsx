import { IValidatorProps } from './IValidatorProps';
import { IValidatorState } from './IValidatorState';
import { IValidatorRule } from './IValidatorRule';
import * as moment from 'moment';


export class Validator extends React.Component<IValidatorProps, IValidatorState> {

	static defaultProps: IValidatorProps = {
		listRule: [],
		children: null,
		name: ''
	}

	state: IValidatorState = {
		error: '',
		isHasError: false,
		isScrollNeeded: false,
		isValidatorWasUsed: false
	}

	child = null;

	validation = {
		required: (value: any, rule: IValidatorRule) => value,
		isTrue: (value: any, rule: IValidatorRule) => Boolean(value),
		isFalse: (value: any, rule: IValidatorRule) => !Boolean(value),
		isNumeric: (value: any, rule: IValidatorRule) => !isNaN(Number(value)),
		latin: (value: string, rule: IValidatorRule) => new RegExp("^[a-zA-Z]*$").test(value),
		noLatin: (value: string, rule: IValidatorRule) => new RegExp("^[^a-zA-Z]*$").test(value),
		isShorter: (value: any, rule: IValidatorRule) => value.toString().replace(' ', '').length <= rule.value,
		isLonger: (value: any, rule: IValidatorRule) => value.toString().length > rule.value,
		isLengthEqual: (value: string, rule: IValidatorRule) => value.toString().length == rule.value,
		email: (value: string, rule: IValidatorRule) => new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).test(value),
		isEqual: (value: string, rule: IValidatorRule) => value == rule.value,
		isNotEqual: (value: string, rule: IValidatorRule) => value !== rule.value,
		isLengthGreater: (value: string | any[], rule: IValidatorRule) => value.length > rule.value,
		isHasValue: (value: any, rule: IValidatorRule) => rule.value,
		isNotIncludes: (value: any, rule: IValidatorRule) => value.indexOf(rule.value) < 0,
		isLatin: (value: any, rule: IValidatorRule) => new RegExp("^[a-zA-Z]*$").test(value),
		isLatinAndHyphen: (value: any, rule: IValidatorRule) => new RegExp("^[a-zA-Z- ]*$").test(value),
		isCyrillic: (value: string, rule: IValidatorRule) => new RegExp("^[а-яёЁА-Я]*$").test(value),
		isCyrillicAndHyphen: (value: string, rule: IValidatorRule) => new RegExp("^[а-яёЁА-Я- \,\/\.]*$").test(value),
		isCyrillicHyphenAndNumber: (value: string, rule: IValidatorRule) => new RegExp('^[а-яёЁА-Я0-9- \,\/\.\(\)]+$').test(value),
		less: (value: number, rule: IValidatorRule) => value < rule.value,
		lessOrSame: (value: number, rule: IValidatorRule) => value <= rule.value,
		greater: (value: number, rule: IValidatorRule) => value > rule.value,
		greaterOrSame: (value: number, rule: IValidatorRule) => value >= rule.value,
		isDateLess: (value: Date, rule: IValidatorRule) => moment(value).isBefore(rule.value),
		isDateLessOrSame: (value: Date, rule: IValidatorRule) => moment(value).isSameOrBefore(rule.value),
		isDateGreaterOrSame: (value: Date, rule: IValidatorRule) => moment(value).isSameOrAfter(rule.value),
		isDateGreater: (value: any, rule: IValidatorRule) => moment(value).isAfter(rule.value),
		isDateValid: (value: any, rule: IValidatorRule) => new Date(value).getDate() <= Utils.date.getNumberDays(new Date(value).getMonth(), new Date(value).getFullYear())
	}

	componentDidMount() {
		const { listValidator } = Utils.validation;
		if (listValidator.indexOf(this) == -1) listValidator.push(this);
		listValidator.sort((a, b) => a.props.validationIndex - b.props.validationIndex)
	}

	componentWillUnmount() {
		const { listValidator } = Utils.validation;
		const index = listValidator.indexOf(this);
		if (index > -1) listValidator.splice(index, 1);

	}

	validate = (isScrollNeeded = false): Promise<boolean> => {

		return new Promise((resolve, reject) => {


			const { listRule, children } = this.props;
			const { child } = this;
			const { listValidator } = Utils.validation;
			let error: string | JSX.Element = '';
			let isHasError = false;
			let isErrorBlockVisible = false;




			listRule.some((rule, index) => {
				let { value } = children.props;
				/**
				 * @desc если указано свойство property в rule, то мы смотрим в начале на child (он не является null только в случае, если в потомке юзается callback см. onMountCallback)
				 * если же child нет - смотрим по validator.props.children. В таком случае property сможет принимать лишь значение свойства, переданного сверху вниз, а не значение например геттера компонента-потомка.
				*/
				if (rule.property) {
					value = child ? (child[rule.property] || child.props[rule.property]) : children.props[rule.property];
				}

				if (!(rule.name in this.validation)) {
					console.warn(`Rule ${rule.name} doesn't exist in validator validation`);
					return true;
				}
				const result = this.validation[rule.name](value, rule);
				if (!result) {
					error = rule.message;
					isHasError = true;
					if (__DEV__) {
						console.log('Краш произошел в валидаторе:' + rule.name + ' со значением');
						console.dir(value);
					}
					return true;
				}
			});

			this.setState({
				error,
				isHasError,
				isScrollNeeded,
				isValidatorWasUsed: true
			}, () => {
				isHasError ? reject(error) : resolve(true);
			});
		});

	}

	render() {
		const { children, permanentError } = this.props;
		const { error, isHasError, isScrollNeeded } = this.state;
		return [
			<div ref='invalidIndicator' className='validator-additional' key='additional-block'></div>,
			React.cloneElement(children as React.ReactElement<any>, { error: permanentError || error, isError: isHasError, key: 'validator', onMountCallback: (component) => this.child = component, updateValidator: () => { if (this.state.isValidatorWasUsed) this.validate() } })
		];
	}
}



