
import { IValidatorRule } from './IValidatorRule';


export interface IValidatorProps {
	children: React.ReactElement<any>
	/**
	 * Ошибка которая будет отображена всегда если передано в проп
	 * @type {*}
	 * @memberof IValidatorProps
	 */
	permanentError?: string | JSX.Element;
	/**
	 * Список правил для валидации
	 * @type {IValidatorRule[]}
	 * @memberof IValidatorProps
	 */
	listRule?: IValidatorRule[];
	/**
	 * Название группы для валидации
	 * @type {string}
	 * @memberof IValidatorProps
	 */
	name? : string;
	/**
	 * Порядковый номер валидатора
	 * @type {number}
	 * @memberOf IValidatorProps
	 */
	validationIndex ?: number;
	/**
	 * Блок, к которому будет скроллиться страница
	 */
	scrollRef ?: any;
}
