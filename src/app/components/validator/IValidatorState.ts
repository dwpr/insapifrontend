export interface IValidatorState {
	/**
	 * Текст ошибки или JSX.Element
	 * @type {*}
	 * @memberof IValidatorState
	 */
	error: string | JSX.Element;
	/**
	 * Валидация зафейлилась
	 * @type {boolean}
	 * @memberof IValidatorState
	 */
	isHasError: boolean;
	/**
	 * Был ли использован валидатор
	 * @type {boolean}
	 * @memberof IValidatorState
	 */
	isValidatorWasUsed: boolean;
	/**
	 * Нужен ли скролл
	 * @type {boolean}
	 * @memberof IValidatorState
	 */
	isScrollNeeded: boolean;
}
