import './ExpandSelect.style.scss';
import { IExpandSelectProps, IExpandSelectState } from './IExpandSelect';
import { ExpandBtn } from '../expand-btn/ExpandBtn';

export class ExpandSelect extends React.Component<IExpandSelectProps, IExpandSelectState> {

    public static defaultProps : IExpandSelectProps = {
        
        buttonText: 'Выбрать',
        listOption: [],
        onOptionSelect: () => {},
        className: ''
    }


    public state: IExpandSelectState = {
        isDropDownOpened: false
    }

    onClick = () => {
        let { isDropDownOpened } = this.state;
        if (this.props.isDisabled) return;
        this.setState({ isDropDownOpened: !isDropDownOpened });
    }


    public render() {
        let { listOption, buttonText, onOptionSelect, className } = this.props;
        let { isDropDownOpened } = this.state;

        return (
            <div className={`expand-select ${className}`} onClick={this.onClick}>

                <ExpandBtn text={buttonText} isExpanded={this.state.isDropDownOpened} />       
                {
                    isDropDownOpened &&
                    <div className="expand-select__dropdown">
                        {
                            listOption.length && listOption.map(({ id, name }) => (
                                <div className="expand-select__option" key={id} onClick={() => { onOptionSelect({ id, name }); this.setState({ isDropDownOpened: false })}}>
                                    { name }
                                </div>
                            ))
                        }
                    </div>
                }
            </div>
        )
    }
}