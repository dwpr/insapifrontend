export interface IExpandSelectProps {
    listOption: { id: number, name: string }[];
    buttonText: string;
    onOptionSelect: any;
    className: string;
    isDisabled?: boolean;
}


export interface IExpandSelectState {
    isDropDownOpened: boolean;
}