import { IControlEvent } from '../../lib/IControlEvent'
import { IDatePickerProps } from '../datepicker/IDatePickerProps';

export interface IRangeDatePickerProps {
	/**
	 * Пропы для календаря FROM
	 * @type {IDatePickerProps}
	 * @memberof IRangeDatePickerProps
	 */
	from: IDatePickerProps;
	/**
	 * Пропы для календаря TO
	 * @type {IDatePickerProps}
	 * @memberof IRangeDatePickerProps
	 */
	to: IDatePickerProps;
	/**
	 * Обработчик выбора даты в календаре
	 * @param {IControlEvent} event 
	 * @memberof IRangeDatePickerProps
	 */
	onChange(event: IControlEvent);
	/**
	 * Вызывается при выборе даты до,
	 * Нужно к примеру, для хайда пикера.
	 * @param {IControlEvent} event 
	 * @memberof IRangeDatePickerProps
	 */
	onToSelect?(event : IControlEvent);
	/**
	 * Вызыватеся при клике вне контейнера
	 * @param {MouseEvent} event 
	 * @memberof IRangeDatePickerProps
	 */
	onClickOutside?(event : MouseEvent);
	/**
	 * На какой вкладке открываем календарь
	 * @type {string}
	 * @memberof IRangeDatePickerProps
	 */
	tabForOpen?: string;

	onChangeActiveTab?(tabName : string);
}