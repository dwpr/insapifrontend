import { IControlEvent } from '../../lib/IControlEvent';
import { DatePicker } from '../datepicker/DatePicker';
import { IRangeDatePickerProps } from './IRangeDatePickerProps';
import { Component, IBemComponentProps } from '../../lib/react-bem-component';
import * as classNames from 'classnames';


export class RangeDatePicker extends Component<IRangeDatePickerProps> {


	static defaultProps = {
		namespace: 'range-datepicker',
		onToSelect: () => { },
		onChange: () => { },
		onClickOutside: () => { }
	}

	listTab = [
		{ name: 'from', caption: 'Туда' },
		{ name: 'to', caption: 'Обратно' }
	];

	state = {
		currentTab: this.props.tabForOpen || 'from'
	}

	componentDidMount() {
		window.addEventListener('mouseup', this.onClickOutsiteContainer);
	}
	componentWillUnmount() {
		window.removeEventListener('mouseup', this.onClickOutsiteContainer)
	}


	onClickOutsiteContainer = (event) => {
		const { onClickOutside } = this.props;
		const container: HTMLElement = this.refs.container as HTMLElement;
		if (!container.contains(event.target)) onClickOutside(event);
	}


	getCalendarClassName = (tabName) => classNames(
		this.element('calendar'),
		{ [this.elementModifier('calendar', 'visible')]: this.state.currentTab === tabName }
	);


	onChange = (event: IControlEvent) => {
		const { from, to, onChange, onToSelect, onChangeActiveTab } = this.props;
		onChange(event);
		if (event.name == from.name) {
			this.setState({ currentTab: 'to' });
			onChangeActiveTab('to');
			onChange({...event, name: to.name });
		}
		if (event.name == to.name) {
			this.setState({ currentTab: 'from' });
			onToSelect(event);
		}
	}

	render() {
		const { from, to, onChange, onChangeActiveTab } = this.props;
		const { block, element, elementModifier } = this;
		const { currentTab } = this.state;


		return (
			<div ref='container' className={block()} >
				<div className={classNames(element('tabs'), { [elementModifier('tabs', 'to')]: this.state.currentTab == 'to' })}>
					{this.listTab.map((tab, index) => {
						const className = classNames(
							element('tab'),
							{ [elementModifier('tab', 'active')]: tab.name === this.state.currentTab }
						);
						return (
							<div key={index} className={className} onClick={event => {
								this.setState({ currentTab: tab.name });
								onChangeActiveTab(tab.name);
							}
							} >
								{tab.caption}
							</div>
						)
					})}
					<div style={{ left: currentTab === 'from' ? '0%' : '50%' }} className={element('tab-indicator')}></div>
				</div>
				<div>
					<DatePicker className={this.getCalendarClassName('from')} {...from} rangeParams={{from: from.value, to: to.value}} onChange={this.onChange} />
					<DatePicker minDate={from.value} className={this.getCalendarClassName('to')} rangeParams={{from: from.value, to: to.value}}  {...to} onChange={this.onChange} />
				</div>
			</div>
		)
	}
}