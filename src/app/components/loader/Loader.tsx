import './Loader.style.scss'

const Loader = () => (
    <div className='loader'>
        <i className='loader__1'/>
        <i className='loader__2'/>
        <i className='loader__3'/>
        <i className='loader__4'/>
        <i className='loader__5'/>
        <i className='loader__6'/>
        <i className='loader__7'/>
        <i className='loader__8'/>
    </div>
)

export default Loader;