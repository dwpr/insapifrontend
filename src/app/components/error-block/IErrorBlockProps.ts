



export interface IErrorProps {
    className ?: string;
    isScrollNeeded ?: boolean;
}