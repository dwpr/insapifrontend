import { Validator } from '../validator/Validator';
import { Component } from "../../lib/react-bem-component/index";
import { IErrorProps } from "./IErrorBlockProps";
import * as classNames from 'classnames';
const $ = require('jquery');





export class ErrorBlock extends Component<IErrorProps> {

    public static defaultProps = {
        namespace: 'error-block',
        className: ''
    }

    public state = {
        isVisible: false,
        style: { top: 0, left : 0 }
    }

    public componentDidMount() {
        window['errorBlock'] = this;
    }


    public scrollToError = () => {
        if (window.parent != self) {
            window.parent.postMessage({
                type: 'TURAV_INS_SCROLL_TO',
                top: this.state.style.top - 50
            }, '*');
        } else {
            $('html, body').animate({ scrollTop: this.state.style.top - 50 }, 300);
        }
    }


    public onCloseClick = () => this.setState({isVisible: false});


    render() {
        const { element, block, modifier } = this;
        const { className } = this.props;
        const { isVisible, style } = this.state;

        const blockClassName = classNames(
            block(),
            className,
            { [modifier('visible')] : isVisible}
        );
      
        if (!isVisible) return null;
        return (
            <div ref='errorBlock' className={blockClassName} style={{ top: style.top + 'px' }}>
                <div className={element('content')}>Заполните обязательные поля</div>
                <div className={element('close')} onClick={this.onCloseClick}  ></div>
            </div>
        )
    }
}