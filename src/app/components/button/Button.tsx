import { IControlEvent } from '../../lib/IControlEvent';
import { Component } from '../../lib/react-bem-component';
import { IButtonProps } from './IButtonProps';
import * as classNames from 'classnames';

import injectSheet , {withTheme} from 'react-jss';




export class Button extends Component<IButtonProps> {


	static defaultProps = {
		namespace: 'button',
		isPrevent: true,
		type: 'button',
		theme: 'primary',
		tabIndex: -1,
		name: '',
		value: '',
		disabled: false,
		size: '',
		onClick: () => { }
	}

	/**
	 * Выполняем функцию при нажатии
	 * @memberof Button
	 */
	onClick = (event) => {
		const { onClick, isPrevent, name, value, disabled, type } = this.props;
		if (disabled) return;
		if (isPrevent && type !== 'submit') event.preventDefault();
		onClick({ props: this.props, name, value });
	}
	/**
	 * Стили кнопки для проставления значений переданных в пропы
	 * @readonly
	 * @memberof Button
	 */
	get style() {
		const style: any = {};
		const { width, height } = this.props;
		if (width) {
			if (typeof width === 'number') style.width = `${width}px`;
			else style.width = width;
		}
		if (height) {
			if (typeof height === 'number') style.height = `${height}px`;
			else style.height = height;
		}
		return style;
	}

	render() {

		const { children, type, tabIndex, theme, disabled, className, size } = this.props
		const { block, modifier, style } = this;
		const blockClassName = classNames(
			block(),
			modifier(theme),
			{ [modifier('disabled')]: disabled },
			{ [modifier('size')]: size },
			className
		);



		return (
			<button style={style} tabIndex={tabIndex} onClick={this.onClick} type={type} className={blockClassName} >
				{children}
			</button>
		)
	}
}