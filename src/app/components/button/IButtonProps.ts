import { IControlEvent } from '../../lib/IControlEvent';

export interface IButtonProps {
	/**
	 * className кнопки
	 * @type {string}
	 * @memberof IButtonProps
	 */
	className? : string;
	/**
	 * Модификатор типа кнопки
	 * @type {string}
	 * @memberof IButtonProps
	 */
	theme? : string;
	/**
	 * Если true добавляет модификатор disabled
	 * @type {boolean}
	 * @memberof IButtonProps
	 */
	disabled ? : boolean;
	/**
	 * Вызывать ли preventDefault при нажатии
	 * @type {boolean}
	 * @memberof IButtonProps
	*/
	isPrevent? : boolean;
	/**
	 * Атрибут type кнопки
	 * @type {string}
	 * @memberof IButtonProps
	 */
	type?: string;
	/**
	 * tabIndex блока
	 * @type {number}
	 * @memberof IButtonProps
	 */
	tabIndex? : number;
	/**
	 * Опциональное поле 
	 * @type {string}
	 * @memberof IButtonProps
	 */
	name? : string;
	/**
	 * Ширина кнопки (для особых случаев), если number то значение будет взято в пикселях, если string то так как передали. 
	 * @type {(number | string)}
	 * @memberof IButtonProps
	 */
	width? : number | string;
	/**
	 * Высота кнопки (для особых случаев), если number то значение будет взято в пикселях, если string то так как передали. 
	 * @type {(number | string)}
	 * @memberof IButtonProps
	 */
	height? : number | string;

	/**
	 * Опциональное поле
	 * @type {*}
	 * @memberof IButtonProps
	 */
	value? : any;
	/**
	 * Будет вызвано при нажатии на кнопку
	 * @param {IControlEvent} event 
	 * @returns {*} 
	 * @memberof IButtonProps
	 */
	onClick?(event : IControlEvent) : any;

	classes?: any;
	/**
	 * Размер кнопки
	 * @type {string}
	 * @memberof IButtonProps
	 */
	size? : string;
}

