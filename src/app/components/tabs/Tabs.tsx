import "./Tabs.style.scss";
import { Component } from '../../lib/react-bem-component';
import { ITabsProps } from './Itabs';
import { history } from '../../app';


export class Tabs extends React.Component<ITabsProps> {

    static defaultProps = {
        tabList: ''
    }

    render() {
        const { className, tabList } = this.props;
        // const currentRoute = history.location.pathname.substring(8);
        // let currentRouteIndex;
        // tabList.map((item, index) => {
        //     if (item.route == currentRoute) {
        //         currentRouteIndex = index;
        //     }
        // });

        const currentRouteIndex = tabList.findIndex(item => Boolean(history.location.pathname.match(item.route)));
        console.log(currentRouteIndex)

        let progressbarStepModifiers = [
            'steps__progressbar--first-step',
            'steps__progressbar--second-step',
            'steps__progressbar--third-step',
            'steps__progressbar--fourth-step',
        ]

        return (
            <div className='steps'>
                <div className={`steps__progressbar ${progressbarStepModifiers[currentRouteIndex]}`}></div>
                <div className='steps__titles' >
                    {
                        tabList.map((item, index) => {
                            return <div key={`step${index}`}
                                className={`steps__title ${index == currentRouteIndex ? 'steps__title--active' : ''} ${index < currentRouteIndex ? 'steps__title--passed' : ''} ${index ==  tabList.length - 1 && index == currentRouteIndex? 'steps__title--done' : ''}`}>
                                <i></i>
                                <span>{item.label}</span>
                            </div>
                        })
                    }
                </div>
            </div>
        )

    }
}