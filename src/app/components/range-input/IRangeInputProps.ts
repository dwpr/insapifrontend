import { IControlEvent } from '../../lib/IControlEvent';

export interface IRangeInputProps {
    /**
     * Класс рутового элемента 
     * @type {string}
     * @memberOf IRangeInputProps
     */
    className? : string;
    /**
     *  Атрибут id
     * 
     * @type {string}
     * @memberOf IRangeInputProps
     */
    id : string;
    /**
     * Название поля в модели
     * @type {string}
     * @memberOf IRangeInputProps
     */
    name : string;
    /**
     * Значение поля 
     * @type {*}
     * @memberOf IRangeInputProps
     */
    value : any;
    /**
     * Массив опций
     * @type {Array<any>}
     * @memberOf IRangeInputProps
     */
    listOption: Array<any>;
    /**
     * Обработка события onChange
     * @param {IControlEvent} event 
     * @returns {*} 
     * 
     * @memberOf IRangeInputProps
     */
    onChange(event : IControlEvent) : any; 
    /**
     * Tab index
     * @type {number}
     * @memberOf IRangeInputProps
     */
    tabIndex? : number;
    /**
     * Кастомный рендер значения 
     * @param {string} value 
     * @returns {(string | JSX.Element)} 
     * 
     * @memberOf IRangeInputProps
     */
    valueRenderer?(value : string) : string | JSX.Element;
}
