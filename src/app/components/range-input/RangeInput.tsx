import { IRangeInputProps } from './IRangeInputProps';
import { Component, IBemComponentProps } from '../../lib/react-bem-component';
import * as classNames from 'classnames';

export interface IRangeInputState {

}

export class RangeInput extends Component<IRangeInputProps, IRangeInputState> {
    /**
     * 
     * 
     * @static
     * 
     * @memberOf RangeInput
     */
    static defaultProps = {
        namespace: 'range-input',
        className: ''
    }
    /**
     * 
     * 
     * @type {IRangeInputState}
     * @memberOf RangeInput
     */
    state: IRangeInputState = {

    }
    /**
     * Обработка change
     * @memberOf RangeInput
     */
    onChange = (value) => {
        const { onChange, name } = this.props;
        onChange({
            props: this.props,
            value: value,
            name
        });
    }

    onPrevClick = () => {
        const { value, listOption } = this.props;
        if (value == listOption[0]) return;
        let index = listOption.indexOf(value);
        this.onChange(listOption[index - 1]);
    }
    onNextClick = () => {
        const { value, listOption } = this.props;
        if (value == listOption[listOption.length - 1]) return;
        let index = listOption.indexOf(value);
        this.onChange(listOption[index + 1]);
    }
    /**
     * Кастомный рендер value 
     * @memberOf RangeInput
     */
    valueRenderer = (value) => {
        const { valueRenderer, listOption } = this.props;
        let selectedOption = listOption.find(item => item == value) || listOption[0];
        let label = selectedOption.toString();
        if (valueRenderer) label = valueRenderer(label);
        return label;
    }

    render() {
        const { element, block, modifier, elementModifier, onChange, valueRenderer, onPrevClick, onNextClick } = this;
        const { id, name, value, listOption, className, tabIndex } = this.props;


        const blockClassName = classNames(
            block(),
			className
        );

        const leftButtonClassName = classNames(
            element('button'),
            element('button-left'),
            { [elementModifier('button', 'disabled')]: value == listOption[0] }
        );
        const rightButtonClassName = classNames(
            element('button'),
            element('button-right'),
            { [elementModifier('button', 'disabled')]: value == listOption[listOption.length - 1] }
        )

        return (
            <div className={blockClassName}>
                <div className={leftButtonClassName} onClick={onPrevClick}>-</div>
                <div className={element('value')}>{valueRenderer(value)}</div>
                <div className={rightButtonClassName} onClick={onNextClick}>+</div>
            </div>
        )
    }
}
