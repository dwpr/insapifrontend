export interface IFieldState {
	/**
	 * Поле в фокусе
	 * @type {boolean}
	 * @memberof IFieldState
	 */
	isFocused: boolean;
}