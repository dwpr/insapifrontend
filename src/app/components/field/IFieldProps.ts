import { IControlEvent } from '../../lib/IControlEvent';
export interface IFieldProps {
	/**
	 * Значение атрибута type input'a
	 * @type {string}
	 * @memberof IFieldProps
	 */
	type? : string;
	/**
	 * Класс рутового элемента
	 * @type {string}
	 * @memberof IFieldProps
	 */
	className? : string;
	/**
	 * Атрибут id инпата
	 * @type {string}
	 * @memberof IFieldProps
	 */
	id : string;
	/**
	 * Название поля в модели также атрибута тега input
	 * @type {string}
	 * @memberof IFieldProps
	 */
	name : string;
	/**
	 * Value
	 * @type {*}
	 * @memberof IFieldProps
	 */
	value : any;
	/**
	 * Функция которая будет вызвана при вводе
	 * @returns {*} 
	 * @memberof IFieldProps
	 */
	onChange?(event : IControlEvent) : any;
	/**
	 * Лейбл 
	 * @type {(string | JSX.Element)}
	 * @memberof IFieldProps
	 */
	label ?: string | JSX.Element; 
	/**
	* validation error
	*/
	isError ? : boolean;
	/**
	 * Строка или блок с ошибкой
	 */	
	error? : string | JSX.Element; 
	/**
	 * Постоянная ошибка, прокинутая не через валидатор
	 * @type {(string | JSX.Element)}
	 * @memberOf IFieldProps
	 */
	permanentError ?: string | JSX.Element;
	/**
	 * Регулярное выражение или название пресета для фильтра ввода
	 * @type {(RegExp | string)}
	 * @memberof IFieldProps
	 */
	regExp? : RegExp | string;
	/**
	 * Вместо обычного input react-input-mask
	 * @type {string}
	 * @memberof IFieldProps
	 */
	mask? : string;
	/**
	 * Проп maskChar для react-input-mask
	 * @type {string}
	 * @memberof IFieldProps
	 */
	maskChar? : string;
	/**
	 * Tab index input'a
	 * @type {number}
	 * @memberof IFieldProps
	 */
	tabIndex? : number;
	/**
	 * Проп для react-input-mask
	 * @type {boolean}
	 * @memberof IFieldProps
	 */
	alwaysShowMask? : boolean;
	/**
	 * Вставляется после значения в input
	 * @type {string}
	 * @memberof IFieldProps
	 */
	postfix? : string;
	/**
	 * Блок под полем
	 * @type {(string | JSX.Element)}
	 * @memberof IFieldProps
	 */
	subholder? : string | JSX.Element;
	/**
	 * Значение атрибута placeholder input'a
	 * @type {string}
	 * @memberof IFieldProps
	 */
	placeholder? : string;
	/**
	 * Значение атрибута pattern input'a
	 * @type {string}
	 * @memberof IFieldProps
	 */
	pattern? : string;
	/**
	 * Значение атрибута inputMode input'a
	 * @type {string}
	 * @memberof IFieldProps
	 */
	inputMode? : string;
	/**
	 * Вызывается при фокусе поля
	 * @param {IControlEvent} event 
	 * @returns {*} 
	 * @memberof IFieldProps
	 */
	onFocus?(event : IControlEvent) : any;
	/**
	 * Вызывается при отфоксе из поля
	 * @param {IControlEvent} event 
	 * @returns {*} 
	 * @memberof IFieldProps
	 */
	onBlur?(event : IControlEvent) : any;
	/**
	 * Вызывается при нажатии на клавиши при фокусе инпута.
	 * @returns {*} 
	 * @memberof IFieldProps
	 */
	onKeyDown?(event : any) : any;
	/**
	 * @desc если у поля есть маска, инпут полностью заполнен, вызывается функция завершения ввода с пропов.
	 * используется, например, на втором шаге realty для фокуса следующего инпута при заполнении серии паспорта
     * onInputComplete
	 * @memberof IFieldProps
	 */
	onInputComplete?() : any;
	/**
	 * Видимость блока валидации
	 * @type {boolean}
	 * @memberOf IFieldProps
	 */
	isErrorBlockVisible ?: boolean;

	carretToEnd ?: boolean;
	
	// признак того, что поле для дат
	// убрать, когда поле на 3 шаге будет замененено на календарь
	isDate ?: boolean;

	isPassportField?: boolean;
}