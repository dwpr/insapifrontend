import Mask from 'react-input-mask';
import { IFieldProps } from './IFieldProps';
import { IFieldState } from './IFieldState';
import { Component, IBemComponentProps } from '../../lib/react-bem-component';
import * as classNames from 'classnames';
import { ErrorBlock } from '../error-block/ErrorBlock';
import { FormElement } from '../form/FormElement';

@FormElement
export class Field extends Component<IFieldProps, IFieldState> {

	/**
	 * 
	 * @static
	 * @memberof Field
	 */
	static defaultProps = {
		namespace: 'field',
		className: '',
		type: 'text',
		maskChar: '_',
		postfix: '',
		onKeyDown: () => { },
		onInputComplete: () => { }
	}
	/**
	 * 
	 * @type {IFieldState}
	 * @memberof Field
	 */
	state: IFieldState = {
		isFocused: false
	}
	input : any = null;
	/**
	 * Конф маски для Mask компонента
	 * @memberof Field
	 */
	maskFormatChars = {
		'9': '[0-9]',
		'a': '[A-Za-z]',
		'а': '[А-Яа-я]',
		'*': '[A-Za-z0-9]'
	};

	/**
	 * @desc реф на рутовый див
	 */
	fieldRootElement = null;

	/**
	 * Валидируем значение по regExp или по переданной строке проверяем по пресету
	 * @memberof Field
	 */
	checkIsCorrectInput = (value) => {
		const { regExp } = this.props;
		if (!regExp) return true;
		if (regExp instanceof RegExp) return regExp.test(value);

		switch (regExp) {
			case 'number':
				return new RegExp(/^(\s*|\d+)$/, 'gi').test(value);
			case 'latin':
				return new RegExp(/^[a-zA-Z\s]*$/, 'gi').test(value);
			case 'cyrillic':
				return new RegExp(/^[а-яА-Я\s]*$/, 'gi').test(value);
			default:
				console.warn(`Unknown preset: ${regExp}, check`);
				return true;
		}
	}

	/**
	 * Эмитим чендж также если установлен фильтр ввода то чекаем что значение валидно, если нет то не эмитим
	 * @memberof Field
	 */
	onChange = (event: React.SyntheticEvent<any>) => {
		if (!this.checkIsCorrectInput(event.currentTarget.value)) return;
		const { name, mask, onInputComplete } = this.props;
		const onChange = this.props.onChange || this.context.onChange || (() => {});
		onChange({ props: this.props, value: event.currentTarget.value, name });

		/**
		 * @desc если у поля есть маска, инпут полностью заполнен, вызываем функцию завершения ввода с пропов.
		 * onInputComplete 
		 * Используем String.prototype.replace чтобы обрезать нижние подчеркивания в маске.
		 */
		if(mask && event.currentTarget.value.replace(/_/g, '').length === mask.length) {
			onInputComplete(); 
		}
	}

	/**
	 * Фокус поля
	 * @memberof Field
	 */
	public onFocus = (event: React.SyntheticEvent<any>) => {
		
		const {  name, value, type, carretToEnd} = this.props;
		const onChange = this.props.onChange || this.context.onChange || (() => {});
		const onFocus = this.props.onFocus || this.context.onFocus || (() => {});
		if(type === 'number' && value == 0) {
			onChange({ props: this.props, value: '', name });
		}
		this.setState({ isFocused: true });
		if (Utils.browser.detectIE) {
			let input = this.refs.input as any;
			if (input) {
				input.selectionStart = 100000;
				input.selectionEnd = 10000;
			}
		}
		onFocus({ props: this.props, name, value });
	}
	/**
	 * Отфокус поля
	 * @memberof Field
	 */
	public onBlur = (event: React.SyntheticEvent<any>) => {
		const {  name, value, type } = this.props;
		const onChange = this.props.onChange || this.context.onChange || (() => {});
		const onBlur = this.props.onBlur || this.context.onBlur || (() => {});
		if(type === 'number' && value == '' && !this.props.placeholder) {
			onChange({ props: this.props, value: 0, name });
		}
		if(type === 'number' && (Number(value) === 0)) {
			onChange({ props: this.props, value: '', name });
		}
		// IE не умеет заменять в number запятые на точки, как делает это тот же Chrome самостоятельно
		if(type === 'number' && Utils.browser.detectIE()) {
			onChange({ props: this.props, value: value.replace(/,/,'.'), name });
		}
		this.setState({ isFocused: false });
		onBlur({ props: this.props, name, value });
	}

	/**
	 * @desc фокусит инпут внутри.
	 * Использует querySelector, т.к. к инпуту mask по другому не подобраться.
	 */
	public focus = () => {
		this.fieldRootElement.querySelector('input').focus();
	}

	public blur = () => {
		this.input && this.input.blur();
	}

	/**
	 * @desc если value === null, то вовзращаем пустую строку
	 * если есть постфикс, то в случае, если значение определено - показываем его.
	 */
	get inputValue() {
		let { value, postfix, type } = this.props;

		if(value && !this.state.isFocused) return value + ' ' + postfix; 
		if (value == null || value == undefined) value = '';
		return value;
	}

	// убрать, когда поле на 3 шаге будет замененено на календарь

	transformIsoToRus = (dateString: string) => {
        if (!dateString && dateString != '0') return '';
        if (dateString.match(/\./gi)) return dateString;
        if (dateString.indexOf('-') > -1) {
            const [year, month, day] = dateString.split('-');
            return `${day}.${month}.${year}`;
        }
    }

	render() {
		const { element, block, modifier, onChange, maskFormatChars, onFocus, onBlur } = this;
		const { isFocused } = this.state;
		const { label, className, type, id, subholder, name, value, postfix, error, isError, tabIndex, mask, maskChar, alwaysShowMask, placeholder, isErrorBlockVisible, onKeyDown, pattern, inputMode, permanentError, isPassportField} = this.props;
		

		const blockClassName = classNames(
			block(),
			{ [modifier('focused')]: isFocused },
			{ [modifier('filled')]: this.inputValue },
			{ [modifier('error')]: isError || Boolean(permanentError) },
			className
		);

		const inputProps = {
			// убрать, когда поле на 3 шаге будет замененено на календарь
			value: this.props.isDate ? this.transformIsoToRus(this.inputValue) : this.inputValue,
			type: isFocused ? type : 'text',
			className: isPassportField ? element('passport-input') :  element('input'),
			tabIndex,
			name,
			id,
			onChange,
			onFocus,
			onBlur,
			onKeyDown
		};

		return (
			<div className={blockClassName} ref={fieldRootElement => this.fieldRootElement = fieldRootElement}>
				{/* {isErrorBlockVisible && <ErrorBlock />} */}				
				<div className={element('text-box')} >
					{mask ?
						<Mask ref='mask' {...inputProps} mask={mask} formatChars={maskFormatChars} maskChar={maskChar} alwaysShowMask={alwaysShowMask} placeholder={placeholder} inputMode={inputMode} pattern={inputMode == 'numeric' ? '\\d*': null}/>
						:
						<input ref='input' {...inputProps} placeholder={placeholder} onKeyPress={onKeyDown} inputMode={inputMode} pattern={inputMode == 'numeric' ? '\\d*': null}/>
					}
					{label && <label className={element('label')} htmlFor={id} >{label}</label>}
					{subholder && <div className={element('subholder')}>{subholder}</div> }
				</div>
					{(error || Boolean(permanentError)) && <div className={element('error')} >{error || permanentError}</div>} 
			</div>
		)
	}
}
