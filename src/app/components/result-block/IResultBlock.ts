import { ICalculationModel, IResultCalculateModel } from '../../store/logic/travel/interfaces/ITravelModel';

export interface IResultBlockProps extends IResultCalculateModel {
    // calculationModel: ICalculationModel;
    // price: number,
    // specialCondition: string;
    /**
     * @desc обработчик нажатия кнопки "оформить"
     */
    onIssue: ({ id }) => any;
    /**
     * 
     */
    listCountry: { id:number, name: string }[];
    getInsuranceRule(quotationGuid: any): any;
    getInsuranceSample(quotationGuid: any): any;
}


export interface IResultBlockState {
    isMoreBlockVisible: boolean;
}