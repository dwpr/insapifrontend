import { CurrencyIcon, Price } from '../price/Price';
import './ResultBlock.style.scss';
import { IResultBlockProps, IResultBlockState } from './IResultBlock';
import { Button } from '../button/Button';
import { ExpandBtn } from '../expand-btn/ExpandBtn';
import { Tooltip } from '../tooltip/Tooltip';
import Loader from '../loader/Loader';
import { enumInsuranceCompany } from '../../store/logic/common';
 
export class ResultBlock extends React.Component<IResultBlockProps, IResultBlockState> {

    public static defaultProps : IResultBlockProps = {
        specialCondition: null,
        price: null,
        calculationModel: null,
        onIssue: () => {},
        isLoading: true,
        isSuccess: false,
        insuranceCompanyId: null,
        assistance: null,
        docLink: null,
        listCountry: null,
        getInsuranceRule: () => {},
        getInsuranceSample: () => {}
    }


    public state: IResultBlockState = {
        isMoreBlockVisible: false,
    }

    get isFranchiseExist() {
        let { risk } = this.props.calculationModel;

        if(!risk) return false;

        let result : boolean = false;

        try {
            result = risk.length && risk.some(riskModel => (riskModel.franchise && riskModel.franchise.length > 0));
        } catch (e) {
            debugger
        }
        
        return result;
    }

    /**
     * @desc возвращает список всех франшиз.
     * @return { countryId, value }[]
     */
    get listFranchiseText() {
        if(!this.isFranchiseExist) return [];

        let { risk, currencyId } = this.props.calculationModel;
        let { listCountry } = this.props;

        //все риски с франшизами
        let listFranchiseRisk = risk.filter(riskModel => (riskModel.franchise && riskModel.franchise.length > 0));

        //все франшизы всех рисков
        let listFranchiseText = [];
        
        listFranchiseRisk.forEach(riskModel => {
            riskModel.franchise.forEach(franchiseModel => {
                let countryName = franchiseModel.countryId && listCountry.find(countryModel => countryModel.id == franchiseModel.countryId).name;
                let riskName = riskModel.name;

                let text = (countryName || riskName) + ', ' + franchiseModel.value + ' ' + Utils.text.getCurrencySymbolById(currencyId);

                listFranchiseText.push(text);
            });
        });

        return listFranchiseText;
    }

    /**
     * @desc данные для отображения иконок
     */
    get conditionNotificationData() {
        let specialCondition = this.props.specialCondition;

        let result = {
            gift: { isExist: false, text: '' },
            limitation: { isExist: false, text: '' }
        }

        if(specialCondition && specialCondition.length) {
            specialCondition.forEach(conditionModel => {
                if(conditionModel.typeId == 1) {
                    result.limitation.isExist = true;
                    result.limitation.text = conditionModel.value;
                }

                if(conditionModel.typeId == 2) {
                    result.gift.isExist = true;
                    result.gift.text = conditionModel.value;
                }
            })
        }

        return result;
    }

    logoSizeModifier(insuranceCompanyId: number): string {
		let insuranceCompanyName = (enumInsuranceCompany.get(insuranceCompanyId) as any).name;
		return `result-block__logo--${insuranceCompanyName}` 
    }

    private getInsuranceRule = (e) => {
        e.preventDefault();
        const { calculationModel, getInsuranceRule } = this.props;
        getInsuranceRule(calculationModel.quotationGuid);
    }

    private getInsuranceSample = (e) => {
        e.preventDefault();
        const { calculationModel, getInsuranceSample } = this.props;
        getInsuranceSample(calculationModel.quotationGuid);
    }

    public render() {
        let { isMoreBlockVisible } = this.state;
        let { specialCondition, price, onIssue, isLoading, isSuccess, insuranceCompanyId, assistance } = this.props;

        if(isLoading) return (
            <div className="result-block result-block--load">
                <div className="result-block__logo-wrap">
                    <div className={`result-block__logo ${this.logoSizeModifier(insuranceCompanyId)}`}>
                        <img src={Utils.text.getInsuranceLogoById(insuranceCompanyId)}/>
                    </div>
                </div>
                <div className="result-block__selection-message">Подбор страховок по вашим параметрам</div>
                <div className="result-block__spinner-wrap">
                    <Loader/>
                </div>
            </div>
        )

        if(!isSuccess) {
            return (
                <div className="result-block result-block--not-found">
                    <div className="result-block__logo-wrap">
                        <div className={`result-block__logo ${this.logoSizeModifier(insuranceCompanyId)}`}>
                            <img src={Utils.text.getInsuranceLogoById(insuranceCompanyId)}/>
                        </div>
                    </div>
                    <div className='result-block__not-found-message'>
                        Не найдено подходящих страховок по вашему запросу
                    </div>
                </div>
            )
            
        }

        let { currencyId, risk } = this.props.calculationModel;
        let { isFranchiseExist, listFranchiseText, conditionNotificationData } = this;

        let currencyIcon = currencyId == 1 ? CurrencyIcon.USD : currencyId == 2 ? CurrencyIcon.EUR : '';

        

        return (
            <div className={`result-block ${isMoreBlockVisible ? 'result-block--expanded' : ''}`}>
                <div className='result-block__main'>
                    <div className="result-block__logo-wrap">
                        <div className={`result-block__logo ${this.logoSizeModifier(insuranceCompanyId)}`}>
                            <img src={Utils.text.getInsuranceLogoById(insuranceCompanyId)}/>
                        </div>
                       <ExpandBtn text='Подробнее' isExpanded={isMoreBlockVisible} onClick={() => this.setState({isMoreBlockVisible: !isMoreBlockVisible})}/>
                    </div>
                    <div className='result-block__flex-wrap'>
                        <div className='result-block__inner'>
                            <div className='result-block__title'>Франшиза</div>
                            <div className='result-block__data'>{isFranchiseExist ? 'Да' : 'Нет'}</div>
                        </div>
                        <div className='result-block__inner'>
                            <div className='result-block__title'>Страховая сумма</div>
                            <div className='result-block__data'>
                                <Price currency={currencyIcon} noDecimal={true}>{risk[0].coverageSum}</Price>
                            </div>
                        </div>
                        <div className='result-block__inner'>
                            <div className='result-block__title'>Стоимость страховки</div>
                            <div className='result-block__data'>
                                <Price currency={CurrencyIcon.RUB}>{price}</Price>                               
                                <div className='result-block__additional'>
                                    { conditionNotificationData.gift.isExist && ( 
                                        <Tooltip tooltip={conditionNotificationData.gift.text} className='gift-tooltip' tooltipIcon={<div className='icon-gift'></div>} tooltipPosition='top'/> 
                                    )} 
                                    { conditionNotificationData.limitation.isExist && ( 
                                        <Tooltip tooltip={conditionNotificationData.limitation.text} className='warning-tooltip' tooltipIcon={<div className='icon-warning'></div>} tooltipPosition='top'/> 
                                    )}   
                                </div>                                

                                {/* <Tooltip tooltip='meow' className='warning-tooltip' tooltipIcon={<div className='icon-warning'></div>} /> */}
                            </div>
                        </div>
                    </div>
                    <Button size='small' onClick={() => onIssue({ id: insuranceCompanyId })}>Оформить</Button>
                </div> 
                {isMoreBlockVisible && 
                    <div className="result-block__more">
                        <div className='result-block__flex-wrap'>
                            <ul className='result-block__item-list'>
                                {
                                    assistance && (assistance.name || assistance.phone) && (
                                        <li className='result-block__item'>
                                            <div className='result-block__more-title'>Ассистанс</div>
                                            <div className='result-block__text'>
                                                <span>{assistance.name} {assistance.phone}</span>
                                            </div>
                                        </li>
                                    )
                                }
                               
                                {
                                    isFranchiseExist && Boolean(listFranchiseText.length) && (
                                        <li className='result-block__item'>
                                            <div className='result-block__more-title'>Франшиза</div>
                                            <div className='result-block__text'>
                                                { listFranchiseText.map(franchiseText => (
                                                    <span>{franchiseText}</span>
                                                )) }
                                                {/* {<span>Таиланд, 50 { Utils.text.getCurrencySymbolById(currencyId) }</span>
                                                <span>Вьетнам, 50 { Utils.text.getCurrencySymbolById(currencyId) }</span>
                                                <span>Гражданская отвественность, 200 €</span>} */}
                                            </div>
                                        </li>
                                    )
                                }

                               {
                                   specialCondition && Boolean(specialCondition.length) && (
                                    <li className='result-block__item'>
                                        <div className='result-block__more-title'>Особые условия</div>
                                        {
                                                specialCondition.map(conditionModel => (
                                                    <div className='result-block__text' key={conditionModel.countryId}>
                                                        <p>
                                                            {conditionModel.value}
                                                        </p>
                                                    </div>
                                                ))
                                            }
                                    </li>
                                   )
                               }
                                
                            </ul> 
                            <div className='result-block__links'>
                               <span onClick={this.getInsuranceRule}>Правила страхования</span>
                               <span onClick={this.getInsuranceSample}>Образец полиса</span>
                            </div>                           
                        </div>
                    </div>    
                }               
            </div>                
        )
    }
}