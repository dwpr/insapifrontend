import { IFormProps } from './IFormProps';
import { IControlEvent } from '../../lib/IControlEvent';
import * as PropTypes from 'prop-types';




/**
 * 
 * Форма для объеденения группы компонентов декорированных с помощью FormElement
 * Цель - возможность прописывать основные обработчики только 1 компоненту
 * @export
 * @class Form
 * @extends {React.Component<IFormProps>}
 */
export class Form extends React.Component<IFormProps> {
	/**
	 * Types для полей контекста
	 * @static
	 * @memberof Form
	 */
	public static childContextTypes = {
		onChange: PropTypes.any,
		onBlur: PropTypes.any,
		onFocus: PropTypes.any
	}
	/**
	 * Закидываем обработчики
	 * @returns 
	 * @memberof Form
	 */
	public getChildContext() {
		const { onChange, onBlur, onFocus } = this.props;
		return {
			onChange,
			onBlur,
			onFocus
		}
	}
	/**
	 * Вызываем сабмит 
	 * @memberof Form
	 */
	public onSubmit = (e) => {
		e.preventDefault();
		this.props.onSubmit(e);
	}
	/**
	 * Геттер ссылки рутового элемента
	 * @readonly
	 * @type {HTMLElement}
	 * @memberof Form
	 */
	public get root(): HTMLElement {
		return this.refs.root as HTMLElement;
	}
	/**
	 * Рендерит форму и чилдренов
	 * @returns 
	 * @memberof Form
	 */
	public render() {

		const { children, className } = this.props;
		const { onSubmit } = this;

		return (
			<form ref='root' onSubmit={onSubmit} className={className} >
				{children}
			</form>
		);
	}
}



