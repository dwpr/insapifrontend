import * as PropTypes from 'prop-types';

/**
 * 
 * @param {*} Component Реакт компонент который на выходе должен эмитить эвенты в формате IControlEvent
 */
export const FormElement = (Component) => {
	/**
	 * Контекст тайпы для прокидывания
	 */
	const elementContexTypes = {
		onChange: PropTypes.any,
		onBlur: PropTypes.any,
		onFocus: PropTypes.any
	};
	/**
	 * Если у компонента есть свои контекст тайпс то также добавляем их
	 */
	let componentContextTypes = {};
	if (Component.contextTypes) componentContextTypes = { ...Component.contextTypes }
	/**
	 * Объеденяем контекст компонента и формы
	 */
	Component.contextTypes = {
		...elementContexTypes,
		...componentContextTypes
	};
}

