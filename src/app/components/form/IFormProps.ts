


import { IControlEvent } from '../../lib/IControlEvent';


export interface IFormProps {
	/**
	 * Обработчик onChange всех FormControl в форме
	 * @param {IControlEvent} event 
	 * @returns {*} 
	 * @memberof IFormProps
	 */
	onChange?(event : IControlEvent): any;
	/**
	 * Обработчик onBlur всех FormControl в форме
	 * @param {IControlEvent} event 
	 * @returns {*} 
	 * @memberof IFormProps
	 */
	onBlur?(event): any;
	/**
	 * Обработчик onFocus всех FormControl в форме
	 * @param {IControlEvent} event 
	 * @returns {*} 
	 * @memberof IFormProps
	 */
	onFocus?(event): any;
	/**
	 * Вызывается на сабмите формы
	 * @param {IControlEvent} event 
	 * @returns {*} 
	 * @memberof IFormProps
	 */
	onSubmit?(event : React.SyntheticEvent<HTMLFormElement>) : any;
	/**
	 * Класс формы
	 * @type {string}
	 * @memberof IFormProps
	 */
	className?: string;
}
