import { RangeInput } from '../range-input/RangeInput';
import { Tooltip } from '../tooltip/Tooltip';
import { Checkbox } from '../checkbox/Checkbox';
import { IInsuranceOptionProps } from './IInsuranceOptionProps';
import { Component, IBemComponentProps } from '../../lib/react-bem-component';
import * as classNames from 'classnames';
import { Price } from '../price/Price';

export interface IInsuranceOptionState {

}

export class InsuranceOption extends Component<IInsuranceOptionProps, IInsuranceOptionState> {
    /**
     * 
     * 
     * @static
     * 
     * @memberOf InsuranceOption
     */
    static defaultProps = {
        namespace: 'insurance-option',
        className: ''
    }
    /**
     * 
     * 
     * @type {IInsuranceOptionState}
     * @memberOf InsuranceOption
     */
    state: IInsuranceOptionState = {

    }



    onChange = (event) => {
        const { inputProps, onChange, listOption, optionProps, onTooltipClick } = this.props;

        const payload = {
            isSelected: optionProps.value,
            selectedSum: listOption.some(option => option == inputProps.value) ? inputProps.value : listOption[0],
            [event.name]: event.value,
        };

        onChange({
            value: payload,
            name: '',
            props: this.props,
        });

        // onTooltipClick();

    }

    valueRenderer = (value) => {
        return <Price currency={this.props.currency} noDecimal={true}>{value}</Price>;
    }

    render() {
        const { element, block, modifier, elementModifier, onChange, valueRenderer } = this;
        const { id, listOption, className, optionProps, inputProps, label, tooltip, currency, onTooltipClick } = this.props;

        const blockClassName = classNames(
            block(),
            className,
			{ [modifier('checked')]: this.props.optionProps.value },
        );


        return (
            <div className={blockClassName} onClick={e => this.onChange(e)}>
                <div className={element('wrap')}>
                    <div className={element('title')}>{label}</div>
                    {tooltip && <Tooltip className={element('tooltip')} tooltip={tooltip} tooltipType='top' onClick={onTooltipClick} width='200'/>}
                    <i className="insurance-option__icon icon-packman"></i>
                </div>
                <div className={element('price')}>
                    {listOption.length > 1 && optionProps.value && <RangeInput className={element('range')} id={inputProps.name} name={inputProps.name} value={inputProps.value} listOption={listOption} onChange={onChange} valueRenderer={valueRenderer}/>}
                    {listOption.length == 1 && listOption[0] != 0 &&
                        <div className={element('price-inner')}>
                            <span className={element('from')}>от </span>
                            <Price currency={currency} noDecimal={true}>{listOption[0]}</Price>
                        </div>
                    }
                </div>
            </div>
        )
    }
}