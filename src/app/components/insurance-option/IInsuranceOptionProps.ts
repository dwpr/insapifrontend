import { IControlEvent } from '../../lib/IControlEvent';
export interface IInsuranceOptionProps {
    /**
     * Класс рутового элемента  
     * @type {string}
     * @memberOf IInsuranceOptionProps
     */
    className?: string;
    /**
     * Атрибут id 
     * @type {string}
     * @memberOf IInsuranceOptionProps
     */
    id: string;
    /**
     * Props for option
     * @type {IInsuranceOptionProps}
     * @memberOf IInsuranceOptionProps
     */
    optionProps?: IInsuranceOptionLabelProps;
    /**
     * Props for input
     * @type {IInsuranceOptionInputProps}
     * @memberOf IInsuranceOptionProps
     */
    inputProps?: IInsuranceOptionInputProps;
    /**
     * Список опция для range input
     * 
     * @type {Array<any>}
     * @memberOf IInsuranceOptionProps
     */
    listOption?: Array<any>;
    /**
     * Лейбл для опции
     * @type {*}
     * @memberOf IInsuranceOptionProps
     */
    label ?: any;
    /**
     * Обработка события onChange 
     * @param {IControlEvent} event 
     * @returns {*} 
     * 
     * @memberOf IInsuranceOptionProps
     */
    onChange(event: IControlEvent): any;
    /**
     * Номер опции
     * @type {number}
     * @memberOf IInsuranceOptionProps
     */
    index: number;
    /**
     * Текст тултипа
     * @type {*}
     * @memberOf IInsuranceOptionProps
     */
    tooltip?: any;
    /**
     * Клик по тултипу
     * @memberOf IInsuranceOptionProps
     */
    onTooltipClick?(): void;
    /**
     * Валюта
     * @type {(string | JSX.Element)}
     * @memberOf IInsuranceOptionProps
     */
    currency ?: string | JSX.Element;
    
}

export interface IInsuranceOptionLabelProps {
    name: string;
    value: boolean;
    id: string;
}

export interface IInsuranceOptionInputProps {
    name: string;
    value: any;
}