import { IControlEvent } from '../../lib/IControlEvent';

export interface ICheckboxProps {
    
    /**
     * Класс рутового элемента 
     * @type {string}
     * @memberof ICheckboxProps
     */
    className? : string;
    /**
     * Атрибут id
     * 
     * @type {string}
     * @memberOf ICheckboxProps
     */
    id : string;
    /**
     * Атрибут name = название поля в модели
     * @type {string}
     * @memberOf ICheckboxProps
     */
    name : string;
    /**
     * value 
     * @type {*}
     * @memberOf ICheckboxProps
     */
    value : any;
    /**
     * disabled у чекбокса
     * @type {boolean}
     * @memberOf ICheckboxProps
     */
    disabled? :boolean;
    /**
     * Функция на change
     * @param {IControlEvent} event 
     * @returns {*} 
     * 
     * @memberOf ICheckboxProps
     */
    onChange(event : IControlEvent) : any;
    /**
     * label 
     * @type {(string | JSX.Element)}
     * @memberOf ICheckboxProps
     */
    label? : string | JSX.Element;
    /**
     * Строка или блок с ошибкой
     * @type {(string | JSX.Element)}
     * @memberOf ICheckboxProps
     */
    error? : string | JSX.Element;
    /**
     * tab index 
     * @type {number}
     * @memberOf ICheckboxProps
     */
    tabIndex? : number;
    index ? : any;
    onBlur?() : void;
    onFocus?(): void;
    isError ?: boolean;

    onClick?(event : any) : any;
}