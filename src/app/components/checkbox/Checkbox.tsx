import { ICheckboxProps } from './ICheckboxProps';
import { Component, IBemComponentProps } from '../../lib/react-bem-component';
import * as classNames from 'classnames';
import { FormElement } from '../form/FormElement';



export interface ICheckboxState {

}
@FormElement
export class Checkbox extends Component<ICheckboxProps, ICheckboxState> {

	/**
	 * @static
	 * @memberof Checkbox
	 */
	static defaultProps = {
		namespace: 'checkbox',
		className: '',
	}
	/**
	 *  
	 * @type {ICheckboxState}
	 * @memberOf Checkbox
	 */
	public state: ICheckboxState = {

	}
	/**
	 * 
	 * @memberof Checkbox
	 */
	public onChange = async(event: React.SyntheticEvent<any>) => {
		const { name } = this.props;
		const onChange = this.props.onChange || this.context.onChange || (() => {});
		const onBlur = this.props.onBlur || this.context.onBlur || (() => {});
		const onFocus = this.props.onFocus || this.context.onFocus || (() => {});
		onChange({
			props: this.props,
			value: event.currentTarget.checked,
			name
		});
		//  async необходим, ибо иначе onFocus не успевает отработать перед onBlur и валидация на чекбоксах на Blur не срабатывает 
		if (onFocus) await onFocus();
		if (onBlur) onBlur();
	}

	public click() {
		const{label} : any = this.refs;
		if (label) label.click();
	}

	render() {
		const { element, block, modifier, onChange, elementModifier } = this;
		const { id, label, name, className, value, error, tabIndex, disabled, isError } = this.props;

		const blockClassName = classNames(
			block(),
			className,
			{ [modifier('disabled')]: disabled }
		);

		const flagClassName = classNames(
			element('flag'),
			{ [elementModifier('flag', 'error')]: isError },
			{ [elementModifier('flag', 'checked')]: value }			
		);
		return (
			<div className={blockClassName}>
				<label ref='label' htmlFor={id} tabIndex={tabIndex} className={element('label-wrapper') } >
					<div className={flagClassName}></div>
					{label && <div className={element('label')}>{label}</div>} 
				</label>
				<input id={id} type='checkbox' name={name} value={value} checked={value} className={element('input')} disabled={disabled} onChange={onChange} />
				{error && <div className={element('error')} >{error}</div>}
			</div>
		)
	}

}