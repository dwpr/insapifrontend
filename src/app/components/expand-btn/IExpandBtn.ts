export interface IExpandBtnProps {
    isExpanded?: boolean,
    text: string,
    onClick?: () => any;
    className?: string;
}