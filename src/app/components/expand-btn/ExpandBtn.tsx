import './ExpandBtn.style.scss';
import { IExpandBtnProps } from './IExpandBtn';

export class ExpandBtn extends React.Component<IExpandBtnProps> {
    static defaultProps: IExpandBtnProps = {
        isExpanded: false,
        text: 'Подробнее',
        className: ''
    }

    private onClick = () => {
        const { onClick } = this.props;
        if (onClick) { onClick(); }
    }

    render() {
        const { isExpanded, className } = this.props;

        return (
            <div className={`expand-btn ${isExpanded ? 'expand-btn--expanded' : ''} ${className}`} onClick={this.onClick}>{this.props.text} <i></i></div>
        )
    }
}