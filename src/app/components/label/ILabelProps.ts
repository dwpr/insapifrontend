



export interface ILabelProps {
	/**
	 * Кэпшн лейбла
	 * @type {string| JSX.Element;}
	 * @memberof ILabelProps
	 */
	caption : string| JSX.Element;
	/**
	 * Контент тултипа
	 * @type {(string | JSX.Element)}
	 * @memberof ILabelProps
	 */
	tooltip? : string | JSX.Element;

	/**
	 * Тип тултипа
	 * @type {string}
	 * @memberof ILabelProps
	 */
	tooltipType? : string;

	/**
	 * Класс тултипа
	 * @type {string}
	 * @memberof ILabelProps
	 */
	tooltipClass? : string;
	onTooltipClick?() : void;

	/**
	 * Доп класс для блока
	 * @type {string}
	 * @memberof ILabelProps
	 */
	className? : string;

	/**
	 * Ширина тултипа
	 * @type {string}
	 * @memberof ILabelProps
	 */
	tooltipWidth? : string;
}