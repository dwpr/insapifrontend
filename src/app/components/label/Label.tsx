import { ILabelProps } from './ILabelProps';
import { Tooltip } from "../tooltip/Tooltip";
import * as classNames from 'classnames';




export const Label = ({ caption, tooltip = '', className = '', tooltipType='', tooltipClass='', tooltipWidth='', onTooltipClick } : ILabelProps ) => (
	<div className={classNames('label', className)}>
		<span className='label__caption'>{caption}</span>
		{tooltip && <Tooltip className={'label__tooltip ' + tooltipClass} tooltip={tooltip} tooltipType={tooltipType} width={tooltipWidth} onClick={onTooltipClick}  tooltipPosition='top'/>}
	</div>
);