import "./AgeField.style.scss";
import Mask from 'react-input-mask';
import { IAgeFieldProps } from './IAgeFieldProps';
import { IAgeFieldState } from './IAgeFieldState';
import { Component, IBemComponentProps } from '../../lib/react-bem-component';
import * as classNames from 'classnames';
import { ErrorBlock } from '../error-block/ErrorBlock';
// import { FormElement } from '../form/FormElement';
import * as NumberInput from 'react-number-format';

// @FormElement
export class AgeField extends Component<IAgeFieldProps, IAgeFieldState> {

	/**
	 * 
	 * @static
	 * @memberof Field
	 */
	static defaultProps = {
		namespace: 'age-field',
		className: '',
		type: 'text',
		onKeyDown: () => { },
	}
	/**
	 * 
	 * @type {IFieldState}
	 * @memberof Field
	 */
	state: IAgeFieldState = {
		isFocused: false
	}
	input: any = null;

	/**
	 * @desc реф на рутовый див
	 */
	fieldRootElement = null;

	onChange = (event: React.SyntheticEvent<any>) => {
		const { name, value } = this.props;
		const onChange = this.props.onChange || this.context.onChange || (() => { });
		if (event.currentTarget.value > 99) {
			onChange({ props: this.props, value: '99', name });
		} else {
			onChange({ props: this.props, value: event.currentTarget.value, name });
		}

	}

	/**
	 * Фокус поля
	 * @memberof Field
	 */
	public onFocus = (event: React.SyntheticEvent<any>) => {

		const { name, value, carretToEnd } = this.props;
		const onChange = this.props.onChange || this.context.onChange || (() => { });
		const onFocus = this.props.onFocus || this.context.onFocus || (() => { });
		if (value == '') {
			onChange({ props: this.props, value: '', name });
		}
		this.setState({ isFocused: true });
		if (Utils.browser.detectIE) {
			let input = this.refs.input as any;
			if (input) {
				input.selectionStart = 100000;
				input.selectionEnd = 10000;
			}
		}
		onFocus({ props: this.props, name, value });
	}
	/**
	 * Отфокус поля
	 * @memberof Field
	 */
	public onBlur = (event: React.SyntheticEvent<any>) => {
		const { name, value } = this.props;
		const onChange = this.props.onChange || this.context.onChange || (() => { });
		const onBlur = this.props.onBlur || this.context.onBlur || (() => { });
		if (value == '' && !this.props.placeholder) {
			onChange({ props: this.props, value: '', name });
		}
		// if ((Number(value) === 0)) {
		// 	onChange({ props: this.props, value: '', name });
		// }
		this.setState({ isFocused: false });
		onBlur({ props: this.props, name, value });
	}

	public focus = () => {
		if (this.fieldRootElement) {
			let input = (this.fieldRootElement as any).querySelector('input');
			input.focus();
		}
	}

	render() {
		const { element, block, modifier, onChange, onFocus, onBlur } = this;
		const { isFocused } = this.state;
		const { className, id, name, value, error, isError, tabIndex, placeholder, pattern, permanentError, onRemoveValue, allowRemove} = this.props;


		const blockClassName = classNames(
			block(),
			{ [modifier('focused')]: isFocused },
			{ [modifier('error')]: isError || Boolean(permanentError) },
			{ [modifier('filled')]: value },
			className
		);

		const inputProps = {
			value,
			className: element('input'),
			tabIndex,
			name,
			id,
			onChange,
			onFocus,
			onBlur
		};

		let input = null;
		

		return (
			<div className={blockClassName} ref={fieldRootElement => this.fieldRootElement = fieldRootElement}>
					<i className={this.props.value >= 16 || !this.props.value ? 'icon-adult' : 'icon-kid'}></i>
				<div className={element('text-box')} >
					{/* <input ref='input' {...inputProps} placeholder={placeholder} onKeyDown={e => onKeyDown(e)} inputMode='numeric' pattern={'\\d*'}/> */}
				<NumberInput
					ref='input'
					{...inputProps} 
					placeholder={placeholder}
					decimalScale={0}
					suffix={isFocused ? '' : Utils.text.getWordForm(value, [' год', ' года', ' лет'])}
					onChange={() => {}}
					onValueChange={(event) => {
						this.onChange({
							currentTarget: {
								value: event.value
							}
						} as any);
					}}
					allowNegative={false}
				/>
				</div>
				{allowRemove && <div className='icon-close' onClick={onRemoveValue}></div>}
				{(error || Boolean(permanentError)) && <div className={element('error')} >{error || permanentError}</div>}
			</div>
		)
	}
}
