export interface IAgeFieldState {
	/**
	 * Поле в фокусе
	 * @type {boolean}
	 * @memberof IAgeFieldState
	 */
	isFocused: boolean;
}