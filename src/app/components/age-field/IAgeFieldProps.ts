import { IControlEvent } from '../../lib/IControlEvent';
export interface IAgeFieldProps {
	/**
	 * Класс рутового элемента
	 * @type {string}
	 * @memberof IAgeFieldProps
	 */
	className? : string;
	/**
	 * Атрибут id инпута
	 * @type {string}
	 * @memberof IAgeFieldProps
	 */
	id : string;
	/**
	 * Название поля в модели также атрибута тега input
	 * @type {string}
	 * @memberof IAgeFieldProps
	 */
	name : string;
	/**
	 * Value
	 * @type {*}
	 * @memberof IAgeFieldProps
	 */
	value : any;
	/**
	 * Функция которая будет вызвана при вводе
	 * @returns {*} 
	 * @memberof IAgeFieldProps
	 */
	onChange?(event : IControlEvent) : any;
	/**
	* validation error
	*/
	isError ? : boolean;
	/**
	 * Строка или блок с ошибкой
	 */	
	error? : string | JSX.Element; 
	/**
	 * Постоянная ошибка, прокинутая не через валидатор
	 * @type {(string | JSX.Element)}
	 * @memberOf IAgeFieldProps
	 */
	permanentError ?: string | JSX.Element;
	/**
	 * Регулярное выражение или название пресета для фильтра ввода
	 * @type {(RegExp | string)}
	 * @memberof IAgeFieldProps
	 */
	regExp? : RegExp | string;
	/**
	 * Tab index input'a
	 * @type {number}
	 * @memberof IAgeFieldProps
	 */
	tabIndex? : number;
	/**
	 * Значение атрибута placeholder input'a
	 * @type {string}
	 * @memberof IAgeFieldProps
	 */
	placeholder? : string;
	/**
	 * Значение атрибута pattern input'a
	 * @type {string}
	 * @memberof IAgeFieldProps
	 */
	pattern? : string;
	/**
	 * Вызывается при фокусе поля
	 * @param {IControlEvent} event 
	 * @returns {*} 
	 * @memberof IAgeFieldProps
	 */
	onFocus?(event : IControlEvent) : any;
	/**
	 * Вызывается при отфоксе из поля
	 * @param {IControlEvent} event 
	 * @returns {*} 
	 * @memberof IAgeFieldProps
	 */
	onBlur?(event : IControlEvent) : any;
	/**
	 * Вызывается при нажатии на клавиши при фокусе инпута.
	 * @returns {*} 
	 * @memberof IAgeFieldProps
	 */
	onKeyDown?(event : any) : any;

	carretToEnd ?: boolean;
	/**
	 * Вызывается при нажатии на крестик
	 * @returns {*} 
	 * @memberof IAgeFieldProps
	 */
	onRemoveValue?(event : any) : any;

	allowRemove?: boolean
}