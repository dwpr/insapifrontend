import { Component } from '../../lib/react-bem-component';
import { ISectionPreloaderProps } from './ISectionPreloaderProps';
import { image } from '../../app';
import * as classNames from 'classnames';






export class SectionPreloader extends Component<ISectionPreloaderProps> {
	static defaultProps = {
		namespace: 'section-preloader'
	}
	render() {
		const { isLoaded, children, className } = this.props;
		const {element, block} = this;
		
		// if (isLoaded) return children;

		return (
			<div className={classNames(block(), className)}>
				<img className={element('image')} src={image('preloader.gif')}  />
			</div>
		)

	}
}