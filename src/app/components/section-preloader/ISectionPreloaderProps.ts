




export interface ISectionPreloaderProps {
	isLoaded? : boolean;
	className? : string;
	children?: any;
}