import { Component } from "../../lib/react-bem-component/index";
import { INotificationProps } from "./INotificationProps";
import * as classNames from 'classnames';
import { image } from "../../app";





export class Notification extends Component<INotificationProps> {

    state = {
        isVisible: false,
    }

    static defaultProps = {
        namespace: 'notification'
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ isVisible: nextProps.isVisible });
    }

    hideNotification = () => this.setState({ isVisible: false })

    render() {
        if (!this.state.isVisible) return null;
        const { block, element, modifier, hideNotification } = this;
        const { text, type, className } = this.props;
        const blockClassName = classNames(
            block(),
            { [modifier('middle')]: type == 'middle' },
            className
        )
        return (
            <div className={blockClassName}>
                <div className={element('close')} onClick={hideNotification}>
                    <img src={image('popover-close.png')} alt=""/>
                </div>
                <div className={element('text')}>{text}</div>
            </div>
        )
    }
}