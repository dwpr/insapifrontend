



export interface INotificationProps {
    /**
     * Текст сообщения
     * @type {(string | JSX.Element)}
     * @memberOf INotificationProps
     */
    text : string | JSX.Element;
    /**
     * Тип notification
     * @type {string}
     * @memberOf INotificationProps
     */
    type? : string;
    /**
     * Класс notification
     * @type {string}
     * @memberOf INotificationProps
     */
    className? : string;
    /**
     * Показывать сообщение
     * @type {boolean}
     * @memberOf INotificationProps
     */
    isVisible : boolean;
}