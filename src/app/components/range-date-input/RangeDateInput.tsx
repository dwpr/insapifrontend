import { ErrorBlock } from '../error-block/ErrorBlock';
import { IControlEvent } from '../../lib/IControlEvent';
import { RangeDatePicker } from '../range-date-picker/RangeDatePicker';
import { Field } from '../field/Field';
import { Component } from '../../lib/react-bem-component';
import Mask from 'react-input-mask';
import * as moment from 'moment';
import { IRangeDateInputProps } from './IRangeDateInputProps';
import * as classNames from 'classnames';
export class RangeDateInput extends Component<IRangeDateInputProps> {

	static defaultProps = {
		namespace: 'range-dateinput',
		onBlur: () => { },
		onFocus: () => { },
		onChange: () => { }
	}

	state = {
		isDatePickerVisible: false,
		tabForOpen: ''
	}


	transformToIso = (dateString: string) => {
		let value = dateString;
		if (dateString) {
			const [day, month, year] = value.split('.');
			value = `${year}-${month}-${day}`;
		}
		return value;
	}

	onChange = (event) => {
		const { onChange } = this.props;
		// if(event.target.value.indexOf('_') > -1) return
		// let value = this.transformToIso(event.target.value);
		// ''
		onChange({
			value: event.target.value,
			name: event.target.name,
			props: this.props
		});
	}

	/**
	 * 
	 * @memberof RangeDateInput
	 */
	onChangeMobileInput = (event, config) => {
		const { onChange } = this.props;
		onChange({
			value: event.target.value,
			name: config.name,
			props: this.props
		});
	}


	/**
	 * На blur если дата заполнена полностью то пропускаем ее через момент.
	 * @memberof RangeDateInput
	 */
	onBlur = (event) => {
		const { onBlur, onChange } = this.props;

		const controlEvent: IControlEvent = {
			name: event.target.name,
			value: event.target.value,
			props: this.props
		};

		if (controlEvent.value.indexOf('_') == -1 && controlEvent.value) {
			const date = moment();
			const [day, month, year] = controlEvent.value.split('.');
			date.year(year);
			date.month(month - 1);
			date.date(day);
			controlEvent.value = date.format('YYYY-MM-DD');
			if (date.isValid()) onChange(controlEvent);
		} else {
			controlEvent.value = this.transformToIso(controlEvent.value);
		}
		onBlur(controlEvent);
	}

	/**
	 * Переформатируем из iso в rus
	 * @memberof RangeDateInput
	 */
	insureDateIsRusFormat = (dateString: string) => {
		if (!dateString && dateString != '0') return '';
		if (dateString.match(/\./gi)) return dateString;
		if (dateString.indexOf('-') > -1) {
			const [year, month, day] = dateString.split('-');
			return `${day}.${month}.${year}`;
		}
	}

	setDatePickerVisibility = (isDatePickerVisible) => {
		this.setState({ isDatePickerVisible });
		if (!isDatePickerVisible) this.setState({ tabForOpen: '' });
	}

	onShowClick = (tab) => {
		// if (Utils.browser.isMobile()) return (this.refs.dateInput as any).focus();
		this.setState({ isDatePickerVisible: !this.state.isDatePickerVisible, tabForOpen: tab });
	}

	render() {

		const { to, from, label, className, isError, minDate, isErrorBlockVisible, maxDate } = this.props;
		const { isDatePickerVisible, tabForOpen } = this.state;
		const { block, element, elementModifier, onBlur, onChange, modifier } = this;
		let amountOfDays;
		if (to.value) {
			amountOfDays = moment(to.value).diff(moment(from.value), 'days') + 1;
		}
		return (
			<div className={classNames(block(), className, { [modifier('error')]: isError })}>
				{label && <label className={element('label')} >{label}</label>}
				<div className={element('input-group')} >
					<div className={classNames(element('field'), element('field-from'))} onClick={e => this.onShowClick('from')}>
						{from.value ?
							<div className={element('date-wrap')}>
								<div className={element('date')}>{moment(from.value).format('D')}</div>
								<div className={element('date-inner')}>
									<div className={element('month')}>{Utils.date.getShortMonth(from.value)}</div>
									<div className={element('year')}>{moment(from.value).format('YYYY')}</div>
								</div>								
							</div>
							:
							<div className={element('placeholder')}>Туда</div>
						}
						<div className={classNames(element('calendar-btn'), { [element('calendar-btn--active')]: tabForOpen == 'from' })} onClick={e => this.onShowClick('from')} >
							<i className="icon-calendar"></i>
						</div>
					</div>

					<div className={element('separator')}></div>
					<div className={classNames(element('field'), element('field-to'))} onClick={e => this.onShowClick('to')}>
						{to.value ?
							<div className={element('date-wrap')}>
								<div className={element('date')}>{moment(to.value).format('D')}</div>
								<div className={element('date-inner')}>
									<div className={element('month')}>{Utils.date.getShortMonth(to.value)}</div>
									<div className={element('year')}>{moment(to.value).format('YYYY')}</div>
								</div>								
							</div>
							:
							<div className={element('placeholder')}>Обратно</div>
						}
						<div className={classNames(element('calendar-btn'), { [element('calendar-btn--active')]: tabForOpen == 'to' })} onClick={e => this.onShowClick('to')} >
							<i className="icon-calendar"></i>
						</div>
					</div>
					{to.value && amountOfDays > 0 && <div className={element('total')}>{amountOfDays} {Utils.text.getWordForm(amountOfDays, ['день', 'дня', 'дней'])}</div>}
				</div>
				<div className={element('datepicker-container')} >
					{isDatePickerVisible && <RangeDatePicker tabForOpen={this.state.tabForOpen} onClickOutside={e => this.setDatePickerVisibility(false)} 
					onToSelect={e => this.setDatePickerVisibility(false)} from={{ ...from, minDate, maxDate }} to={{ ...to, minDate: from.value || minDate, maxDate }} onChange={this.props.onChange} onChangeActiveTab={tabName => this.setState({ tabForOpen: tabName })}  />}
				</div>
			</div>
		);

	}
}