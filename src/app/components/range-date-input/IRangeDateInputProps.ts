import { IDatePickerProps } from '../datepicker/IDatePickerProps';
import { IControlEvent } from '../../lib/IControlEvent';

export interface IRangeDateInputProps {
	from: IDatePickerProps;
	to: IDatePickerProps;
	mask?: string;
	maskChar?: string;
	label?: string | JSX.Element;
	className?: string;
	/**
	 * ошибка, которую передает в компонент его валидатор
	 */
	error?: string | boolean | JSX.Element;
	/**
	* validation error
	*/
	isError ? : boolean;
	/**
	 * @desc дата начала - в формате YYYY-MM-DD
	 */
	startDate?: string;
	/**
	 * @desc дата конца - в формате YYYY-MM-DD
	 */
	endDate?: string;
	/**
	 * событие на OnChange
	 */
	onChange(event: IControlEvent): any;
	/**
	 * событие на OnBlur
	 */
	onBlur?(event: IControlEvent): any;
	/**
	 * событие на фокусе
	 */
	onFocus?(event: IControlEvent): any;
	/**
	 * минимально допустимая к выбору дата
	 */
	minDate?: any;
	/**
	 * максимально допустимая к выбору дата
	 */
	maxDate?: any;
	isErrorBlockVisible ?: boolean;
	onMountCallback?: any;
}