import './MainLoader.style.scss'

const MainLoader = () => (
    <div className='main-loader'>
        <i className='main-loader__1'/>
        <i className='main-loader__2'/>
        <i className='main-loader__3'/>
        <i className='main-loader__4'/>
        <i className='main-loader__5'/>
        <i className='main-loader__6'/>
        <i className='main-loader__7'/>
        <i className='main-loader__8'/>
    </div>
)

export default MainLoader;