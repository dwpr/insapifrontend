import './PlanePreloader.style.scss';
import { IPlanePreloaderProps, IPlanePreloaderState } from './IPlanePreloader';

export class PlanePreloader extends React.Component<IPlanePreloaderProps, IPlanePreloaderState> {

    public static defaultProps : IPlanePreloaderProps = {
        total: 0,
        loaded: 0
    }


    public state: IPlanePreloaderState = {
        
    }

    componentWillMount() {

    }

    componentDidMount() {
        console.log("loaded ", this.props.loaded);    
        console.log("total ", this.props.total);      
    }

    get loadedPercentage() {
        let { total, loaded } = this.props;

        return Math.floor(loaded / total * 100);
    }


    public render() {
        let loadedPercentage = this.loadedPercentage;

        return (
            <div className='plane-preloader'>
                <div className={`plane-preloader__yellow-block ${this.props.loaded == this.props.total ? 'plane-preloader__yellow-block--done' : ''}`} style={{ width: loadedPercentage < 8 ? '8%' : loadedPercentage + '%' }}>
                    <img className={`plane-preloader__plane-pic ${this.props.loaded == this.props.total ? 'plane-preloader__plane-pic--hidden' : '' }`} src={require('../../../public/img/plane.svg')} alt=""/>
                </div>                
            </div>
        )
    }
}