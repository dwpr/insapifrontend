import { IPolicyTypeProps } from './IPolicyTypeProps';
import * as classNames from 'classnames';






export const PolicyType = ({ className, type, name } : IPolicyTypeProps) => (
	<div className={classNames('policy-type', className)} >
		<div className='policy-type__content'>
			<div className={`policy-type__icon policy-type__icon--${type || 'electronic'}`} ></div>
			<div className='policy-type__desc' >
				<div className='policy-type__desc-title' >Вид полиса</div>
				<div className='policy-type__type-name'>{name}</div>
			</div>
		</div>
	</div>
);
