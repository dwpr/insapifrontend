


export interface IPolicyTypeProps {
	/**
	 * Класс враппера
	 * @type {string}
	 * @memberof IPolicyTypeProps
	 */
	className ?: string;
	/**
	 * Ссылка на иконку
	 * @type {string}
	 * @memberof IPolicyTypeProps
	 */
	type ?: string;
	/**
	 * Тип полиса
	 * @type {string}
	 * @memberof IPolicyTypeProps
	 */
	name : string;
}
