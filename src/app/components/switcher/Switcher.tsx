import './Switcher.style.scss';
import { ISwitcherState } from './ISwitcherState';
import { ISwitcherProps } from './ISwitcherProps';
import { Component, IBemComponentProps } from '../../lib/react-bem-component';
import * as classNames from 'classnames';


export class Switcher extends Component<ISwitcherProps, ISwitcherState> {
    
    static defaultProps = {
        namespace: 'switcher',
        className: ''
    }
    state : ISwitcherState = {

    }
    onChange = (value) => {
        const { options, onChange, name, disabled } = this.props;
        //если контрол в состоянии дизейблед
        if(disabled) return null;
        //если выбраная опция дизейблед
        if(options.find(option => option.value == value).disabled) return null;
        if(value == this.props.value) return null;


		onChange({
			props: this.props,
			value,
			name
		});
    }
    /**
     * Кастомный рендер options
     * @param option 
     * @memberOf Switcher
     */
    optionRenderer = (option) => {
        const { optionRenderer } = this.props;
        if (optionRenderer) option = optionRenderer(option);
        return option;
    }

    getOptionClassName = (optionValue) => {
        const { element, elementModifier } = this;
        const { value, options } = this.props;

        const isOptionDisabled : boolean = options.find(option => option.value == optionValue).disabled;

        return classNames(
            element('option'),
            { [elementModifier('option', 'active')] : optionValue == value},
            { [elementModifier('option', 'disabled')] : isOptionDisabled }
        )
    }

    render() {
        const { element, block, modifier, onChange, elementModifier, optionRenderer, getOptionClassName } = this;
		const { id, name, className, value, options, tabIndex, disabled } = this.props;

		const blockClassName = classNames(
			block(),
            className,
            { [modifier('disabled')]: disabled }
		);
        return(
            <div className={blockClassName}>
                <div className='switcher__wrap'>
                    {options.map((item, index) => (
                        <div key={`switcher${id}${index}`} className={getOptionClassName(item.value)} onClick={() => onChange(item.value)}>
                            {optionRenderer(item.label)}
                            {item.tooltip && <div className='switcher__tooltip'>{item.tooltip}</div>}
                        </div>
                    ))}
                    <div className='switcher__active-block'></div>
                </div>                
            </div>
        )
    }
}