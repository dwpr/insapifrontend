import { IControlEvent } from '../../lib/IControlEvent';


export interface ISwitcherProps {
    /**
     * Атрибут id
     * 
     * @type {string}
     * @memberOf ISwitcherProps
     */
    id : string;
    /**
     * value 
     * @type {*}
     * @memberOf ISwitcherProps
     */
    value : any;
    /**
     * ClassName
     * 
     * @type {string}
     * @memberOf ISwitcherProps
     */
    className? : string;
    /**
     * Массив опций
     * 
     * @type {Array<any>}
     * @memberOf ISwitcherProps
     */
    options : Array<any>;
    /**
     * Поле в модели
     * 
     * @type {string}
     * @memberOf ISwitcherProps
     */
    name : string;
    /**
     * Функция для обработки события change
     * 
     * @param {IControlEvent} event 
     * @returns {*} 
     * 
     * @memberOf ISwitcherProps
     */
    onChange(event : IControlEvent) : any;
    /**
     * tab index
     * 
     * @type {number}
     * @memberOf ISwitcherProps
     */
    tabIndex? : number;
    /**
     * Отрисовка options
     * 
     * @param {string} option 
     * @returns {(string | JSX.Element)} 
     * 
     * @memberOf ISwitcherProps
     */
    optionRenderer?(option : string) : string | JSX.Element;
    /**
     * @desc является ли контрол неактивным
     */
    disabled?:boolean;
}