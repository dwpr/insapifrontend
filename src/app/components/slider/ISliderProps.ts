

export interface ISliderProps {
    arrows?: boolean;
    dots?: boolean,
    infinite?: boolean,
    speed?: number,
    slidesToShow?: number,
    slidesToScroll?: number,
    swipe?: boolean,
    centerMode?: boolean,
    initialSlide?: number,
    className?: string
}