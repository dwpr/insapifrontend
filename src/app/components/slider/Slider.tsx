import Slider from 'react-slick';
import { ISliderProps } from './ISliderProps';

export default class extends React.Component<ISliderProps> {
    static defaultProps = {
        arrows: true,
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 6,
        slidesToScroll: 1,
        swipe: false,
        centerMode: false,
        initialSlide: 0
    }
    render() {
        let {arrows, dots, infinite, speed, slidesToShow, slidesToScroll, swipe, centerMode, initialSlide, className} = this.props;

        let settings = {
            arrows,
            dots,
            infinite,
            speed,
            slidesToShow,
            slidesToScroll,
            swipe,
            centerMode,
            prevArrow: <LeftNavButton />,
            nextArrow: <RightNavButton />,
            initialSlide,
            variableWidth: true
        };
        return (
            <div className={className}>
                <Slider ref='slider' {...settings} className={this.props.className}>
                    {this.props.children}
                </Slider>
            </div>
        )
    }

}

class RightNavButton extends React.Component<any> {

    render() {
        return <div className='slider-next-btn'onClick={this.props.onClick} ><i className='icon-arrow-next'></i></div>
    }
}
class LeftNavButton extends React.Component<any> {
    render() {
        return <div className='slider-prev-btn' onClick={this.props.onClick}><i className='icon-arrow-prev'></i></div>
    }
}
