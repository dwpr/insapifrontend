import './Select.style.scss';
import { default as ReactVirtualizedSelect } from 'react-virtualized-select';
import { default as ReactSelect } from 'react-select';
import { AutoSizer, CellMeasurer, List, CellMeasurerCache, Grid } from 'react-virtualized';
import { ISelectProps } from './ISelectProps';
import { ISelectState } from './ISelectState';
import { Component, IBemComponentProps } from '../../lib/react-bem-component';
import * as classNames from 'classnames';
import { ErrorBlock } from '../error-block/ErrorBlock';
import { FormElement } from './../form/FormElement';



export interface ISelectState {

}


@FormElement
export class Select extends Component<ISelectProps, ISelectState> {
    /** 
     * @static
     * @memberOf Select
     */
    public static defaultProps = {
        namespace: 'select',
        className: '',
        multi: false,
        searchable: false,
        isSelectVisible: false,
        placeholder: 'Выберите страну',
        tabIndex: '',
        labelKey: 'label',
        useCustomFilter: true
        // clearable: false
    }
    /** 
     * @type {ISelectState}
     * @memberOf Select
     */
    public state: ISelectState = {
        isEmptyInput: true
    }

    public componentDidUpdate(prevProps, prevState) {
        /**
        * внутри компонента селекта при удалении выбранного элемента, вызывается фокус на инпуте
        * принудительно вызываем blur, если был удален элемент
        */
        if (this.props.multi) {
            if (prevProps.value.length > (this.props.value as any).length) {
                let input = (this.refs.select as any).querySelector('.Select-input input');
                if (input) {
                    input.blur();
                }
            }
        } 
    }
    /**
    * Функция обработки change 
    * @memberOf Select
    */
    public onChange = (event) => {
        event = event ? event.currentTarget || event : '';
        let value = this.props.multi ? [] : '';
        if (event) {
            if (event.value || event.value === 0) value = event.value;
            else value = event;
        }
        const { name, multi } = this.props;
        let multiValue;
        //Если свойство value - массив, создаем новый массив для эмита и пушим в него значения value.
        //в итоге если массив value у нас value = [ name: 'house', id: 1 ], получится массив value = [1]
        if (multi) {
            if (Array.isArray(this.props.value)) {
                multiValue = (value as Array<any>).map(item => item.id);
            } else {
                multiValue = (value as string).split(',');
            }
        }
        const onChange = this.props.onChange || this.context.onChange || (() => { });
        onChange({
            props: this.props,
            value: multi ? multiValue : value,
            name
        });
        let input = (this.refs.select as any).querySelector('.Select-input input');
        if (input) {
            input.blur();
        }
    }
    onFastOptionClick = (option) => {
        const { onChange, name, multi, value } = this.props;
        let emitValue = value;
        if (multi) {
            if (Array.isArray(value)) {
                const isInList = value.find(item => item.id == option.id);
                // if (isInList) emitValue = value.filter(item => item.value != option.value);
                // else
                if (!isInList) emitValue = value.concat([option.id]);
            } else {
                const listValue = value.toString().split(',');
                const index = listValue.indexOf(option.id);
                if (index == -1) {
                    emitValue = listValue.concat([option.id]).join(',');
                }
                // else {    
                //     emitValue = listValue.filter(item => item != option.value);
                // }
            }
        } else {
            emitValue = option.id;
        }
        onChange({
            props: this.props,
            value: emitValue,
            name
        });
    }
    /**
     * Рендер иконки для селекта 
     * @memberOf Select
     */
    arrowRenderer = () => {
        if ((this.props.value as any).length > 0) {
            return <span className={this.element('arrow')}></span>
        }
    }
    /**
     * Рендер опций 
     * @memberOf Select
     */
    optionRenderer = (option) => {
        const { options, labelKey, focusedOption, value } = this.props;
        const currentOption = option;
        // const optionClassName = classNames(
        //     this.element('option'),
        //     { [this.elementModifier('option', 'default')]: currentOption.isHighlighted },
        //     { [`icon-${currentOption.id}`]: currentOption.isHighlighted},
        //     { [this.elementModifier('option', 'first-common')]: !currentOption.isHighlighted && currentOption.id == options.filter(item => !item.isHighlighted)[0].id },
        //     { [this.elementModifier('option', 'selected')]: currentOption.id == value }
        // );

        const optionDefaultClassName = classNames(
            this.element('default-option'),
            { [`icon-${currentOption.id}`]: currentOption.isHighlighted},
        )

        const optionClassName = classNames(
            this.element('option'),
            { [this.elementModifier('option', 'selected')]: currentOption.id == value }
        )

        return (
            <div key={option.key} className={currentOption.isHighlighted ? optionDefaultClassName : optionClassName}>
                <span>{currentOption[labelKey]}</span>
            </div>
        )
    }
    /**
     * Для filterOptions, в начале отфильтрованного массива все совпадения по началу строки, потом все остальные. Массив уже отфильтрован по алфавиту
     * @param array - массив значений селекта
     * @param query - строка
     * @param options - текущие значения
     */
    customFilter = (array, query, options) => {
        if (options) array = array.filter(item => options.every(el => el.id != item.id));
        let filtered_default = array.filter(item => item.isHighlighted);
        let filtered_common = array.filter(item => !item.isHighlighted);
        let filtered_by_default = filtered_default.concat(filtered_common);
        if (!query) return filtered_by_default;
        let filtered_start = filtered_common.filter(item => item.name.toLowerCase().indexOf(query.toLowerCase()) == 0);
        let filtered_other = filtered_common.filter(item => item.name.toLowerCase().indexOf(query.toLowerCase()) > 0);
        let filtered = filtered_default.concat(filtered_start).concat(filtered_other);
        return filtered;
    }

    Select = (props) => {
        return <ReactSelect {...props} optionClassName={this.element('option-wrapper')} />
    }

    onCustomInputChange = (value) => {
        const { onInputChange } = this.props;
        //debugger;
        this.setState({ isEmptyInput: !value });
        if (onInputChange) onInputChange(value);
    }

    valueRenderer = (option) => {
        return <span>{option.name}</span>
    }

    render() {
        const { element, block, modifier, onChange, elementModifier, arrowRenderer, optionRenderer, customFilter, Select, onCustomInputChange, valueRenderer } = this;
        const { id, label, name, className, value, options, tabIndex, multi, searchable, placeholder, fastSelectOptions, isSelectVisible, error, disabled, isError, onBlur, onFocus, isScrollNeeded, isErrorBlockVisible, onInputChange, clearable, labelKey, focusedOption, useCustomFilter } = this.props;

        let onFastOptionClick = this.props.onFastOptionClick ? this.props.onFastOptionClick : this.onFastOptionClick;

        const blockClassName = classNames(
            block(),
            className,
            { [modifier('error')]: isError }
        );

        const selectBlockClassName = classNames(
            element('select-control'),
            { [elementModifier('select-control', 'invisible')]: !isSelectVisible },
            className
        );

        const listOption = options.map(item => {
            if ('id' in item) {
                return {
                    ...item,
                    value: item.id,
                    label: item.name || item.label
                };
            }
            return item;
        });
        const selectProps: any = {};
        if (useCustomFilter) {
            selectProps.filterOptions = customFilter;
        }

        return (

            <div ref='select' className={blockClassName}>
                {label && <label className={element('label')} >{label}</label>}
                <div className={element('wrapper')}>
                    <Select
                        name={name}
                        value={value}
                        valueRenderer={valueRenderer}
                        options={listOption}
                        onChange={onChange}
                        valueKey='value'
                        labelKey={labelKey}
                        arrowRenderer={arrowRenderer}
                        optionRenderer={optionRenderer}
                        className={element('select-control')}
                        multi={multi}
                        joinValues={true}
                        searchable={searchable}
                        tabIndex={tabIndex.toString()}
                        placeholder={placeholder}
                        disabled={disabled}
                        onBlur={onBlur}
                        onFocus={onFocus}
                        onInputChange={onCustomInputChange}
                        clearable={false}
                        scrollMenuIntoView={false}
                        autofocus={false}
                        resetValue={multi ? [] : ''}
                        blurOnRemove={true}
                        inputProps={{ autoComplete: 'off', autoCorrect: 'off', autoCapitalize: "off", spellCheck: 'false' }}
                        {...selectProps}
                    />

                </div>
                <select name={name} multiple={multi} id={id} value={value} onChange={this.onChange} className={selectBlockClassName} tabIndex={tabIndex}>
                    {listOption.map((option, index) => {
                        return (
                            <option key={`select-option${id}${option.id}`} value={option.id}>{option.name}</option>
                        )
                    })}
                </select>
                {fastSelectOptions &&
                    <div className={element('fast-options')}>
                        <span className={element('fast-option-text')}>Например:</span>
                        {fastSelectOptions.map((option, index) => {
                            return (
                                <div key={`fast-option${id}${index}`} className={element('fast-option-wrapper')}>
                                    <div className={element('fast-option')} onClick={() => onFastOptionClick(option)}>{option.name}</div>
                                    {index != fastSelectOptions.length - 1 && <span> </span>}
                                </div>
                            )
                        })}
                    </div>
                }
                {error && <div>
                    {error}
                </div>}
            </div>
        )
    }

}