import { IControlEvent } from '../../lib/IControlEvent';
import { Option, MenuRendererHandler } from 'react-select';

export interface ISelectProps {
    /**
     * Класс рутового элемента
     * 
     * @type {string}
     * @memberOf ISelectProps
     */
    className? : string;
    /**
     * Атрибут id
     * 
     * @type {string}
     * @memberOf ISelectProps
     */
    id : string;
    /**
     * Атрибут name = название поля в модели
     * 
     * @type {string}
     * @memberOf ISelectProps
     */
    name : string;
    /**
     * value
     * 
     * @type {(number | string | Array<any>)}
     * @memberOf ISelectProps
     */
    value :  Array<any> | string | number;
    /**
     * Опции селекта
     * 
     * @type {Array}
     * @memberOf ISelectProps
     */
    options : Array<any>;
    /**
     * label
     * 
     * @type {(string | JSX.Element)}
     * @memberOf ISelectProps
     */
    label ? : string | JSX.Element;
    /**
     * validation error
     */
    isError ? : boolean;
    /**
     * Строка или блок с ошибкой
     * 
     * @type {(string | JSX.Element)}
     * @memberOf ISelectProps
     */
    error? : string | JSX.Element;
    /**
     * tab index
     * 
     * @type {number}
     * @memberOf ISelectProps
     */
    tabIndex? : number;
    /**
     * Функция на change
     * 
     * @param {IControlEvent} event 
     * @returns {*} 
     * 
     * @memberOf ISelectProps
     */
    onChange?(event : IControlEvent) : any;
    /**
     * Срабатывает в том случае, если предоставлены быстрые опции и на них происходит клик.
     */
    onFastOptionClick?({ label, value }): any 
    /**
     * Индикатор мультиселекта 
     * @type {boolean}
     * @memberOf ISelectProps
     */
    multi? : boolean;
    /**
     * Поиск в селекте 
     * @type {boolean}
     * @memberOf ISelectProps
     */
    searchable? :boolean;
    /**
     * disabled в селекте 
     * @type {boolean}
     * @memberOf ISelectProps
     */
    disabled? :boolean;
    /**
     * placeholder 
     * @type {string}
     * @memberOf ISelectProps
     */
    placeholder? :string;
    /**
     * Options для быстрого выбора 
     * @type {Array<any>}
     * @memberOf ISelectProps
     */
    fastSelectOptions? : Array<any>;
    /**
     * Видимость обычного селекта
     * @type {boolean}
     * @memberOf ISelectProps
     */
    isSelectVisible? : boolean;

    index ?: any;

    focusedOption ?: any;

    onBlur?() : void;
    onFocus?() : void;
    isScrollNeeded?: boolean;
    isErrorBlockVisible ?: boolean;
    onInputChange? : any;
    clearable? : boolean;
    // onInputChange?(inputVaslue : string): void;
    labelKey? : string;
    useCustomFilter ?: boolean;
}