export interface ISelectState {
    /**
     * Заполнено ли значение селекта с клавиатуры
     * 
     * @type {boolean}
     * @memberOf ISelectState
     */
    isEmptyInput : boolean
}