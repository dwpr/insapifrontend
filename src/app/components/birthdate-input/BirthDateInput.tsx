import { IControlEvent } from '../../lib/IControlEvent';
// import { DatePicker } from '../datepicker/DatePicker';
import { Component } from '../../lib/react-bem-component';
import Mask from 'react-input-mask';
import * as moment from 'moment';
import { IBirthdateInputProps } from './IBirthdateInput';
import * as classNames from 'classnames';
import { BirthDatePicker } from './birthdate-datepicker/BirthDatePicker';
import './BirthdateInput.style.scss';


export class BirthdateInput extends Component<IBirthdateInputProps> {


	static defaultProps = {
		namespace: 'field',
		onBlur: () => { },
		onChange: () => { },
		onFocus: () => { }
	}

	state = {
		isDatePickerVisible: false,
		isFocused: false
	}


	transformToIso = (dateString: string) => {
		let value = dateString;
		if (dateString) {
			const [day, month, year] = value.split('.');
			value = `${year}-${month}-${day}`;
		}
		return value;
	}

	openCalendar = () => {
		this.setState({ isDatePickerVisible: true });
		this.onFocus();
	}

	onChange = (event) => {
		let { value } = event.target;
		if (!value) return;
		value = this.transformToIso(value);
		const { onChange, name } = this.props;
		onChange({
			value,
			props: this.props,
			name
		});
	}

	onClickCalendar = () => {
		this.openCalendar();
	}

	onBlur = () => {
		const { onBlur, value, name } = this.props;
		this.setState({ isFocused: false });
		//  если поле пустое, убираем маску из value
		if (!value.match(/[0-9]/)) {
			this.props.onChange({
				value: '',
				props: this.props,
				name
			});
		}
		onBlur();
	}

	onFocus = () => {
		const onFocus = this.props.onFocus || this.context.onFocus || (() => { });
		this.setState({ isFocused: true });
		onFocus();
	}

	/**
	 * Переформатируем из iso в rus
	 */
	insureDateIsRusFormat = (dateString: string) => {
		if (!dateString && dateString != '0') return '';
		if (dateString.match(/\./gi)) return dateString;
		if (dateString.indexOf('-') > -1) {
			const [year, month, day] = dateString.split('-');
			return `${day}.${month}.${year}`;
		}
	}

	onDatePickerChange = (event: IControlEvent) => {
		this.openCalendar();
		this.props.onChange(event);
	}

	onCloseDatePicker = () => {
		this.setState({ isDatePickerVisible: false });
		this.onBlur();
	}

	render() {
		const { name, value, label, className, id, error, isError, defaultAge, permanentError } = this.props;
		const { isDatePickerVisible, isFocused } = this.state;
		const { block, element, modifier, onChange, onFocus, onBlur } = this;

		const blockClassName = classNames(
			block(),
			'birthdate-input',
			className,
			{ [modifier('error')]: isError || Boolean(permanentError) },
			{ [modifier('filled')]: value },
			{ [modifier('focused')]: isFocused }
		);

		return (
			<div id={id} className={blockClassName}>
				<div className={element('text-box')} >
					<div className='birthdate-input__wrapper' >
						<Mask id={id + '-input'}
							className={element('input')}
							onChange={onChange}
							mask={'99.99.9999'}
							maskChar={'_'}
							onBlur={onBlur}
							onFocus={onFocus}
							value={value ? this.insureDateIsRusFormat(value): ''} />
						<div className={`birthdate-input__calendar-btn ${isDatePickerVisible ? 'birthdate-input__calendar-btn--active' : ''}`} onClick={this.onClickCalendar}  >
							<i className="icon-calendar"></i>
						</div>
					</div>
					{label && <label className={element('label')} >{label}</label>}
				</div>

				{(error || Boolean(permanentError)) && <div className={element('error')} >{error || permanentError}</div>}

				{isDatePickerVisible && <BirthDatePicker onClickOutside={this.onCloseDatePicker} onFullSelect={this.onCloseDatePicker} onChange={this.onDatePickerChange} name={name} value={value} defaultAge={defaultAge} />}

			</div>
		)
	}
}


