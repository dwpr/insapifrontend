import { IControlEvent } from '../../../lib/IControlEvent';

export interface IBirthDatePickerProps {
	/**
	 * Обработчик выбора даты 
	 */
	onChange?(event: IControlEvent): any;

	/**
	 * Название поля в модели
	 */
	name?: string;
	/**
	 * Текущая выбранная дата в ISO
	 */
	value?: any;
	/**
	 * Класс wrapper'a
	 */
	className?: string;
	/**
	 * Срабатывает при выборе даты
	 */
	onFullSelect?(event) : any;

	/**
	 * Срабатывает при клике за календарем
	 */
	onClickOutside?(event) : any;
	/**
	* возраст по дефолту
	*/
	defaultAge?: string | number;
	
}

export interface IBirthDatePickerState {
    /**
     * Текущая открытая вкладка
     */
    currentTab : string;
    /**
     * Текущая дата
     */
    currentDate : string;
}