import { IControlEvent } from '../../../lib/IControlEvent';
import { Component, IBemComponentProps } from '../../../lib/react-bem-component';
import * as moment from 'moment';
import * as classNames from 'classnames';
import { IBirthDatePickerProps, IBirthDatePickerState } from './IBirthDatePicker';
import './BirthDatePicker.style.scss';

export class BirthDatePicker extends Component<IBirthDatePickerProps, IBirthDatePickerState> {
	/**
	 * Список название месяцев для календаря
	 * @type {string[]}
	 * @memberof DatePicker
	 */
    listMonths: string[] = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

    listTab = [
        { name: 'day', caption: 'День' },
        { name: 'month', caption: 'Месяц' },
        { name: 'year', caption: 'Год' }
    ];

    state = {
        currentTab: 'day',
        currentDate: this.props.value || '____-__-__'
    }

    static defaultProps = {
        namespace: 'birth-datepicker',
        onChange: (event: IControlEvent) => { },
        onClickOutside: () => { },
        onSelect: () => { }
    }

    componentWillMount() {
        const { currentDate } = this.state;
        //  открываем вкладку первого незаполненного показателя (дата, месяц, год)
        let indexEmptyValue = currentDate.lastIndexOf('_');
        if (indexEmptyValue == -1 || indexEmptyValue > 7) { 
            return this.setState({ currentTab: 'day' }); 
        }
        if (indexEmptyValue > 4) {
            return this.setState({ currentTab: 'month' });
        }
        return this.setState({ currentTab: 'year' });
    }


    componentDidMount() {
        if (this.state.currentTab == 'year') {
            this.scrollToDefaultYear();
        }
        window.addEventListener('mouseup', this.onClickOutsideContainer);
    }
    componentWillUnmount() {
        window.removeEventListener('mouseup', this.onClickOutsideContainer)
    }

    componentDidUpdate(prevState) {
        if (prevState.currentTab != this.state.currentTab && this.state.currentTab == 'year') {
            this.scrollToDefaultYear();
        }
    }

    /**
	 * Вкладку года открываем так, чтобы виден был предполагаемый год рождения туриста
	 */

    scrollToDefaultYear = () => {
        const { defaultAge } = this.props;
        const minAge = 0;
        const maxAge = 100;

        let containerTop = document.getElementById('yearContainer').offsetTop;
        let currentAge = defaultAge && defaultAge >= minAge && defaultAge < maxAge ? Number(defaultAge) : 30;
        let currentYearTop = document.getElementById((moment().year() - currentAge).toString()).offsetTop;
        
        document.getElementById('yearContainer').scrollTop = currentYearTop - containerTop;
    }

    getDays() {
        let days = [];
        for (let i = 1; i <= 31; i++) {
            days.push(i);
        }
        return days;
    }

    getYears() {
        let years = [];
        for (let i = new Date().getFullYear() - 99; i <= new Date().getFullYear(); i++) {
            years.push(i);
        }
        return years;
    }


	/**
	 * При клике за контейнер прячем 
	 */
    public onClickOutsideContainer = (event) => {
        const { onClickOutside } = this.props;
        const container: HTMLElement = this.refs.container as HTMLElement;
        if (!container.contains(event.target)) onClickOutside(event);
    }

    public onChange = (value) => {
        const { onChange, onFullSelect, name } = this.props;
        const { currentTab } = this.state;
        const { listTab } = this;
        onChange({
            value: value,
            name,
            props: this.props
        });
        // автоматическое переключение на следующую вкладку
        if (currentTab == listTab[listTab.length - 1].name) {
            onFullSelect(value);
            this.setState({ currentTab: listTab[0].name });
        } else {
            let currentTabIndex = listTab.findIndex(tab => tab.name == currentTab);
            this.setState({ currentTab: listTab[currentTabIndex + 1].name });
        }
    }

    /**
	 * Выбор значения из дейтпикера
	 */
    public onClickDate = (value) => {
        const { currentTab } = this.state;
        let [year, month, day] = this.state.currentDate.split('-');
        if (currentTab == 'day') {
            if (value < 10) {
                day = '0' + value;
            } else {
                day = value;
            }
        }
        if (currentTab == 'month') {
            if (value < 10) {
                month = '0' + value;
            } else {
                month = value;
            }
        }
        if (currentTab == 'year') {
            year = value;
        }
        let newDate = `${year}-${month}-${day}`;
        this.onChange(newDate);
        this.setState({ currentDate: newDate });
    }

    render() {

        const { block, element, elementModifier, listTab, listMonths } = this;
        const { className, value } = this.props;
        const { currentTab } = this.state;

        const tabsBlockModifier = (tab) => {
            switch (tab) {
                case 'day': return 'birth-datepicker__tabs--day';
                case 'month': return 'birth-datepicker__tabs--month';
                case 'year': return 'birth-datepicker__tabs--year';
            }
        }
        return (
            <div ref='container' className={classNames(block(), className)} >
                <div className={`${element('tabs')} ${tabsBlockModifier(currentTab)}`}>
                    {listTab.map((tab, index) => {
                        return <div
                            key={`tab${index}`}
                            className={classNames(element('tab'), elementModifier('tab', tab.name), { [elementModifier('tab', 'active')]: currentTab == tab.name })}
                            onClick={event => this.setState({ currentTab: tab.name })}>
                            {tab.caption}
                        </div>
                    })}
                </div>
                {currentTab == 'day' &&
                    <div className={element('days')}>
                        {this.getDays().map((day, index) => {
                            return <div id={day}
                                key={`day${index}`}
                                className={classNames(element('day'), element('date'), value ? day == value.slice(-2) ? elementModifier('date', 'active') : '' : '')}
                                onClick={() => this.onClickDate(day)}>
                                {day}
                            </div>
                        })}
                    </div>
                }
                {currentTab == 'month' &&
                    <div className={element('months')}>
                        {listMonths.map((month, index) => {
                            return <div
                                id={(index + 1).toString()}
                                key={`month${index}`}
                                className={classNames(element('month'), element('date'), value ? index + 1 == value.substr(5, 2) ? elementModifier('date', 'active') : '' : '')}
                                onClick={() => this.onClickDate(index + 1)}>
                                {month}
                            </div>
                        })}
                    </div>
                }
                {currentTab == 'year' &&
                    <div ref='yearContainer' id='yearContainer' className={element('years')}>
                        {this.getYears().map((year, index) => {
                            return <div
                                id={year}
                                key={`day${index}`}
                                className={classNames(element('year'), element('date'), value ? year == value.substr(0, 4) ? elementModifier('date', 'active') : '' : '')}
                                onClick={() => this.onClickDate(year)}>
                                {year}
                            </div>
                        })}
                    </div>
                }
            </div>
        )
    }
}


