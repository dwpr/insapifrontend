import { IControlEvent } from '../../lib/IControlEvent';

export interface IBirthdateInputProps {
	/**
	 * html id
	 */
	id?: string;
	/**
	 * Название поля в модели
	 */
	name: string;
	/**
	 * Значение
	 */
	value: any;
	/**
	* validation error
	*/
	isError ? : boolean;
	/**
	 * ошибка, которую передает в компонент его валидатор
	 */
	error?: string | boolean | JSX.Element;
	/**
	 * Постоянная ошибка, прокинутая не через валидатор
	 */
	permanentError ?: string | JSX.Element;
	/**
	 * Будет вызван при выборе даты
	 */
	onChange(event: IControlEvent);
	/**
	 * Вызывается на отфокусе из поля
	 */
	onBlur?();
	/**
	 * Вызывается на фокусе из поля
	 */
	onFocus?() : void;
	/**
	 * Плейсхолдер
	 */
	placeholder? : string;
	/**
	 * Клас врапера
	 */
	className? : string;
	/**
	 * Лейбл над полем
	 */
	label? : string | JSX.Element;
	/**
	 * возраст по дефолту
	 */
	defaultAge? : string | number;
	
}