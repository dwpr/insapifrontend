import { IControlEvent } from '../../lib/IControlEvent';
import { DatePicker } from '../datepicker/DatePicker';
import { Component } from '../../lib/react-bem-component';
import Mask from 'react-input-mask';
import * as moment from 'moment';
import { IDateInputProps } from './IDateInputProps';
import * as classNames from 'classnames';


export class DateInput extends Component<IDateInputProps> {


	static defaultProps = {
		maskChar: '_',
		mask: '99.99.9999',
		namespace: 'dateinput',
		placeholder: 'дд.мм.гггг',
		onBlur: () => { },
		onChange: () => { },
		onFocus: () => { },
		onOpenWithNullValue: () => { }
	}

	state = {
		isDatePickerVisible: false
	}


	transformToIso = (dateString: string) => {
		let value = dateString;
		if (dateString) {
			const [day, month, year] = value.split('.');
			value = `${year}-${month}-${day}`;
		}
		return value;
	}

	/**
	 * @desc открытие календаря. (не обработчик события)
	 */
	openCalendar = () => {
		this.setState({ isDatePickerVisible: true });


		if (this.props.value === null) {
			this.props.onOpenWithNullValue();
		}
	}

	onChange = (event) => {

		let { value } = event.target;
		if (!value) return
		const { onChange, name } = this.props;
		value = Utils.browser.isMobile() ? value : this.transformToIso(value);
		onChange({
			value,
			props: this.props,
			name
		});
	}

	onMobileInputChange = (event) => {
		const { onChange, name } = this.props;
		onChange({
			value: event.target.value,
			props: this.props,
			name
		});
	}

	onMobileBlur = (event) => {
		const { onBlur, name } = this.props;
		onBlur({
			props: this.props,
			name,
			value: event.target.value || this.props.value
		});

	}
	onClickCalendar = () => {
		const { mobileDateInput } = this.refs as any;
		if (Utils.browser.isMobile() && mobileDateInput) {
			mobileDateInput.focus();
		} else {
			this.openCalendar();
		}
	}
	/**
	 * На blur если дата заполнена полностью то пропускаем ее через момент.
	 * @memberof RangeDateInput
	 */
	onBlur = (event) => {
		const { onBlur, onChange, name } = this.props;
		const controlEvent: IControlEvent = {
			value: event.target.value,
			props: this.props,
			name
		};
		if (controlEvent.value.indexOf('_') == -1 && controlEvent.value) {
			const date = moment();
			const [day, month, year] = controlEvent.value.split('.');
			date.year(year);
			date.month(month - 1);
			date.date(day);
			controlEvent.value = date.format('YYYY-MM-DD');

			if (date.isValid()) onChange(controlEvent);
		} else {
			controlEvent.value = this.transformToIso(controlEvent.value);
		}
		onBlur(controlEvent);
	}

	/**
	 * Переформатируем из iso в rus
	 * @memberof RangeDateInput
	 */
	insureDateIsRusFormat = (dateString: string) => {
		if (!dateString && dateString != '0') return '';
		if (dateString.match(/\./gi)) return dateString;
		if (dateString.indexOf('-') > -1) {
			const [year, month, day] = dateString.split('-');
			return `${day}.${month}.${year}`;
		}
	}

	onDatePickerChange = (event: IControlEvent) => {
		this.openCalendar();
		this.props.onChange(event);
	}

	render() {
		const { mask, onChange, maskChar, name, value, placeholder, label, className, id, error, isError, minDate, defaultValue, maxDate } = this.props;
		const { isDatePickerVisible } = this.state;
		const { block, element, modifier, elementModifier } = this;
		// const inputValue = value ? value : '';

		const blockClassName = classNames(
			block(),
			className,
			{ [modifier('error')]: isError }
		);

		return (
			<div id={id} className={blockClassName}>
				{label && <label className={element('label')} >{label}</label>}

				<div className={element('datepicker-container')}></div>
				<div className={element('field')} >

					{value ?
						<div className={element('date-wrap')}>
							<div className={element('date')}>{moment(value).format('D')}</div>
							<div className={element('date-inner')}>
								<div className={element('month')}>{moment(value).format('MMM')}</div>
								<div className={element('year')}>{moment(value).format('YYYY')}</div>
							</div>								
						</div>
						:
						<div className={element('placeholder')}>Туда</div>
					}
					{/* {Utils.browser.isMobile() ?
						<input className={element('input')}
							ref='mobileDateInput'
							id={id + '-input'}
							type='date'
							value={value || ''}
							placeholder={placeholder}
							onChange={this.onMobileInputChange}
							onFocus={this.props.onFocus}
							onBlur={this.onMobileBlur}
							min={minDate}
							max={maxDate}
						/> :
						<Mask id={id + '-input'}
							placeholder={placeholder}
							className={element('input')}
							onChange={this.onChange}
							mask={mask}
							maskChar={maskChar}
							onBlur={this.onBlur}
							onFocus={this.props.onFocus}
							value={this.insureDateIsRusFormat(value)} />
					} */}
					<div className={classNames(element('calendar-btn'), { [elementModifier('calendar-btn', 'active')]: isDatePickerVisible })} onClick={this.onClickCalendar}  >
						<i className="icon-calendar"></i>
					</div>
				</div>

				<div className={element('datepicker-container')} >
					{isDatePickerVisible && <DatePicker onClickOutside={e => this.setState({ isDatePickerVisible: false })} onSelect={e => this.setState({ isDatePickerVisible: false })} onChange={this.onDatePickerChange} name={name} value={value || defaultValue} minDate={minDate} maxDate={maxDate} defaultValue={defaultValue} />}
				</div>
				{isError && <div className={element('error')}>{error}</div>}
			</div>
		)
	}
}


