import { IControlEvent } from '../../lib/IControlEvent';

export interface IDateInputProps {
	/**
	 * html id
	 * @type {string}
	 * @memberof IDateInputProps
	 */
	id?: string;
	/**
	 * Название поля в модели
	 * @type {string}
	 * @memberof IDateInputProps
	 */
	name: string;
	/**
	 * Значение 
	 * @type {*}
	 * @memberof IDateInputProps
	 */
	value: any;
	/**
	* validation error
	*/
	isError ? : boolean;
	/**
	 * ошибка, которую передает в компонент его валидатор
	 */
	error?: string | boolean | JSX.Element;
	/**
	 * Будет вызван при выборе даты
	 * @param {IControlEvent} event 
	 * @memberof IDateInputProps
	 */
	onChange(event: IControlEvent);
	/**
	 * Вызывается на отфокусе из поля
	 * @param {IControlEvent} event 
	 * @memberof IDateInputProps
	 */
	onBlur?(event: IControlEvent);
	/**
	 * Вызывается на фокусе из поля
	 * @param {IControlEvent} event 
	 * @memberof IDateInputProps
	 */
	onFocus?() : void;
	/**
	 * Функция будет вызвана, если календарь открыт пока value компонента === null
	 * @param {IControlEvent} event 
	 * @memberof IDateInputProps
	 */
	onOpenWithNullValue?() : void;
	/**
	 *  Маска, по дефолту под дату с dd.mm.yyyy
	 * @type {string}
	 * @memberof IDateInputProps
	 */
	mask?: string;
	/**
	 * Чар для маски, если вдруг понадобится
	 * @type {string}
	 * @memberof IDateInputProps
	 */
	maskChar?: string;
	/**
	 * Угадай с 3 раз
	 * @type {string}
	 * @memberof IDateInputProps
	 */
	placeholder? : string;
	/**
	 * Клас врапера
	 * @type {string}
	 * @memberof IDateInputProps
	 */
	className? : string;
	/**
	 * Лейбл над полем
	 * @type {(string | JSX.Element)}
	 * @memberof IDateInputProps
	 */
	label? : string | JSX.Element;
	/**
	 * min Date
	 * @type {string}
	 * @memberOf IDateInputProps
	 */
	minDate?: string;
	/**
	 * max Date
	 * @type {string}
	 * @memberOf IDateInputProps
	 */
	maxDate?: string;
	/**
	 * Значение по умолчанию
	 * @type {Date}
	 * @memberOf IDateInputProps
	 */
	defaultValue ?: Date;
}