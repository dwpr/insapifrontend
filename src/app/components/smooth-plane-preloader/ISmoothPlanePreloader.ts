import { IControlEvent } from "../../lib/IControlEvent";

export interface ISmoothPlanePreloaderProps {
    total: number;
    loaded: number;
    isNeedResetLoading?: boolean,
    onResetLoading?() : any;
}


export interface ISmoothPlanePreloaderState {
    isFinishedAnimation: boolean
}