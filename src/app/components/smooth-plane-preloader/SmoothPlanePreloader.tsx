import './SmoothPlanePreloader.style.scss';
import { ISmoothPlanePreloaderProps, ISmoothPlanePreloaderState } from './ISmoothPlanePreloader';
import { image } from '../../app';

export class SmoothPlanePreloader extends React.Component<ISmoothPlanePreloaderProps, ISmoothPlanePreloaderState> {

    public static defaultProps: ISmoothPlanePreloaderProps = {
        total: 0,
        loaded: 0,
        isNeedResetLoading: false
    }


    public state: ISmoothPlanePreloaderState = {
        isFinishedAnimation: false
    }

    componentWillMount() {

    }

    componentDidMount() {
        this.setAnimation();
    }

    componentDidUpdate() {
        if (this.props.loaded == this.props.total && !this.state.isFinishedAnimation) {
            this.setState({ isFinishedAnimation: true })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.loaded == this.props.total && this.props.loaded != nextProps.loaded) {
            // отработка анимации заново
            this.setState({ isFinishedAnimation: false });
            this.setAnimation();
        }
        // сброс анимации при сбросе запросов
        if (nextProps.isNeedResetLoading && !this.props.isNeedResetLoading) {
            this.setAnimation();
            this.props.onResetLoading();
        }
    }

    componentWillUnmount() {
        if (this.timer) {
            clearInterval(this.timer);
        }
    }

    timer = null;

    setAnimation = () => {
        if (document.getElementById('preloader')) {
            let start = Date.now(); // сохранить время начала
            if (this.timer) {
                clearInterval(this.timer);
            }
            this.timer = setInterval(() => {
                // вычислить сколько времени прошло с начала анимации
                let timePassed = Date.now() - start;
                // время анимации 40с
                if (timePassed >= 40000) {
                    clearInterval(this.timer);
                    return;
                }

                // рисует состояние анимации, соответствующее времени timePassed
                if (!this.state.isFinishedAnimation) {
                    this.animate(timePassed);
                } else {
                    // если все ответы от страховых пришли ранее, принудительно заканчиваем анимацию
                    clearInterval(this.timer);
                    document.getElementById('preloader').style.width = '104%';
                }

            }, 20);
        }
    }

    animate = (timePassed) => {
        /** максимальное время анимации = 400 с
         * 1766 - время, которое при делении на 384 (почему 384 объяснено ниже) дает минимальную ширину прелоудера 4,5%
        */
        if (timePassed < 1766) {
            // ибо при ширине в 0%, самолетик ужимается в точку, до 4,5%(минимальная ширина прелоудера с самолетиком) имитируем полет изменение координаты left
            document.getElementById('preloader').style.width = '4.5%';
            document.getElementById('preloader').style.left = '-' + (4 - timePassed / 400) + '%';
        } else {
            // максимальное время анимации = 400 с, ставим 384, чтобы width было чуть > 104%, дабы самолет улетел за пределы экрана, и координата left = 4% не оставляла его хвост на экране
            document.getElementById('preloader').style.width = timePassed / 384 + '%';
        }
    }

    public render() {

        return (
            <div className='smooth-plane-preloader'>
                <div id='preloader' className={`smooth-plane-preloader__yellow-block ${this.props.loaded == this.props.total ? 'smooth-plane-preloader__yellow-block--done' : ''}`}>
                    <img className={`smooth-plane-preloader__plane-pic ${this.props.loaded == this.props.total ? 'smooth-plane-preloader__plane-pic--hidden' : ''}`} src={image('plane.svg')} alt="" />
                </div>
            </div>
        )
    }
}