import './Radio.style.scss';
import { IRadioProps } from './IRadioProps';
import { Component, IBemComponentProps } from '../../lib/react-bem-component';
import * as classNames from 'classnames';
import { FormElement } from '../form/FormElement';

export interface IRadioState {
    
}

@FormElement
export class Radio extends Component<IRadioProps, IRadioState> {

    /**
     * 
     * @static
     * @memberOf Radio
     */
    static defaultProps = {
        namespace: 'radio',
        className: '',
    }
    /**
     * @type {IRadioState}
     * @memberOf Radio
     */
    public state : IRadioState = {

    }
    /**
     * 
     * 
     * @memberof Radio
     */
    public onChange = (event) => {
        const {  name, value } = this.props;
        const onChange = this.props.onChange || this.context.onChange || (() => {});
		onChange({
			props: this.props,
			value,
			name
		})
    }

    render() {
        const { element, block, modifier, onChange, elementModifier } = this;
		const {id, label, name, className, value, error, tabIndex, checked} = this.props;

		const blockClassName = classNames(
            block(),
            {[modifier('checked')] : checked},
			className
        );
        
        return (
            <div className={blockClassName}>
                <input id={id} name={name} value={value} checked={checked} type="radio" onChange={onChange}/>
                <label htmlFor={id} className={element('item')}>
                    <div className={element('flag')}></div>
                    {label && <div className={element('label')}>{label}</div>}
                </label>
            </div>
        )
    }

}
