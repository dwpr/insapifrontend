import { IControlEvent } from '../../lib/IControlEvent';

export interface IRadioProps {
    /**
     * Атрибут id
     * 
     * @type {string}
     * @memberOf IRadioProps
     */
    id : string;
    /**
     * 
     * Атрибут name = название поля в модели
     * @type {string}
     * @memberOf IRadioProps
     */
    name : string;
    /**
     * value
     * 
     * @type {*}
     * @memberOf IRadioProps
     */
    value : any;
    /**
     * Атрибут checked
     * 
     * @type {boolean}
     * @memberOf IRadioProps
     */
    checked : boolean;
    /**
     * Класс рутового элемента 
     * 
     * @type {string}
     * @memberOf IRadioProps
     */
    className? : string;
    /**
     * label
     * 
     * @type {(string | JSX.Element)}
     * @memberOf IRadioProps
     */
    label? : string | JSX.Element;
    /**
     * Строка или блок с ошибкой
     * 
     * @type {(string | JSX.Element)}
     * @memberOf IRadioProps
     */
    error? : string | JSX.Element;
    /**
     * tab index
     * 
     * @type {number}
     * @memberOf IRadioProps
     */
    tabIndex? : number;
    /**
     * Функция на change
     * @param {IControlEvent} event 
     * @returns {*} 
     * 
     * @memberOf ICheckboxProps
     */
    onChange?(event : IControlEvent) : any;
}