import { IControlEvent } from '../../lib/IControlEvent';


export interface IChangeEmailProps {

    /**
     * Поле, содержащее email
     * 
     * @type {string}
     * @memberOf IChangeEmailProps
     */
    name? : string;
    /**
     * Атрибут id
     * 
     * @type {string}
     * @memberOf IChangeEmailProps
     */
    id? : string;
    /**
     * value
     * 
     * @type {*}
     * @memberOf IChangeEmailProps
     */
    value: any;
    /**
     * Обработка события onChange
     * 
     * @param {IControlEvent} event 
     * @returns {*} 
     * 
     * @memberOf IChangeEmailProps
     */
    onChange?(event : IControlEvent) : any;
    /**
     * Функция на сохранение email
     * @returns {*} 
     * @memberOf IChangeEmailProps
     */
    onChangeEmailClick() : any;
    /**
     * Индикатор отправки полиса
     * 
     * @type {boolean}
     * @memberOf IChangeEmailProps
     */
    isPolicySending? : boolean;
}