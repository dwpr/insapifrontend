import { IChangeEmailState } from './IChangeEmailState';
import { IChangeEmailProps } from './IChangeEmailProps';
import { Component } from '../../lib/react-bem-component/index';
import { Field } from '../field/Field';
import { Validator } from '../validator/Validator';
import { Button } from '../button/Button';
import { image } from '../../app';





export class ChangeEmail extends Component<IChangeEmailProps, IChangeEmailState> {

    state : IChangeEmailState = {
        isInputVisible: false,
        previousEmail: ''
    }
    /**
     * toggle формы для изменения email
     * @memberOf ChangeEmail
     */
    toggleInput = () => {
        const { previousEmail, isInputVisible } = this.state;
        const { value, onChange, name } = this.props;
        /** При открытии сохраняем предыдущее значение email */
        if (!isInputVisible) this.setState({ previousEmail: value });
        /** При закрытии возвращаем к предыдущему значению */
        else onChange({ name, value: previousEmail, props: this.props });
        this.setState({ isInputVisible: !this.state.isInputVisible });
    }
    /**
     * Отправка полиса на измененный email
     * @memberof ChangeEmail
     */
    onChangeEmailClick = () => {
        const { onChangeEmailClick, value } = this.props;
        const { previousEmail } = this.state;
        if (previousEmail == value) return;
        onChangeEmailClick();
        this.setState({ isInputVisible: false });
    }

    render() {

        const { id, name, value, onChange, isPolicySending } = this.props;
        const { isInputVisible } = this.state;
        const { toggleInput, onChangeEmailClick } = this;

        return(
            <div>
                <div className='response__label'>Файл полиса будет отправлен на адрес</div>
                {/* Если форма скрыта и не идет отправка полиса */}
                {!isInputVisible && !isPolicySending ?
                    <div className='response__email-block'>
                        <div><b className='response__email'>{value}</b></div>
                        <span className='link link--blue' onClick={toggleInput}>Изменить адрес</span>
                    </div>
                    :
                    <div className='response__email-block'>
                        <div className='response__change-email'>
                            <Validator>
                                <Field id={this.props.id} name={this.props.name} value={value} onChange={onChange}/>
                            </Validator>
                            <Button className='response__change-email-button' onClick={onChangeEmailClick}>Сохранить</Button>
                        </div>
                        <div className='response__change-email-column'>
                            <div className='link link--blue response__change-email-cancel' onClick={toggleInput}>Отмена</div>
                            {isPolicySending && 
                                <div className='response__loader'>
                                    <img src={image('loading.gif')} alt="" width="20px" height="20px"/>
                                </div>
                            }
                        </div>
                    </div>
                }
            </div>
        )
    }
}