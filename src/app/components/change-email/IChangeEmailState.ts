


export interface IChangeEmailState {
    /**
     * Видимость инпута
     * 
     * @type {boolean}
     * @memberOf IChangeEmailState
     */
    isInputVisible: boolean;
    /**
     * Предыдущее значение email
     * @type {string}
     * @memberOf IChangeEmailState
     */
    previousEmail : string;
}