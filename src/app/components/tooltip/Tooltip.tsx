import './Tooltip.style.scss';
import { ITooltipProps } from './ITooltipProps';
import { Component, IBemComponentProps } from '../../lib/react-bem-component';
import * as classNames from 'classnames';

export interface ITooltipState {
    /**
     * Тултип показан
     * 
     * @type {boolean}
     * @memberOf ITooltipState
     */
    isVisible: boolean;
    /**
     * Скрывать Тултип
     * 
     * @type {boolean}
     * @memberOf ITooltipState
     */
    tooltipShouldHide: boolean;
    /**
     * Расположение тултипа
     * 
     * @type {string}
     * @memberOf ITooltipState
     */
    tooltipType: string;
}


export class Tooltip extends Component<ITooltipProps, ITooltipState> {

    /**
     * 
     * @static
     * @memberOf Tooltip
     */
    static defaultProps = {
        namespace: 'tooltip',
        className: '',
        tooltipIcon: <i className='icon-tooltip'/>,
        width: '280',
        tooltipPosition: 'top-left'
    }
    /**
     * @type {ITooltipState}
     * @memberOf Tooltip
     */
    state: ITooltipState = {
        isVisible: false,
        tooltipShouldHide: true,
        tooltipType: this.props.tooltipType || 'right'
    }

    componentDidMount() {
        window.addEventListener('mouseup', this.hideTooltip);
        window.addEventListener('click', this.hideTooltip);
    }

    componentWillUnmount() {
        window.removeEventListener('mouseup', this.hideTooltip);
        window.removeEventListener('click', this.hideTooltip);
    }
    /**
     * показываем тултип и определяем положение 
     * @memberOf Tooltip
     */
    showTooltip = (e) => {
        e.stopPropagation();
        e.preventDefault();
        if (!this.state.isVisible) this.getTooltipPosition();
        // this.setState({ isVisible: true, tooltipShouldHide: false });
        this.setState({ isVisible: true });
        if (this.props.onClick) this.props.onClick();
    }

    hideTooltip = (e) => { 
        // if (this.state.isVisible && this.state.tooltipShouldHide) {
        if (this.state.isVisible) {
            e.preventDefault();
            e.stopPropagation();
            this.setState({ isVisible: false });
        }
        // this.setState({ tooltipShouldHide: true });
    }
    /**
     * Определем положение тултипа 
     * @memberOf Tooltip
     */
    getTooltipPosition = () => {
        /** Размер окна */
        let windowWidth = window.document.body.clientWidth;
        /** Определяем координаты правой и левой точки и ширину */
        let rightPointTooltip = (this.refs.tooltipBody as any).getBoundingClientRect().right;
        let leftPointTooltip = (this.refs.tooltipBody as any).getBoundingClientRect().left;
        let widthTooltip = (this.refs.tooltipBody as any).getBoundingClientRect().width;
       /** Если тултип располгается справа */
       if (this.state.tooltipType == 'right'){
           /** Если расстояние между правым краем окна и правым краем тултипа меньше 10px, меняем тип тултипа на левый */
           if (windowWidth - rightPointTooltip < 10 && leftPointTooltip > widthTooltip) this.setState({ tooltipType: 'left' });
       }
    }

    get style() {
		const style: any = {};
        const {width} = this.props;
        if (width) style.width = `${width}px`;
        return style;
    }

    render() {
        const { element, block, modifier, elementModifier, showTooltip, hideTooltip, style } = this;
        const { id, className, tooltip, tooltipPosition, tooltipIcon, width } = this.props;
        const { isVisible, tooltipShouldHide, tooltipType } = this.state;
 
        const blockClassName = classNames(
            block(),
            className,
        );

        const tooltipBodyClassName = classNames(
            element('body'),
            { [elementModifier('body', 'hidden')]: !isVisible },
            elementModifier('body', tooltipPosition || tooltipType)
        );

        const iconClassName = classNames(
            element('icon'),
            { [elementModifier('icon', 'active')]: isVisible }
        )

        return (
            <div className={blockClassName}>
                <div className={iconClassName} onMouseOver={showTooltip} onMouseOut={hideTooltip}>{tooltipIcon}</div>
                <div ref='tooltipBody' className={tooltipBodyClassName} style={style} >
                    {/* <div className={element('close')} onClick={hideTooltip}></div> */}
                    <div className={element('content')} onClick={e => {e.preventDefault(); e.stopPropagation()}} onMouseUp={e => {e.preventDefault(); e.stopPropagation()}}>{tooltip}</div>
                </div>
            </div>
        )
    }

}
