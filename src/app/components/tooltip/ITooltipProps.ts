import { IControlEvent } from '../../lib/IControlEvent';

export interface ITooltipProps {
    /**
     * Класс рутового элемента
     * 
     * @type {string}
     * @memberOf ITooltipProps
     */
    className? : string;
    /**
     * Атрибут id
     * 
     * @type {string}
     * @memberOf ITooltipProps
     */
    id? : string;
    /**
     * Текст тултипа
     * 
     * @type {(string | JSX.Element)}
     * @memberOf ITooltipProps
     */
    tooltip: string | JSX.Element;
    /**
     * Положение тултипа
     *  top, left, bottom, right
     * @type {string}
     * @memberOf ITooltipProps
     */
    tooltipPosition? : string;
    
    /**
     * Иконка тултипа
     * 
     * @type {(string | JSX.Element)}
     * @memberOf ITooltipProps
     */
    tooltipIcon? : string | JSX.Element;
    /**
     * Тип тултип
     */
    tooltipType? : string;
    /**
     * Ширина тултипа
     * 
     * @type {string}
     * @memberOf ITooltipProps
     */
    width? : string;
    /**
     * Клин на тултип
     * @memberOf ITooltipProps
     */
    onClick?(): void;
}