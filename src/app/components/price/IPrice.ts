


export interface IPriceProps {
	/**
	 * className блока
	 * @type {string}
	 * @memberof IPriceProps
	 */
	className?: string;
	/**
	 * @type {(string | number)}
	 * @memberof IPriceProps
	 */
	children : string | number;
	/**
	 * Класс знака валюты
	 * @type {string}
	 * @memberof IPriceProps
	 */
	decimalClassName? : string;
	/**
	 * Валюта
	 * @type {currencyTypes}
	 * @memberof IPriceProps
	 */
	currency? : string | JSX.Element;
	/**
	 * Нужны знаки после запятой
	 * @type {boolean}
	 * @memberOf IPriceProps
	 */
	noDecimal ?: boolean;
	/**
	 * Нужна ли анимация
	 * @type {boolean}
	 * @memberOf IPriceProps
	 */
	isNeedAnimation ?: boolean;
}

export interface IPriceState {
	isAnimated: boolean,
	isPriceWasAnimated: boolean
}