import { Component } from '../../lib/react-bem-component';
import * as classNames from 'classnames';
import { IPriceProps, IPriceState } from './IPrice';


export class Price extends Component<IPriceProps, IPriceState> {

	static defaultProps = {
		namespace: 'price-formater',
		isNeedAnimation: false
	}

	state = {
		isAnimated: false,
		isPriceWasAnimated: false
	}

	componentDidUpdate() {
		if (!this.state.isAnimated && this.props.isNeedAnimation) {
			this.setState({ isAnimated: true, isPriceWasAnimated: false });
		}
		if (this.state.isAnimated && !this.props.isNeedAnimation) {
			this.setState({ isAnimated: false, isPriceWasAnimated: true });
		}
	}

	digitArrayForAnimation = (start) => {
		let digitArray = [];
		let currentDigit = start;
		for (let i = 0; i < 10; i++) {
			digitArray.push(currentDigit);
			if (currentDigit == 9) {
				currentDigit = 0;
			} else {
				currentDigit++;
			}
		}
		return digitArray;
	}


	render() {
		const { children, decimalClassName, className, currency, noDecimal, isNeedAnimation } = this.props;
		const [whole, decimal] = Number(children).toFixed(2).toString().split('.');
		const { block, element, elementModifier } = this;
		const { isAnimated, isPriceWasAnimated } = this.state
		return (
			<div className={classNames(block(), className)}>
				<span className={classNames(element('wrap'))}>
					<span className={classNames(element('whole'), { [elementModifier('whole', 'with-animation')]: isAnimated })} >
						{Utils.text.numberWithSpaces(whole).split('').map((item, index) => {
							if (item.match(/[0-9]/)) {
								return (
									<span key={`whole-number${index}`} className={classNames({ [element('with-animation')]: isAnimated }, { [element('was-animated')]: isPriceWasAnimated })}>
										{isAnimated ?
											<span className='animation-trip'>{this.digitArrayForAnimation(item).map((item, index) => {
												return <span key={`digit${index}`}>{item}</span>
											})}</span>
											:
											<span>{item}</span>
										}
									</span>
								)
							}
							return <span key={`whole-number${index}`}>{item}</span>
						})}
					</span>
					{!noDecimal && <span className={classNames(element('decimal'), decimalClassName, { [elementModifier('decimal', 'with-animation')]: isAnimated })}>,
						{decimal.split('').map((item, index) => {
							return (
								<span key={`decimal-number${index}`} className={classNames({ [element('with-animation')]: isAnimated }, { [element('was-animated')]: isPriceWasAnimated })}>
									{isAnimated ?
										<span className='animation-trip'>{this.digitArrayForAnimation(item).map((item, index) => {
											return <span key={`digit${index}`}>{item}</span>
										})}</span>
										:
										<span>{item}</span>
									}
								</span>)
						})}
					</span>}
					<span className={classNames(element('currency'))} >{currency}</span>
				</span>
			</div>
		)
	}
}



export const CurrencyIcon = {
	RUB: <span className='rouble'>{Utils.browser.detectIE() ? 'i' : 'p'}</span>,
	EUR: ' €',
	USD: ' $'
};