import { Component } from "../../lib/react-bem-component";
import { ICustomDatepickerProps } from "./ICustomDatePicker";
import DatePicker, { ReactDatePickerProps } from 'react-datepicker';
import * as moment from 'moment';
import * as classNames from 'classnames';

//import 'react-datepicker/dist/react-datepicker.css';
import './CustomRangeDatepicker.style.scss';





export class CustomRangeDatepicker extends Component<ICustomDatepickerProps> {

    static defaultProps = {
        namespace: 'range-calendar'
    }

    state = {
        selectedPicker: '',
    }


    onStartDateChange = (event) => {
        this.setState({ selectedPicker: 'to' });
        this.props.onChange({
            name: this.props.from.name,
            value: event.format('YYYY-MM-DD'),
            props: this.props
        })
    }

    onEndDateChange = (event) => {
        this.props.onChange({
            name: this.props.to.name,
            value: event.format('YYYY-MM-DD'),
            props: this.props
        });
        this.setState({ selectedPicker: '' })
    }

    onClickOutside = (target) => {
        const { selectedPicker } = this.state;
        if (selectedPicker == target) this.setState({ selectedPicker: '' })
    }

    ValueRenderer = (props) => {
        const { onStartDateChange,
            onEndDateChange,
            element,
            block,
            modifier } = this;
        const { placeholder} = this.props;
        const { isError } = props;
        const { selectedPicker } = this.state;
        return (
            <div className={element('values-wrapper')}>
                <div className={props.className} onClick={() => this.setState({ selectedPicker: props.selectedPicker })}>
                    {props.value ?
                        <div className={element('date-wrap')}>
                            <div className={element('date')}>{moment(props.value).format('D')}</div>
                            <div className={element('date-inner')}>
                                <div className={element('month')}>{moment(props.value).format('MMM')}</div>
                                <div className={element('year')}>{moment(props.value).format('YYYY')}</div>
                            </div>
                        </div>
                        :
                        <div className={classNames(element('placeholder'), { [element('placeholder--error')]: isError })}>{props.placeholder}</div>
                    }
                    <div className={classNames(element('calendar-btn'), { [element('calendar-btn--active')]: selectedPicker == props.selectedPicker }, { [element('calendar-btn--error')]: isError })} >
                        <i className="icon-calendar"></i>
                    </div>
                </div>
            </div>
        )
    }

    get startDate() {
        return this.props.from.value ? moment(this.props.from.value) : undefined
    }

    get endDate() {
        return this.props.to.value ? moment(this.props.to.value) : undefined
    }

    get amountOfDays() {
        const { to, from } = this.props;
        return moment(to.value).diff(moment(from.value), 'days') + 1;
    }


    render() {

        const { onStartDateChange,
            onEndDateChange,
            element,
            block,
            modifier,
            ValueRenderer,
            elementModifier,
            onClickOutside,
            startDate,
            endDate,
            amountOfDays } = this;

        const { selectedPicker } = this.state;

        const { className, 
            placeholder,
            minDate,
            maxDate,
            label,
            isError,
            onBlur,
            onFocus,
            to,
            from } = this.props;

        const blockClassName = classNames(
            block(),
            className,
            { [modifier('error')]: isError }
        );

        return (
            <div className={blockClassName}>
                {label && <label className={element('label')} >{label}</label>}
                <div className={element('values-main-wrapper')}>
                    <ValueRenderer className={element('value-from')} isError={isError} value={startDate} selectedPicker='from' placeholder='Туда'/>
                    <div className={element('separator')}></div>
                    <ValueRenderer className={element('value-to')} isError={isError} value={endDate} selectedPicker='to' placeholder='Обратно'/>
                    {to.value && amountOfDays > 0 && <div className={element('total')}>{amountOfDays} {Utils.text.getWordForm(amountOfDays, ['день', 'дня', 'дней'])}</div>}
                </div>
                <div className={element('calendar')}>
                    {selectedPicker && 
                        <div className={classNames(element('tabs'), { [elementModifier('tabs', 'to')]: selectedPicker == 'to' })}>
                            <div className={classNames(element('tab'), { [elementModifier('tab', 'active')]: selectedPicker == 'from' })} onClick={() => this.setState({ selectedPicker: 'from' })}>Туда</div>
                            <div className={classNames(element('tab'), { [elementModifier('tab', 'active')]: selectedPicker == 'to' })} onClick={() => this.setState({ selectedPicker: 'to' })}>Обратно</div>
                        </div>
                    }
                    <div className={classNames(element('calendar-wrapper'), { [elementModifier('calendar-wrapper', 'hidden')]: selectedPicker == 'to' || !selectedPicker })}>
                        <DatePicker
                            ref='from'
                            inline
                            selected={startDate}
                            selectsStart
                            startDate={startDate}
                            endDate={endDate}
                            onChange={this.onStartDateChange}
                            minDate={moment(minDate)}
                            maxDate={moment(maxDate)}
                            onClickOutside={() => onClickOutside('from')}
                            onFocus={onFocus}
                            onBlur={onBlur}
                        />
                    </div>
                    <div className={classNames(element('calendar-wrapper'), { [elementModifier('calendar-wrapper', 'hidden')]: selectedPicker == 'from' || !selectedPicker })}>
                        <DatePicker
                            ref='to'
                            inline
                            selected={startDate}
                            selectsEnd
                            startDate={startDate}
                            endDate={endDate}
                            onChange={this.onEndDateChange}
                            minDate={startDate || moment(minDate)}
                            maxDate={moment(maxDate)}
                            onClickOutside={() => onClickOutside('to')}
                            onFocus={onFocus}
                            onBlur={onBlur}
                            allowSameDay={true}
                        // className={this.state.selectedPicker == 'to' ? '' : 'hidden'}
                        />
                    </div>
                </div>
                
            </div>
        )
    }
}