import { IControlEvent } from "../../lib/IControlEvent";
import { Moment } from "moment";




export interface ICustomDatepickerProps {
    onChange(event: IControlEvent) : any;
    className ? : string;
    placeholder ?: string | JSX.Element;
    minDate ?: string;
    maxDate ?: string;
    label ?: string | JSX.Element;
    from: IDatePickerValue;
    to: IDatePickerValue;
    isError ?: boolean;
    error ?: string;
    startDate ?: string;
    endDate ?: string;
    onFocus?(event?: any) : any;
    onBlur?(event?: any) : any;
}

export interface IDatePickerValue {
    name: string;
    value: string
}