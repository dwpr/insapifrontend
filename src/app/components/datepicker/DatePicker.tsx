import { IControlEvent } from '../../lib/IControlEvent';
import { Component, IBemComponentProps } from '../../lib/react-bem-component';
// https://github.com/YouCanBookMe/react-datetime
import Calendar from './react-datetime/DateTime';
import * as moment from 'moment';
import * as classNames from 'classnames';
import { IDatePickerProps } from './IDatePickerProps';

export class DatePicker extends Component<IDatePickerProps> {
	/**
	 * Список название месяцев для календаря
	 * @type {string[]}
	 * @memberof DatePicker
	 */
	listMonth: string[] = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];


	componentDidMount() {
		window.addEventListener('mouseup', this.onClickOutsideContainer);
	}
	componentWillUnmount() {
		window.removeEventListener('mouseup', this.onClickOutsideContainer)
	}


	/**
	 * При клике за контейнер прячем 
	 */
	public onClickOutsideContainer = (event) => {
		const { onClickOutside } = this.props;
		const container : HTMLElement = this.refs.container as HTMLElement;
		if (!container.contains(event.target)) onClickOutside(event); 
	}

	

	public static defaultProps = {
		namespace: 'datepicker',
		onChange: (event: IControlEvent) => {},
		onClickOutside: () => {},
		onSelect: () => {}
	}

	public onChange = (payload: moment.Moment) => {
		const { onChange, name, onSelect } = this.props;
		onChange({
			value: payload.format('YYYY-MM-DD'),
			name,
			props: this.props
		});
		onSelect(event);
	}

	/**
	 * Чтобы в value было хоть что то
	 */
	public get value() {
		const { value } = this.props;
		return new Date(value || new Date());
	}

	/**
	 * Проверяет стрингу на соотв iso (без времени)
	 */
	public checkIsIsoString = (stringToTest: string) => new RegExp(/^\d{4}-\d{2}-\d{2}$/, 'gi').test(stringToTest);

	/**
	 * Определяет дизейблить день или нет
	 * @memberof DatePicker
	 */
	validateDate = (current) => {
		const { minDate, maxDate } = this.props;

		if (minDate && this.checkIsIsoString(minDate)) {
			const minMomenet = moment(minDate);
			minMomenet.subtract(1, 'd');
			if (maxDate && this.checkIsIsoString(maxDate)) {
				const maxMoment = moment(maxDate);
				maxMoment.add(1, 'd');
				return current.isBefore(maxMoment) && current.isAfter(minMomenet);
			}
			return current.isAfter(minMomenet);
		}
		return true;
	}

	renderDay = (props, currentDate) => {
		let className = props.className.replace('rdtActive', '');
		let dayWrapperClassName = 'rdtDayWrapper'
        const { value, rangeParams } = this.props;
        if (value == currentDate.format('YYYY-MM-DD') || (rangeParams && rangeParams.from && rangeParams.to && (currentDate.format('YYYY-MM-DD')  == rangeParams.from || currentDate.format('YYYY-MM-DD') == rangeParams.to))) {
            dayWrapperClassName += ' rdtActive';
        }
		if (rangeParams && rangeParams.to && currentDate.format('YYYY-MM-DD') > moment(rangeParams.from).format('YYYY-MM-DD') && currentDate.format('YYYY-MM-DD') < moment(rangeParams.to).format('YYYY-MM-DD')) {
			dayWrapperClassName += ' rdtRange';		
		}
		return (
			<td key={props.key} className={dayWrapperClassName} >
				<div  {...props} className={className}>{props['data-value']}</div>
			</td>
		);
	}
	renderMonth = (props, currentDate) => {
		const number = props['data-value'];
		return (
			<td key={number} >
				<div {...props} >{this.listMonth[number]}</div>
			</td>
		)
	}

	renderYear = (props, currenDate) => {
		const year = props['data-value'];
		return (
			<td key={year} >
				<div {...props} >{year}</div>
			</td>
		)
	}

	render() {

		const { onChange, block, validateDate, renderYear, value, renderDay, renderMonth } = this;
		const { className, defaultValue } = this.props;
		return (
			<div ref='container' className={classNames(block(), className)} >
				<Calendar
					renderDay={renderDay}
					renderMonth={renderMonth}
					renderYear={renderYear}
					value={value}
					input={false}
					locale='ru'
					onChange={onChange}
					isValidDate={validateDate}
					defaultValue={defaultValue}
				/>
			</div>
		)
	}
}


