import { IControlEvent } from '../../lib/IControlEvent';

export interface IDatePickerProps {
	/**
	 * Обработчик выбора даты 
	 * @param {IControlEvent} event 
	 * @returns {*} 
	 * @memberof IDatePickerProps
	 */
	onChange?(event: IControlEvent): any;

	/**
	 * Название поля в модели
	 * @type {string}
	 * @memberof IDatePickerProps
	 */
	name?: string;
	/**
	 * Текущая выбранная дата в ISO
	 * @type {*}
	 * @memberof IDatePickerProps
	 */
	value?: any;

	/**
	 * Минимальная дата для выбора
	 * @type {string}
	 * @memberof IDatePickerProps
	 */
	minDate?: string;

	/**
	 * Максимальная дата для выбора
	 * @type {string}
	 * @memberof IDatePickerProps
	 */
	maxDate?: string;
	/**
	 * Класс wrapper'a
	 * @type {string}
	 * @memberof IDatePickerProps
	 */
	className?: string;
	/**
	 * Срабатывает при выборе даты
	 * @returns {*} 
	 * @memberof IDatePickerProps
	 */
	onSelect?(event) : any;

	/**
	 * Срабатывает при клике за dp
	 * @returns {*} 
	 * @memberof IDatePickerProps
	 */
	onClickOutside?(event) : any;
	/**
	 * 
	 * @type {Date}
	 * @memberOf IDatePickerProps
	 */
	defaultValue? : Date;
	
	rangeParams ? : any;
}