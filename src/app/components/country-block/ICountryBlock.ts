export interface ICountryBlockProps {
    className?: string,
    listCountry: { id: number, name: string }[],
    countryId: number,
    isShowFlag?: boolean
}


export interface ICountryBlockState {
    
}