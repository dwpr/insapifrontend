import './CountryBlock.style.scss';
import { ICountryBlockProps, ICountryBlockState } from './ICountryBlock';

export class CountryBlock extends React.PureComponent<ICountryBlockProps, ICountryBlockState> {

    public static defaultProps : ICountryBlockProps = {
        className: '',
        listCountry: null,
        countryId: null,
        isShowFlag: true
    }


    public state: ICountryBlockState = {
        
    }

    get countryName() {
        let { countryId, listCountry } = this.props;

        let countryModel = listCountry.find(countryModel => countryModel.id == countryId);

        if(countryModel) return countryModel.name;
    }

    public render() {
        let { className, isShowFlag, countryId } = this.props;
        let { countryName } = this;

        return (
            <div className={'country-block' + (className ? className : '' )}>
                { isShowFlag && <i className={'country-block__flag icon-' + countryId}></i> }
                <div className='country-block__name'>{ countryName }</div>
            </div>
        )
    }
}