import { ITravelRiskModel } from './ITravelListModel';

export interface IInsurerModel {
    phone: string,
    email: string,
    birthDate: string,
    lastName: string,
    firstName: string,
    documentSeries: string,
    documentNo: string
}

export const getDefaultInsurerModel = () : IInsurerModel => ({
    phone: null,
    email: null,
    birthDate: null,
    lastName: null,
    firstName: null,
    documentSeries: null,
    documentNo: null
});

export interface ICountryModel {
    id: number,
    name: string
}

export interface IInsuredModel {
    age: number,
    birthDate: string,
    lastName: string,
    firstName: string,
    documentSeries: string,
    documentNo: string,
    policyNo: string
}

export interface IRiskModel {
    id: number,
    coverageSum: number,
    name?: string,
    franchise?: { value: string, countryId: number }[];
}

export interface ITravelModel {
    quotationGuid: any,
    insuranceCompanyId: number,
    startDate: any,
    endDate: any,
    isMultiple: boolean,
    currencyId: number,
    coveredDay: number,
    insurer: IInsurerModel,
    country: ICountryModel[],
    insured: IInsuredModel[],
    risk: IRiskModel[]
}

export interface ICalculationModel {
    quotationGuid: string,
    insuranceCompanyId: number,
    startDate: string,
    endDate: string,
    isMultiple: boolean,
    currencyId: number,
    coveredDay: number,
    insurer: any,
    country: ICountryModel[],
    insured: IInsuredModel[],
    risk: IRiskModel[]
}

/**
 * @desc модель, используемая на втором шаге (результаты) для правильного 
 * отображения списка результатов в пройцессе загрузки
 */
export interface IResultCalculateModel {
    calculationModel: ICalculationModel;
    price: number,
    specialCondition: { typeId: number, countryId: number, value: string }[]
    /**
     * @desc результат загружается
     */
    isLoading: boolean;
    /**
     * @desc есть ли успешный результат
     */
    isSuccess: boolean;
    /**
     * @desc название страховой для сортировки в процессе загрузки
     */
    name?: string;
    /**
     * @desc id страховой
     */
    insuranceCompanyId?: number;
    /**
     * @desc
     */
    listTravelRisk?: ITravelRiskModel[];
    /**
     * @desc
     */
    assistance: { name: string, phone: string };
    /**
     * @desc
     */
    docLink: { samplePolicyLink: string, insuranceRulesLink: string };
}

export interface IOrderModel {
    calculationModel: ICalculationModel;
    price: number,
    specialCondition: Array<any>;
    listTravelRisk: ITravelRiskModel[];
    docLink: {
        insuranceRulesLink: string,
        samplePolicyLink: string
    }
}
