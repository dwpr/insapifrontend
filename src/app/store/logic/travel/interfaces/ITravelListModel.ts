export interface ICountryModel {
    isHighlighted: boolean,
    id: number,
    name: string,
    isShengen: boolean;
}

export interface IInsuranceCompanyModel {
    id: number,
    name: string,
    isCyrillicOnly: boolean
}

export interface ICurrencyModel {
    id: number,
    name: string
}

export interface ITravelRiskModel {
    id: number,
    name: string,
    listCoverageSum: Array<number>,
    description: string,
    typeId: number
}

export interface ITravelListModel {
    listCountry: ICountryModel[],
    listInsuranceCompany: IInsuranceCompanyModel[],
    listCurrency: ICurrencyModel[],
    listTravelRisk: ITravelRiskModel[]
}