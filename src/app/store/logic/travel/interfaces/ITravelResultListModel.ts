import { ITravelModel } from "./ITravelModel";


export interface ITravelResultListModel {
	constraintText: string;
	insuranceCompanyId: number;
	insuranceSumId: number;
	insurancePrice: number;
	insuranceCurrency: string;
	franchiseText: string;
	franchiseValue: Array<any>;
	specialConditionsText: string;
	orderModel: ITravelModel;
	listTouristBirthDates: Array<any>;
	isLoaded: boolean;
	logoImgLink: string;
}