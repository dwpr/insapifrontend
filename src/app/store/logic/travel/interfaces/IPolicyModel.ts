import { IInsurerModel, ICountryModel, IInsuredModel, IRiskModel } from "./ITravelModel";

export interface IPolicyInsuranceCompanyModel {
    id: number,
    name: string,
    phone: string,
    email: string,
    assistanceName: string,
    assistancePhone: string,
    insuranceRulesLink: string,
    isMailSend: boolean
}

export interface IPolicyModel {
    price: number,
    insuranceCompany: IPolicyInsuranceCompanyModel,
    startDate: string,
    endDate: string,
    isMultiple: boolean,
    currencyId: number,
    coveredDay: number,
    insurer: IInsurerModel,
    country: ICountryModel[],
    insured: IInsuredModel[],
    risk: IRiskModel[]
}