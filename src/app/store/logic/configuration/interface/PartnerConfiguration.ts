import { IPageConfiguration } from "./PageConfiguration";
import { IPartnerCommonConfiguration } from "./PartnerCommonConfiguration";






export interface IPartnerConfiguration {
	/**
	 * Partner id
	 * @type {(number | string)}
	 * @memberof IPartnerConfiguration
	 */
	id : number | string;
	/**
	 * Common settings for partner configuration (colors, ?fonts:nope?)
	 * @type {IPartnerCommonConfiguration}
	 * @memberof IPartnerConfiguration
	 */
	common: IPartnerCommonConfiguration;
	/**
	 * List of page model with configuration for different sections
	 * @type {IPageConfiguration[]}
	 * @memberof IPartnerConfiguration
	 */
	listPage : IPageConfiguration[];
}
