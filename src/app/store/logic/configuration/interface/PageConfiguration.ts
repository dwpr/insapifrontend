import { ITravelSectionConfiguration } from './TravelSectionConfiguration';
import { IRealtySectionConfiguration } from './RealtySectionConfiguration';





export interface IPageConfiguration {
	/**
	 * Url of page where settings will be emited
	 * @type {string}
	 * @memberof IPageConfiguration
	 */
	url: string;
	/**
	 * Configuration for travel section
	 * @type {ITravelSectionConfiguration}
	 * @memberof IPageConfiguration
	 */
	travel? : ITravelSectionConfiguration;

	/**
	 * Configuration for realty section
	 * @type {IRealtySectionConfiguration}
	 * @memberof IPageConfiguration
	 */
	realty? : IRealtySectionConfiguration;

	name: string;
	params: {};
}