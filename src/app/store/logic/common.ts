export const sectionTypes = {
	'2': 'travel', 
	'26': 'realty'
}


export const enumInsuranceCompanyId = {
	'renins': 1,
	'alpha': 2,
	'guideh': 3,
	'soglasie': 4,
	'absolut': 5,
	'tinkoff' : 6,
	'allianz' : 7,
	'liberty': 8,
	'ingos': 9,
	'vtb': 10,
	'rgs': 11,
	'zetta': 12,
	'uralsib': 13,
}

// список рисков для realty

export const enumRealtyListRiskId = {
	'fire': 19,
	'naturalDisasters': 20,
	'water': 21,
	'externalInfluence': 22,
	'crime': 23,
	'terrorism': 72
}


let listInsuranceCompany : any = [
	[1, { name: 'renins' }],
	[2, { name: 'alpha' }],
	[3, { name: 'guideh' }],
	[4, { name: 'soglasie' }],
	[5, { name: 'absolut' }],
	[6, { name: 'tinkoff' }],
	[7, { name: 'allianz' }],
	[8, { name: 'liberty' }],
	[9, { name: 'ingos' }],
	[10, { name: 'vtb' }],
	[11, { name: 'rgs' }],
	[12, { name: 'zetta' }],
	[13, { name: 'uralsib' }],
];

export const enumInsuranceCompany = new Map(listInsuranceCompany);





export interface ILabelValueModel {
	label : string;
	value : string;
}

export interface IIdNameValueModel {
	id : number;
	name : string;
	value : string;
}

/**
 * @desc модель варианта в массиве suggestions (например в комплите addresComplete)
 */
export interface ISuggestionModel {
	data: any;
	unrestricted_value: string;
	value: string;
}