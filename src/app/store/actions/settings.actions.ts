import { ActionModule } from '../../lib/ActionModule';
import { api } from '../api/Api';
import { history } from '../../app';


import * as parseUrl from 'parse-url';
import { sectionTypes } from '../logic/common';


export class SettingsActions extends ActionModule {
	public types = {
		SETTINGS_PARTNER_CONFIGURATION_SET: 'SETTINGS_PARTNER_CONFIGURATION_SET'
	}

	/**
	 * 
	 * 
	 * @private
	 * @param {string} url 
	 * @memberof SettingsActions
	 */
	private handleParentUrl = (url: string) => dispatch => {
		/**
		 * в parseUrl падает непонятная ошибка поэтому пока что в trycatch 
		 */
		console.log('url', url)
		try {
			const urlModel = parseUrl(__DEV__ ? url.replace('/#/', '/') : url);
			Object.keys(urlModel.query)
				.map(key => {
					urlModel.query[key.replace('amp;', '')] = urlModel.query[key];
				});
			const listRequiredQueryField = ['quotationGuid', 'result'];
			const isHasNeededForRedirectFields = listRequiredQueryField.every(field => field in urlModel.query);
			if (isHasNeededForRedirectFields) {
				const { quotationGuid, result } = urlModel.query;
				// const redirectUrl = Configuration.url(`${sectionTypes[type]}/${result}?orderId=${orderId}&result=${result}`);
				const redirectUrl = frameManager.url(`travel/response?quotationGuid=${quotationGuid}&result=${result}`);
				history.push(redirectUrl);
			}
		} catch (err) {
			// if (__DEV__) console.log(err);
		}
	}
	/**
	 * 
	 * @memberof SettingsActions
	 */
	public getPartnerId = () => dispatch => {
		// return api.shared.getPartnerId(frameManager.parentUrl)
		// 	.then(response => {
		// 		const params = response.data.param ? JSON.parse(response.data.param) : null;
		// 		dispatch(this.createPayload(this.types.SETTINGS_PARTNER_CONFIGURATION_SET, { ...response.data, param: params }));
		// 		const section = frameManager.setupParams(frameManager.parentUrl, params);
		// 		console.log('section', section)
		// 		// хендлим настройки
		// 		if (!section) {
		// 			history.push(frameManager.url('iframe-error'));
		// 		} else if (Array.isArray(section)) {
		// 			history.push(frameManager.url('iframe-landing'));
		// 		} else {
		// 			history.push(frameManager.url(`${section}/insurance`));
		// 		}

		// 		// хендлим внешний урл чтобы заслать на decline / success оплаты
		// 		this.handleParentUrl(frameManager.parentUrl);
		// 	}).catch((err) => { this.handleParentUrl(frameManager.parentUrl); console.log(err) });
		dispatch(this.handleParentUrl(frameManager.parentUrl))
	}

	public getProjectInfo = () => dispatch => {
		console.log(frameManager.partnerGuid, 'guid')
		return api.shared.getProjectInfo()
			.then(response => {
				if (response.data.name != 'TuravIns') {
					history.push('/unavailable');
				} else {
					dispatch(this.handleParentUrl(frameManager.parentUrl))
				}
			}).catch((err) => { console.log(err) });
	}
}


export const settingsActions = new SettingsActions();




