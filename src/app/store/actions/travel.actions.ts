import { IResultCalculateModel, getDefaultInsurerModel } from '../logic/travel/interfaces/ITravelModel';
import { ITravelRiskModel } from '../logic/travel/interfaces/ITravelListModel';
import { travelMockGet, travelMockListModelGet } from '../../routes/travel/mock-get';
import { IControlEvent } from '../../lib/IControlEvent';
import { api } from '../api/Api';
import { ActionModule } from '../../lib/ActionModule';
import { history } from '../../app';
import { IRootState } from '../reducers/index';
import * as moment from 'moment';
import { enumInsuranceCompany } from '../logic/common';



export class TravelActions extends ActionModule {

	public types = {
		TRAVEL_GET_SUCCESS: 'TRAVEL_GET_SUCCESS',
		TRAVEL_DICTIONARY_GET_SUCCESS: 'TRAVEL_DICTIONARY_GET_SUCCESS',
		TRAVEL_MODEL_SET: 'TRAVEL_MODEL_SET',
		TRAVEL_LIST_GET_SUCCESS: 'TRAVEL_LIST_GET_SUCCESS',
		TRAVEL_LIST_GET_FAILURE: 'TRAVEL_LIST_GET_FAILURE',
		TRAVEL_TOURIST_ADD: 'TRAVEL_TOURIST_ADD',
		TRAVEL_TOURIST_REMOVE: 'TRAVEL_TOURIST_REMOVE',
		TRAVEL_TOURIST_AGE_SET: 'TRAVEL_TOURIST_AGE_SET',
		TRAVEL_COUNTRY_SET: 'TRAVEL_COUNTRY_SET',
		TRAVEL_ADD_THAILAND: 'TRAVEL_ADD_THAILAND',
		TRAVEL_REMOVE_THAILAND: 'TRAVEL_REMOVE_THAILAND',
		TRAVEL_ADD_RISK: 'TRAVEL_ADD_RISK',
		TRAVEL_REMOVE_RISK: 'TRAVEL_REMOVE_RISK',
		TRAVEL_CHANGE_RISK_COVERAGE: 'TRAVEL_CHANGE_RISK_COVERAGE',
		TRAVEL_START_LIST_RESULT_CALCULATE: 'TRAVEL_START_LIST_RESULT_CALCULATE',
		TRAVEL__RESULT_CALCULATE_SUCCESS: 'TRAVEL__RESULT_CALCULATE_SUCCESS',
		TRAVEL__RESULT_CALCULATE_FAILURE: 'TRAVEL__RESULT_CALCULATE_FAILURE',
		TRAVEL_STATE_SET: 'TRAVEL_STATE_SET',
		TRAVEL_ORDER_MODEL_SET: 'TRAVEL_ORDER_MODEL_SET',
		TRAVEL_INSURER_SET: 'TRAVEL_INSURER_SET',
		TRAVEL_TOURIST_MODEL_SET: 'TRAVEL_TOURIST_MODEL_SET',
		TRAVEL_ORDER_ADD_TOURIST: 'TRAVEL_ORDER_ADD_TOURIST',
		TRAVEL_ORDER_REMOVE_TOURIST: 'TRAVEL_ORDER_REMOVE_TOURIST',
		TRAVEL_RECALCULATE_ORDER_MODEL_FAILURE: 'TRAVEL_RECALCULATE_ORDER_MODEL_FAILURE',
		TRAVEL_ORDER_ADD_RISK: 'TRAVEL_ORDER_ADD_RISK',
		TRAVEL_ORDER_REMOVE_RISK: 'TRAVEL_ORDER_REMOVE_RISK',
		TRAVEL_ORDER_CHANGE_RISK_COVERAGE: 'TRAVEL_ORDER_CHANGE_RISK_COVERAGE',
		TRAVEL_RECALCULATE_LOADING: 'TRAVEL_RECALCULATE_LOADING',
		TRAVEL_RECALCULATE_ORDER_MODEL_SUCCESS: 'TRAVEL_RECALCULATE_ORDER_MODEL_SUCCESS',
		TRAVEL_POLICY_GET_INFO: 'TRAVEL_POLICY_GET_INFO',
		TRAVEL_PAYMENT_INFO_SET: 'TRAVEL_PAYMENT_INFO_SET',
		TRAVEL_POLICY_SET_EMAIL: 'TRAVEL_POLICY_SET_EMAIL',
		TRAVEL_POLICY_PRINT_SUCCESS: 'TRAVEL_POLICY_PRINT_SUCCESS',
		TRAVEL_CONVERT_POLICY_INFO_TO_ORDER: 'TRAVEL_CONVERT_POLICY_INFO_TO_ORDER',
		TRAVEL_ORDER_INIT_LIST_CHOOSED_BEFORE_LIST_RISK: 'TRAVEL_ORDER_INIT_LIST_CHOOSED_BEFORE_LIST_RISK',
		TRAVEL__RESULT_CALCULATE_ABORTED: 'TRAVEL__RESULT_CALCULATE_ABORTED',
		TRAVEL_RESET_LOADING: 'TRAVEL_RESET_LOADING',
		TRAVEL_PAYMENT_LOADING: 'TRAVEL_PAYMENT_LOADING',
		TRAVEL_PAYMENT_FAILURE: 'TRAVEL_PAYMENT_FAILURE',
		TRAVEL_SET_MODELS_FROM_CACHE: 'TRAVEL_SET_MODELS_FROM_CACHE',
		TRAVEL_SET_INDEX_TOURIST_IS_BUYER: 'TRAVEL_SET_INDEX_TOURIST_IS_BUYER',
		TRAVEL_CONVERT_POLICY_INFO_TO_INSURANCE: 'TRAVEL_CONVERT_POLICY_INFO_TO_INSURANCE',
		TRAVEL_POLICY_ACCEPT_SUCCESS: 'TRAVEL_POLICY_ACCEPT_SUCCESS'
	}

	public modelGet = () => (dispatch, getState) => {
		api.travel.modelGet()
			.then(response => {
				dispatch(this.createPayload(this.types.TRAVEL_GET_SUCCESS, response.data));
			})
			.catch(error => { console.log(error) });
	}

	public listModelGet = () => (dispatch, getState) => {
		api.travel.listModelGet()
			.then(response => {
				dispatch(this.createPayload(this.types.TRAVEL_LIST_GET_SUCCESS, response.data));
			})
			.catch(error => { console.log(error) });
	}

	public set = (event) => this.createPayload(this.types.TRAVEL_MODEL_SET, event);

	public touristAdd = () => this.createPayload(this.types.TRAVEL_TOURIST_ADD);
	public touristAgeSet = (index: number, value: number) => this.createPayload(this.types.TRAVEL_TOURIST_AGE_SET, { index, value });
	public touristRemove = (index: number) => this.createPayload(this.types.TRAVEL_TOURIST_REMOVE, { index });

	public countrySet = (event) => this.createPayload(this.types.TRAVEL_COUNTRY_SET, event);

	public travelAddThailand = () => this.createPayload(this.types.TRAVEL_ADD_THAILAND);
	public travelRemoveThailand = () => this.createPayload(this.types.TRAVEL_REMOVE_THAILAND);

	public addRisk = (payload: { riskModel: ITravelRiskModel }) => this.createPayload(this.types.TRAVEL_ADD_RISK, payload);

	public removeRisk = (payload: { id: number }) => this.createPayload(this.types.TRAVEL_REMOVE_RISK, payload);

	public changeRiskCoverage = (payload: { id: number, coverageSum: number }) => this.createPayload(this.types.TRAVEL_CHANGE_RISK_COVERAGE, payload);

	public calculateListResult = (calculateProgressHandler) => async (dispatch, getState) => {
		let loadedInsuranceCount = 0;
		let { model, dictionaryModel } = getState().travel;
		
		dispatch(this.createPayload(this.types.TRAVEL_START_LIST_RESULT_CALCULATE));

		//нужно для того, чтобы бекенд подставил нормальные токены для запросов к страховым.
		// api.travel.listModelGet();

		dictionaryModel.listInsuranceCompany.forEach(({ id }) => {
			api.travel.quotationCalculate({ ...model, insuranceCompanyId: id }, (enumInsuranceCompany.get(id) as any).name).then((response) => {
				loadedInsuranceCount += 1;
				calculateProgressHandler(loadedInsuranceCount);
				dispatch(this.createPayload(this.types.TRAVEL__RESULT_CALCULATE_SUCCESS, response.data));
			}).catch(response => {
				if (response) {
					if (response.resultText == 'aborted') {
						dispatch(this.createPayload(this.types.TRAVEL__RESULT_CALCULATE_ABORTED));
					} else {
						loadedInsuranceCount += 1;
						calculateProgressHandler(loadedInsuranceCount);
						dispatch(this.createPayload(this.types.TRAVEL__RESULT_CALCULATE_FAILURE, { id }));
					}
				}
			})
		})
	}

	public issuePolicy = (resultCalculateModel: IResultCalculateModel) => this.createPayload(this.types.TRAVEL_ORDER_MODEL_SET, resultCalculateModel );
	public travelInsurerSet = (event) => this.createPayload(this.types.TRAVEL_INSURER_SET, event);

	public touristOrderModelSet = (event) => this.createPayload(this.types.TRAVEL_TOURIST_MODEL_SET, event);

	public travelOrderAddTourist = () => this.createPayload(this.types.TRAVEL_ORDER_ADD_TOURIST, event);

	public travelOrderRemoveTourist = (index) => this.createPayload(this.types.TRAVEL_ORDER_REMOVE_TOURIST, index);

	public addOrderModelRisk = (payload: { riskModel: ITravelRiskModel }) => this.createPayload(this.types.TRAVEL_ORDER_ADD_RISK, payload);

	public removeOrderModelRisk = (payload: { id: number }) => this.createPayload(this.types.TRAVEL_ORDER_REMOVE_RISK, payload);

	public changeOrderRiskCoverage = (payload: { id: number, coverageSum: number }) => this.createPayload(this.types.TRAVEL_ORDER_CHANGE_RISK_COVERAGE, payload);

	public travelRecalculate = (orderModel) => async (dispatch) => {
		const { insuranceCompanyId } = orderModel;

		dispatch(this.createPayload(this.types.TRAVEL_RECALCULATE_LOADING));

		api.travel.quotationCalculate(orderModel, (enumInsuranceCompany.get(insuranceCompanyId) as any).name).then((response) => {
			dispatch(this.createPayload(this.types.TRAVEL_RECALCULATE_ORDER_MODEL_SUCCESS, response.data));
		}).catch(error => {
			dispatch(this.createPayload(this.types.TRAVEL_RECALCULATE_ORDER_MODEL_FAILURE, { data: error.data, resultText: error.resultText }));
		})
	}

	public policySave = (finishLoading) => async (dispatch, getState) => {
	// 	let createResponse;
	// 	const { url, query } = frameManager.separateNotPaymentRedirectParams(frameManager.parentUrl);
		try {
			dispatch(this.createPayload(this.types.TRAVEL_PAYMENT_LOADING));
			let response = await api.travel.policySave({...getState().travel.orderModel.calculationModel, returnUrl: frameManager.parentUrl, partnerGuid: frameManager.partnerGuid});
			// сохраняем в кэш модели для расчета и модель заявки для пользователей, возвращающихся с кнопки "назад"
			localStorage.setItem('orderModel', JSON.stringify({ 
				model: getState().travel.orderModel, 
				indexTouristIsBuyer: getState().travel.indexTouristIsBuyer,
				isRussianPassport: getState().travel.orderInfo.isRussianPassport
			}));
			localStorage.setItem('model', JSON.stringify({ model: getState().travel.model}));
			if (parent != self) parent.postMessage({ type: 'TURAV_INS_REDIRECT', payload: { url: response.data } }, '*');
			else location.href = response.data;
			//перенаправляем на ресурс эквайринга.
			// location.href = response.data;
		} catch (error) {
			console.log(error)
			dispatch(this.createPayload(this.types.TRAVEL_PAYMENT_FAILURE, { data: error.data, resultText: error.resultText }));
		}
	// 	try {
	// 		const calculateResponse = await api.travel.policyPaymentAdd(createResponse.data, url, query);
	// 		if (parent != self) parent.postMessage({ type: 'STRAHOVKI_24_REDIRECT', payload: { url: calculateResponse.data } }, '*');
	// 		else location.href = calculateResponse.data;
	// 		finishLoading(true);
	// 	} catch(err) {
	// 		dispatch(this.createPayload(this.types.TRAVEL_RECALCULATE_LOADING));
	// 		finishLoading(false);
	// 	}
	}

	public travelPolicyInformationGet = (quotationGuid) => dispatch => {
		dispatch(this.policyAccept(quotationGuid))
		api.travel.policyInformationGet(quotationGuid)
			.then(response => dispatch(this.createPayload(this.types.TRAVEL_POLICY_GET_INFO, response)))
	}

	public travelOrderInfoSet = (event : { name, value }) => this.createPayload(this.types.TRAVEL_PAYMENT_INFO_SET, event);
	
	public travelPolicyDownload = (quotationGuid) => dispatch => {
		api.travel.policyDownload(quotationGuid)
	}

	public travelPolicySetEmail = (event) => this.createPayload(this.types.TRAVEL_POLICY_SET_EMAIL, event);

	public policyPrint = (quotationGuid, email) => dispatch => {
		api.travel.policyPrint(quotationGuid, email)
			.then(response => dispatch(this.createPayload(this.types.TRAVEL_POLICY_PRINT_SUCCESS, {})))
	}

	/**
	 * @description
	 * 1) делает запрос policyInformationInfo
	 * 2) создает из неё модель orderModel
	 * 3) вызывает коллбек по окончанию отработки всех действий.
	 */
	public convertPolicyInformation = ({ policyData, callback }) => async(dispatch, getState) => {
		await api.travel.listModelGet().then(response => {
			dispatch(this.createPayload(this.types.TRAVEL_LIST_GET_SUCCESS, response.data));
		})
		api.travel.policyInformationGet(policyData.quotationGuid).then(response => {
			//policyData - данные которые взяты из url при редиректе реквайрингом на наш сервис.
			//policyInformation - просто данные из метода policyInformationGet
			dispatch(this.createPayload(this.types.TRAVEL_CONVERT_POLICY_INFO_TO_ORDER, { policyData, policyInformation: response.data }));
			callback();
		});
	}

	/**
	 * @desc инициализирует список ранее не выбраных рисков, основываясь на модели orderModel.
	 */
	public initChoosedBeforeListRisk = () => this.createPayload(this.types.TRAVEL_ORDER_INIT_LIST_CHOOSED_BEFORE_LIST_RISK);

	/**
	 * @desc обработка переходов между шагами	 
	 * @param targetStep - номер шага, на который осуществляется переход.
	 * @param prevStep - номер текущего шага, с которого осуществляется переход. Нужен для того, 
	 * чтобы обрабатывать более кастомные ситуаици перехода. (Отличать переход с 1 шага на 2, от перехода с 3 шага на 2)
	 **/
	public navigate = (targetStep : number, prevStep: number = -1) => (dispatch, getState) => {


		switch(targetStep) {
			case 1: {
				history.push(frameManager.url('travel/insurance'));
			}

			case 2: {

				//если переход осуществляется с 3 шага, копируем orderModel.calculationModel в обычную модель заполнения для 1 и 2 шагов.
				if(prevStep == 3) {
					// сброс сообщения об ошибке
					dispatch(this.createPayload(this.types.TRAVEL_PAYMENT_INFO_SET, { name: 'isPaymentError', value: false }));
					let calculationModel = {
						...getState().travel.orderModel.calculationModel,
						//  не передаем в запросы на 2 шаге данные туристов, введенные на 3 шаге
						insured: getState().travel.orderModel.calculationModel.insured.map(item => {
							return {
								...item,
								firstName: null,
								lastName: null,
								documentNo: null,
								documentSeries: null
							}
						}),
						//  не передаем в запросы на 2 шаге данные покупателя, введенные на 3 шаге
						insurer: getDefaultInsurerModel(),
						quotationGuid: null
					};
					let mergeModelAction = this.createPayload(this.types.TRAVEL_STATE_SET, { name: 'model', value: calculationModel });
					dispatch(mergeModelAction);
				}

				history.push(frameManager.url('travel/results'));
				break;
			}

			case 3: {
				history.push(frameManager.url('travel/order'));
			}
		}
	}

	public resetLoading = () => this.createPayload(this.types.TRAVEL_RESET_LOADING);

	// модели из кеша для пользователей, которые нажали кнопку "назад", находясь на шаге оплаты

	public setModelsFromCache = () => (dispatch) => {
		let orderModel = JSON.parse(localStorage.getItem('orderModel'));
		let model = JSON.parse(localStorage.getItem('model')).model;
		// проставляем в модель для запроса к страховым актуальные возраста застрахованных, введенные ранее на 3 шаге
		model = {
			...model,
			insured: orderModel.model.calculationModel.insured.map((item, index) => {
				return {
					...item,
					firstName: null,
					lastName: null,
					documentNo: null,
					documentSeries: null
				}
			})
		}
		dispatch(this.createPayload(this.types.TRAVEL_SET_MODELS_FROM_CACHE, { orderModel: orderModel.model, model: model, indexTouristIsBuyer: orderModel.indexTouristIsBuyer, isRussianPassport: orderModel.isRussianPassport }));
	}

	/** @desc запоминаем индекс туриста, который является покупателем
	 */
	public setIndexTouristIsBuyer = (index: number) => this.createPayload(this.types.TRAVEL_SET_INDEX_TOURIST_IS_BUYER, index);

	/** @desc переход с 4 шага на 1 по кнопке "Создать новый расчет"
	 */
	public createNewInsurance = () => this.createPayload(this.types.TRAVEL_CONVERT_POLICY_INFO_TO_INSURANCE);
	
	/** @desc скачать пользовательское соглашение
	 */
	public getUserAgreement = (quotationGuid, firstName, lastName) => () => {
		api.travel.termsOfService(quotationGuid, firstName, lastName).then((response) => {
		}).catch(error => {
			console.log(error)
		});
	}

	public policyAccept = (quotationGuid) => async (dispatch) => {
		try {
			let response = await api.travel.policyAccept(quotationGuid);
			dispatch(this.createPayload(this.types.TRAVEL_POLICY_ACCEPT_SUCCESS, response.data));
		} catch (error) {
			
		}
	}

	public insuranceRules = (quotationGuid) => () => {
		api.travel.insuranceRules(quotationGuid).then((response) => {
		}).catch(error => {
			console.log(error)
		});
	}

	public insurancePolicySample = (quotationGuid) => () => {
		api.travel.insurancePolicySample(quotationGuid).then((response) => {
		}).catch(error => {
			console.log(error)
		});
	}

}




export const travelActions = new TravelActions();



