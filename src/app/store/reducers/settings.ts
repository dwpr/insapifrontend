import { settingsActions } from '../actions/settings.actions';
import { IPartnerConfiguration } from '../logic/configuration/interface/PartnerConfiguration';





export interface ISettingsState {
	partnerId : string | number;
	isLoaded: boolean;
	params : IPartnerConfiguration;
}

export const getDefaultSettingsState = () : ISettingsState => ({
	partnerId : 0,
	isLoaded: false,
	params: null
});

export const settingsReducer = (state = getDefaultSettingsState(), action: any) : ISettingsState => {

	switch (action.type) {
		case settingsActions.types.SETTINGS_PARTNER_CONFIGURATION_SET: {
			return {
				...state,
				isLoaded: true,
				params: action.payload.param,
				partnerId: action.payload.id
			};
		}
		default:
			return state;
	}

}