import { ITravelState, travelReducer } from './travel/travel';
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { settingsReducer, ISettingsState } from './settings';
export interface IRootState {
	travel : ITravelState;
	settings: ISettingsState;
}


export const rootReducer : (state : IRootState, action : any) => IRootState = combineReducers({
	routing: routerReducer,
	travel: travelReducer,
	settings: settingsReducer,
});
