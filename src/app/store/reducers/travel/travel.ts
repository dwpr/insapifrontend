import { quotationCalculateMock } from '../../../routes/travel/mock-get';
import { travelActions } from '../../actions/travel.actions';
import { IRiskModel, ITravelModel, IResultCalculateModel, IOrderModel, getDefaultInsurerModel } from '../../logic/travel/interfaces/ITravelModel';
import { ITravelListModel } from '../../logic/travel/interfaces/ITravelListModel';
import { IPolicyModel } from '../../logic/travel/interfaces/IPolicyModel';
 



export const getLoadingResultCalculateModel = (insuranceCompanyId: number, name: string) => {
	let resultCalculateModel : IResultCalculateModel = {
		calculationModel:null,
		price: 0,
		specialCondition: [],
		isLoading: true,
		isSuccess: false,
		name,
		insuranceCompanyId,
		assistance: null,
		docLink: null
	}

	return resultCalculateModel;
}
export interface ITravelState {
	model: ITravelModel,
	dictionaryModel: ITravelListModel,
	isLoaded: boolean,
	listResultCalculateModel: IResultCalculateModel[],
	orderModel: IOrderModel;
	policyModel: IPolicyModel,
	orderInfo: { quotationGuid: string, isPaymentError: boolean, isRussianPassport: boolean },
	/**
	 * @desc список рисков, которые были выбраны до инициализации 3 шага (/travel/order)
	 * по этому списку фильтруются риски, для того чтобы рекомендовать не выбранные риски на 3 шаге.
	 */
	listChoosedBeforeRisk: IRiskModel[];
	isRequestsAborted: boolean;
	isRecalculated: boolean;
	showWarningMessage: boolean;
	indexTouristIsBuyer: number;
	isPolicyAccepted: boolean;
	listPolicy: any[];
}

export const getDefaultTravelState = () : ITravelState => ({
	model: null,
	dictionaryModel: null,
	isLoaded: null,
	listResultCalculateModel: [],
	orderModel: quotationCalculateMock().data,
	policyModel: null,
	orderInfo: { quotationGuid: '', isPaymentError: false, isRussianPassport: false },
	listChoosedBeforeRisk: [],
	isRequestsAborted: false,
	isRecalculated: true,
	showWarningMessage: false,
	indexTouristIsBuyer: -1,
	isPolicyAccepted: false,
	listPolicy: []
});

export const travelReducer = (state = getDefaultTravelState(), action: any) : ITravelState => {
	let { type, payload } = action;

	switch (action.type) {

		case travelActions.types.TRAVEL_GET_SUCCESS: {

			/**
			 * @desc инициализируем массив risk в нужный вид.
			 */
			let risk = [{ id: 1, coverageSum: 50000 }]

			return {
				...state,
				model: { 
					...action.payload,
					risk,
					isMultiple: Boolean(action.payload.isMultiple),
					currencyId: 2,
					// проставляем null, ибо не можем сразу показывать пользователю приходящий 0, а вводить он его может
					insured: action.payload.insured.map(item => {
						return item = {...item, age: null}
					})
				},
				isLoaded: true
			}
		}
		case travelActions.types.TRAVEL_LIST_GET_SUCCESS: {
			return {
				...state,
				dictionaryModel: action.payload
			}
		}
		case travelActions.types.TRAVEL_MODEL_SET: {
			return {
				...state,
				model: {
					...state.model,
					[action.payload.name]:action.payload.value
				}
			}
		}

		case travelActions.types.TRAVEL_DICTIONARY_GET_SUCCESS: {
			return {
				...state,
				dictionaryModel: {
					...state.dictionaryModel,
					...action.payload
				},
			}
		}
		case travelActions.types.TRAVEL_TOURIST_ADD: {
			return {
				...state,
				model: {
					...state.model,
					insured: state.model.insured.concat({
						age: null,
						birthDate: null,
						lastName: null,
						firstName: null,
						documentSeries: null,
						documentNo: null,
						policyNo: null
					})
				}
			}
		}
		case travelActions.types.TRAVEL_TOURIST_REMOVE: {
			return {
				...state,
				model: {
					...state.model,
					insured: state.model.insured.filter((item, index) => {
						return index != action.payload.index;
					})
				}
			}
		}
		//проставляем возраст туриста и вместе с тем затираем его дату рождения. (Согласовано с димой)
		case travelActions.types.TRAVEL_TOURIST_AGE_SET: {			
			return {
				...state,
				model: {
					...state.model,
					insured: state.model.insured.map((item, index) => {
						if (index != action.payload.index) return item;
						return {
							...state.model.insured[index],
							age: action.payload.value,
							birthDate: ''
						}
					})
				}
			}
		}
		case travelActions.types.TRAVEL_COUNTRY_SET: {
			let model = state.model;
			let { listCountry } = state.dictionaryModel;
			let country = payload.value;
			country = country.map(item => item == 1000 ? '163' : item );
			//если какая то из стран в dictionaryModel совпадает со странами из listCountryId, то пушим эту страну в listSelectedValue	

			// let listSelectedCountry = listCountry.filter(countryItem => country.find(chosenCountry => chosenCountry == countryItem.id));

			let listSelectedCountry = country.map(item => {
				return listCountry.find(countryItem => countryItem.id == item)
			})
			return {
				...state,
				model: {
					...state.model,
					country: listSelectedCountry
				}
			}
		}

		case travelActions.types.TRAVEL_ADD_THAILAND: {
			return {
				...state,
				dictionaryModel: {
					...state.dictionaryModel,
					listCountry: state.dictionaryModel.listCountry.concat({ id: 1000, name: 'Тайланд', isHighlighted: false, isShengen: false })
				}
			}
		}
		case travelActions.types.TRAVEL_REMOVE_THAILAND: {
			return {
				...state,
				dictionaryModel: {
					...state.dictionaryModel,
					listCountry: state.dictionaryModel.listCountry.slice(0, state.dictionaryModel.listCountry.length - 2)
				}
			}
		}
		case travelActions.types.TRAVEL_ADD_RISK: {
			let { riskModel } = action.payload;

			let riskModelNew : IRiskModel = {
				id: riskModel.id,
				coverageSum: riskModel.listCoverageSum[0]
			}

			return {
				...state,
				model: { 
					...state.model,
					risk: [
						...state.model.risk,
						riskModelNew
					]
				}
			}
		}

		case travelActions.types.TRAVEL_REMOVE_RISK: {
			let { id } = action.payload;

			let listRisk = state.model.risk.filter(( riskModel: IRiskModel ) => riskModel.id != id);

			return {
				...state,
				model: { 
					...state.model,
					risk: listRisk
				}
			}
		}
		case travelActions.types.TRAVEL_ORDER_ADD_RISK: {
			let { riskModel } = action.payload;

			let riskModelNew : IRiskModel = {
				id: riskModel.id,
				coverageSum: riskModel.listCoverageSum[0],
				name: riskModel.name
			}

			return {
				...state,
				orderModel: { 
					...state.orderModel,
					calculationModel: {
						...state.orderModel.calculationModel,
						risk: [
							...state.orderModel.calculationModel.risk,
							riskModelNew
						]
					}
				}
			}
		}

		case travelActions.types.TRAVEL_ORDER_REMOVE_RISK: {
			return {
				...state,
				orderModel: { 
					...state.orderModel,
					calculationModel: {
						...state.orderModel.calculationModel,
						risk: state.orderModel.calculationModel.risk.filter(( riskModel: IRiskModel ) => riskModel.id != action.payload.id)
					}
				}
			}
		}

		case travelActions.types.TRAVEL_ORDER_CHANGE_RISK_COVERAGE: {

			return {
				...state,
				orderModel: { 
					...state.orderModel,
					calculationModel: {
						...state.orderModel.calculationModel,
						risk: state.orderModel.calculationModel.risk.map(item => {
							if (item.id != action.payload.id) return item;
							return {
								...item,
								coverageSum: action.payload.coverageSum
							}
						})
					}
				}
			}
		}

		case travelActions.types.TRAVEL_CHANGE_RISK_COVERAGE: {
			let { id, coverageSum } = action.payload;

			let listRisk = state.model.risk.map(( riskModel: IRiskModel ) => {
				if(riskModel.id == id) return { id, coverageSum }
				
				return riskModel
			})

			return {
				...state,
				model: { 
					...state.model,
					risk: listRisk
				}
			}
		}

		case travelActions.types.TRAVEL_START_LIST_RESULT_CALCULATE: {
			let { listInsuranceCompany } = state.dictionaryModel;
			
			let listResultCalculateModel : IResultCalculateModel[];
			listResultCalculateModel = listInsuranceCompany.map(({ id, name }) => getLoadingResultCalculateModel(id, name));

			return {
				...state,
				listResultCalculateModel
			}
		}

		case travelActions.types.TRAVEL_INSURER_SET: {
			return {
				...state,
				orderModel: {
					...state.orderModel,
					calculationModel: {
						...state.orderModel.calculationModel,
						insurer: {
							...state.orderModel.calculationModel.insurer,
							[action.payload.name]: action.payload.value
						}
					}
				}
			}
		}

		case travelActions.types.TRAVEL_TOURIST_MODEL_SET: {
			return {
				...state,
				orderModel: {
					...state.orderModel,
					calculationModel: {
						...state.orderModel.calculationModel,
						insured: state.orderModel.calculationModel.insured.map((item, touristIndex) => {
							if (action.payload.index != touristIndex) return item;
							return {
								...item,
								[action.payload.name]: action.payload.value
							}
						})
					}
				}
			}
		}
		
		case travelActions.types.TRAVEL_ORDER_ADD_TOURIST: {
			return {
				...state,
				orderModel: {
					...state.orderModel,
					calculationModel: {
						...state.orderModel.calculationModel,
						insured: state.orderModel.calculationModel.insured.concat({
							age: 30,
							birthDate: null,
							lastName: null,
							firstName: null,
							documentSeries: null,
							documentNo: null,
							policyNo: null
						})
					}
				}
			}
		}

		case travelActions.types.TRAVEL_ORDER_REMOVE_TOURIST: {
			return {
				...state,
				orderModel: {
					...state.orderModel,
					calculationModel: {
						...state.orderModel.calculationModel,
						insured: state.orderModel.calculationModel.insured.filter((item, index) => {
							if (index != action.payload) return item;
						})
					}
				}
			}
		}

		case travelActions.types.TRAVEL_RECALCULATE_LOADING: {
			return {
				...state,
				// isLoaded: false,
				isRecalculated: false,
				showWarningMessage: false
			}
		}

		case travelActions.types.TRAVEL_PAYMENT_LOADING: {
			return {
				...state,
				isLoaded: false
			}
		}

		case travelActions.types.TRAVEL_PAYMENT_FAILURE: {
			return {
				...state,
				isLoaded: true,
				showWarningMessage: payload.resultText == '523'
			}
		}

		case travelActions.types.TRAVEL_RECALCULATE_ORDER_MODEL_SUCCESS: {
			return {
				...state,
				// isLoaded: true,
				isRecalculated: true,
				orderModel: {
					...state.orderModel,
					price: action.payload.price,
					specialCondition: action.payload.specialCondition,
					listTravelRisk: action.payload.listTravelRisk
				}
			}
		}
		case travelActions.types.TRAVEL_RECALCULATE_ORDER_MODEL_FAILURE: {
			// resultText == 'aborted' говорит о том, что запрос прерван, потому что пошел другой запрос, а значит флаг isRecalculated снимать не нужно
			let isRecalculated = payload.resultText == 'aborted' ? state.isRecalculated : true;
			return {
				...state,
				// isLoaded: true,
				isRecalculated,
				showWarningMessage: payload.resultText == '523'
			}
		}

		case travelActions.types.TRAVEL__RESULT_CALCULATE_SUCCESS: {
			let resultData = action.payload;

			let listResultCalculateModelNew = state.listResultCalculateModel.map(model => {
				if(model.insuranceCompanyId == resultData.calculationModel.insuranceCompanyId) return {
					...model,
					...resultData,
					isSuccess: true,
					isLoading: false
				}

				return model;
			})


			return {
				...state,
				listResultCalculateModel:listResultCalculateModelNew
			}
		}

		case travelActions.types.TRAVEL__RESULT_CALCULATE_FAILURE: {
			let { id } = action.payload;
			let listResultCalculateModelNew = state.listResultCalculateModel.map(model => {
				if(model.insuranceCompanyId == id) return {
					...model,
					isSuccess: false,
					isLoading: false
				}

				return model;
			})

			return {
				...state,
				listResultCalculateModel: listResultCalculateModelNew
			}
		}

		case travelActions.types.TRAVEL__RESULT_CALCULATE_ABORTED: {
			return {
				...state,
				isRequestsAborted: true
			}
		}

		case travelActions.types.TRAVEL_RESET_LOADING: {
			return {
				...state,
				isRequestsAborted: false
			}
		}		

		case travelActions.types.TRAVEL_STATE_SET: {
			let { name, value } = action.payload;

			return {
				...state,
				[name]: value
			}
		}

		case travelActions.types.TRAVEL_PAYMENT_INFO_SET: {
			let { name, value } = action.payload;

			return {
				...state,
				orderInfo: {
					...state.orderInfo,
					[name]: value
				}
				
			}
		}

		case travelActions.types.TRAVEL_ORDER_MODEL_SET: {
			let resultCalculateModel : IResultCalculateModel = action.payload;
			let { price, specialCondition, listTravelRisk, calculationModel, docLink } = resultCalculateModel;
			let orderModel: IOrderModel = { 
				calculationModel: {
					...calculationModel,
					insured: calculationModel.insured.map((item, index) => {
						// берем сохраненные данные из старой orderModel, если такой путешественник уже существует в orderModel(не был добавлен на 1 шаге) и если таковые данные имеются (переход с 3 шага на 2 и потом обратно на 3)
						if (state.orderModel.calculationModel.insured[index]) {
							return {
								...state.orderModel.calculationModel.insured[index],
								age: item.age,
								birthDate: item.birthDate ? item.birthDate : ''
							};
						} else {
							return {
								...item
							}
						}
					}),
					insurer: state.orderModel.calculationModel.insurer
				},
				price, 
				specialCondition, 
				listTravelRisk,
				docLink
			};	

			return {
				...state,
				orderModel,
				showWarningMessage: false
			}
		}
		
		case travelActions.types.TRAVEL_CONVERT_POLICY_INFO_TO_ORDER: {
			let { policyData, policyInformation } = payload;

			let { price, specialCondition, listTravelRisk, insuranceCompany,
				  startDate, endDate, isMultiple, currencyId, coveredDay, insurer, country,
				  insured, risk } = policyInformation;

			let { insuranceRulesLink, samplePolicyLink } = insuranceCompany;
			let { quotationGuid } = policyData;

			let isRussianPassport = false;

			if (insurer.documentSeries.length == 4) {
				isRussianPassport = true;
			}
			
			let orderModel : IOrderModel = {
				calculationModel: {
					quotationGuid,
					insuranceCompanyId: insuranceCompany.id,
					startDate, 
					endDate, 
					isMultiple, 
					currencyId, 
					coveredDay,
					insurer, 
					country,
					insured, 
					risk
				},
				price,
				specialCondition,
				listTravelRisk,
				docLink: {
					insuranceRulesLink,
					samplePolicyLink
				}
			}

			return {
				...state,
				orderModel,
				orderInfo: {
					...state.orderInfo,
					isRussianPassport
				},
				isLoaded: true
			}
		}

		case travelActions.types.TRAVEL_POLICY_GET_INFO: {
			return {
				...state,
				policyModel: action.payload.data,
				isLoaded: true
			}
		}

		case travelActions.types.TRAVEL_POLICY_SET_EMAIL: {
			return {
				...state, 
				policyModel: {
					...state.policyModel,
					insurer: {
						...state.policyModel.insurer,
						[payload.name]: payload.value
					}
				}
			}
		}		

		case travelActions.types.TRAVEL_ORDER_INIT_LIST_CHOOSED_BEFORE_LIST_RISK: {
			let { orderModel } = state;

			let listChoosedBeforeRisk = [...orderModel.calculationModel.risk];

			return {
				...state, 
				listChoosedBeforeRisk
			}
		}		

		case travelActions.types.TRAVEL_POLICY_PRINT_SUCCESS: {
			return {
				...state,
			}
		}

		case travelActions.types.TRAVEL_SET_MODELS_FROM_CACHE: {
			return {
				...state,
				model: action.payload.model,
				orderModel: action.payload.orderModel,
				orderInfo: {
					...state.orderInfo,
					isRussianPassport: action.payload.isRussianPassport
				},
				isLoaded: true,
				indexTouristIsBuyer: action.payload.indexTouristIsBuyer
			}
		}

		case travelActions.types.TRAVEL_SET_INDEX_TOURIST_IS_BUYER: {
			return {
				...state,
				indexTouristIsBuyer: action.payload
			}
		}

		case travelActions.types.TRAVEL_CONVERT_POLICY_INFO_TO_INSURANCE: {
			let { policyModel } = state;
			let { country, startDate, endDate, isMultiple, coveredDay, insurer, insured, risk, currencyId } = policyModel;

			let model : ITravelModel = {
				quotationGuid: null,
				insuranceCompanyId: null,
				startDate,
				endDate,
				isMultiple,
				coveredDay,
				currencyId,
				insurer: getDefaultInsurerModel(),
				insured: insured.map(item => ({
					...item,
					firstName: null,
					lastName: null,
					documentNo: null,
					documentSeries: null
				})),
				country,
				risk
			}

			let isRussianPassport = false;

			if (insurer.documentSeries.length == 4) {
				isRussianPassport = true;
			}
			
			let orderModel : IOrderModel = {
				calculationModel: {
					quotationGuid: null,
					insuranceCompanyId: null,
					startDate, 
					endDate, 
					isMultiple, 
					currencyId, 
					coveredDay,
					insurer, 
					country,
					insured, 
					risk: []
				},
				price: null,
				specialCondition: [],
				listTravelRisk: [],
				docLink: {
					insuranceRulesLink: '',
					samplePolicyLink: ''
				}
			}

			return {
				...state,
				model,
				orderModel,
				orderInfo: {
					...state.orderInfo,
					isRussianPassport
				},
				isLoaded: true
			}
		}

		case travelActions.types.TRAVEL_POLICY_ACCEPT_SUCCESS: {
			return {
				...state,
				isPolicyAccepted: true
			}
		}

		default:
			return state;
	}

}



















