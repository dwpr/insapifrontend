import { TravelApi } from './TravelApi';
import { SharedApi } from './SharedApi';



export class Api {

	/**
	 * Контроллер travel
	 * @memberof Api
	 */
	public travel: TravelApi = new TravelApi();

	/**
	 * Shared controller
	 * @type {SharedApi}
	 * @memberof Api
	 */
	public shared : SharedApi = new SharedApi();	
}


export const api = new Api();
