import { ApiModule } from '../api/ApiModule';
import { ITravelListModel } from '../logic/travel/interfaces/ITravelListModel';
import { ITravelModel } from '../logic/travel/interfaces/ITravelModel';








export class TravelApi extends ApiModule {
	public name = '/Travel';

	public modelGet = () => this.ajax<ITravelModel>('/modelGet');
	public listModelGet = () => this.ajax<ITravelListModel>('/listModelGet');

	public minimalInsuranceSumGet = (listCountryId) => { return this.ajax('/minimalInsuranceSumGet', { listId: listCountryId.join(',') }) };

	public policySave = (model) => this.ajax('/policySave', { json: JSON.stringify(model) });
	// public orderSave = (model) => this.ajax('/orderSave', { json: JSON.stringify(model) });

	// public insuranceSingleQuoteGet = (insuranceCompanyId, json) => this.ajax('/insuranceSingleQuoteGet', { insuranceCompanyId, json });

	// public policyPrintInfo = (orderGuid) => this.ajax('/policyPrintInfo', { orderGuid });

	// public policyPaymentAdd = (orderId, redirectUrl = '', iFrameTail = '') => this.ajax('/policyPaymentAdd', { orderId, redirectUrl, iFrameTail });
	
	public quotationCalculate = (model: ITravelModel, insuranceCompanyName : string) => this.ajaxXHR('/quotationCalculate', { json:JSON.stringify(model) }, insuranceCompanyName);

	public policyDownload = (quotationGuid) => this.ajaxDownloadFile('/policyDownload', { quotationGuid }, false);

	public policyInformationGet = (quotationGuid) => this.ajax('/policyInformationGet', { quotationGuid });

	public policyPrint = (quotationGuid, email) => this.ajax('/policyPrint', { quotationGuid, email });

	public termsOfService = (quotationGuid, insurerFirstName, insurerLastName) => this.ajaxDownloadFile('/termsOfService', { quotationGuid, insurerFirstName, insurerLastName });

	public policyAccept = (quotationGuid) => this.ajax('/policyAccept', { quotationGuid });
	
	public insuranceRules = (quotationGuid) => this.ajaxDownloadFile('/insuranceRules', { quotationGuid });

	public insurancePolicySample = (quotationGuid) => this.ajaxDownloadFile('/insurancePolicySample', { quotationGuid });
}