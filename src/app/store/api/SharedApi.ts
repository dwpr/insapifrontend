import { ApiModule } from "./ApiModule";






export class SharedApi extends ApiModule {
	/**
	 * Shared controller
	 */
	protected name = '';
	/**
	 * Get partner id by url
	 * @memberof SharedApi
	 */
	public getPartnerId = (address : string) => this.ajax('/Shared/getPartnerId', {address});

	public getProjectInfo = () => this.ajax('/_getProjectInfo', {});
}