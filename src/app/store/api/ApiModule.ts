import { IApiResponse } from '../../lib/IApiResponse';
import axios from 'axios';



export interface IAjaxOptions {
	files?: File[];
	isUseFileNameAsKey?: boolean;
	method?: string;
}



export class ApiModule {
	protected name = '';
	/**
	 * @type {string}
	 * @memberof ApiModule
	 */
	protected baseUrl: string = '';
	/**
	 * Creates an instance of ApiModule.
	 * @memberof ApiModule
	 */
	constructor() {
		if (__DEV__) this.baseUrl = __LOCAL__ ? 'http://localhost:1549' : `http://turavins${__TEST__ ?'-test.dwpr' : '-dev.dwpr'}.ru`;
		else this.baseUrl = '';
	}
	/**
	 * Переводим объект в формат application/x-www-form-urlencoded
	 * @memberof ApiModule
	 */
	protected encodeToURI = (target) => Object.keys(target)
		.map(key => encodeURIComponent(key) + '=' + encodeURIComponent(target[key]))
		.join('&');

	protected ajaxDownloadFile = (url, params = {}, isOpenInNewWindow = true) => {
		let body: any = this.encodeToURI(params);
		let headers = {
			'Content-type': 'application/x-www-form-urlencoded'
		}
		return new Promise((resolve, reject) => {
			const form = document.createElement('form');
			form.action = this.baseUrl + this.name + url;
			form.method = 'POST';
			if (isOpenInNewWindow) {
				form.target = '_blank';
			}
			form.style.display = 'none';
			const data = this.encodeToURI(params);
			data.split('&')
				.map(item => {
					const pair = item.split('=');
					const input = document.createElement('input');
					input.type = 'hidden';
					input.name = pair[0];
					input.value = pair[1];
					form.appendChild(input);
				});
			document.body.appendChild(form);
			form.submit();
			form.remove();
			resolve();
		});
		// return new Promise((resolve, reject) => {
		// 	axios({
		// 		url: this.baseUrl + this.name + url,
		// 		data: body,
		// 		method: 'POST',
		// 		responseType: 'blob',
		// 		withCredentials: true,
		// 		headers
		// 	}).then(response => {
		// 		// const url = window.URL.createObjectURL(new Blob([response.data], { type: 'application/pdf' }));
		// 		// const link = document.createElement('a');
		// 		// link.href = url;
		// 		// link.target = '_blank';
		// 		// // link.setAttribute('download', 'file.pdf');
		// 		// document.body.appendChild(link);
		// 		// link.click();
		// 		let form = document.createElement('form');
		// 		form.action = ''
		// 		var form = $('<form></form>').attr('action', url).attr('method', 'post');
		// 		// Add the one key/value
		// 		form.append($("<input></input>").attr('type', 'hidden').attr('name', key).attr('value', data));
		// 		//send request
		// 		form.appendTo('body').submit().remove();
			
		// 	}).catch(error => {
		// 		reject({
		// 			isOk: false,
		// 			resultText: error.resultText || error.message,
		// 			data: {}
		// 		});
		// 	});
		// })
	}

	listInsuranceCompanyXHR = {
		'guideh': null,
		'uralsib': null,
		'soglasie': null,
		'absolut': null,
		'vtb': null,
		'renins': null,
		'liberty': null,
		'alpha': null,
		'ingos': null,
		'rgs': null,
		'allianz' : null,
		'tinkoff' : null
	}

	protected ajaxXHR = <R = any>(url, params = {}, insuranceCompanyName : string = '') : Promise<IApiResponse<R>> => {
		if (insuranceCompanyName && this.listInsuranceCompanyXHR[insuranceCompanyName]) {
			this.listInsuranceCompanyXHR[insuranceCompanyName].onreadystatechange = () => this.listInsuranceCompanyXHR[insuranceCompanyName].reject({ isOk: false, resultText: 'aborted' });
			this.listInsuranceCompanyXHR[insuranceCompanyName].abort();
		}
		return new Promise((resolve, reject) => {
            try {
                let xhr = new XMLHttpRequest();

                xhr.open('POST', this.baseUrl + this.name + url, true); // serialize data
                xhr.withCredentials = true;
                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhr.onreadystatechange = () => {
                    if (xhr.readyState > 3) {
                        let response;
                        try {
							response = JSON.parse(xhr.responseText);
                            response.isOk ? resolve(response) : reject(response);
                        } catch (err) {
                            reject(response);
                        }
                    } else {

					}
				}
				xhr.send(this.encodeToURI(params));
				this.listInsuranceCompanyXHR[insuranceCompanyName] = xhr;
				this.listInsuranceCompanyXHR[insuranceCompanyName].reject = reject;
            } catch (err) {
                reject({
					isOk: false,
					resultText: err.resultText || err.message,
					data: {}
				});
            }
        });
	}

	/**
	 * @memberof ApiModule
	 */
	protected ajax = <R = any>(url, params = {}, options: IAjaxOptions = { files: undefined, method: 'POST', isUseFileNameAsKey: false }): Promise<IApiResponse<R>> => {
		let body: any = this.encodeToURI(params);
		let headers = {
			'Content-type': 'application/x-www-form-urlencoded'
		}
		if (options.files){
			delete headers['Content-type'];
			body = new FormData();
			Object.keys(params).map(key => body.append(key, params[key]));
			Array.from(options.files).map((file: File, index) => {
				const key = options.isUseFileNameAsKey ? file.name : `file${index}`;
				body.append(key, file);
			});
		}

		return new Promise((resolve, reject) => {
			axios({
				url: this.baseUrl + this.name + url,
				data: body,
				method: options.method || 'POST',
				withCredentials: true,
				headers, 
				
			}).then(response => {
				if (response.data.isOk) resolve(response.data);
				else reject(response.data);
			}).catch(error => {
				reject({
					isOk: false,
					resultText: error.resultText || error.message,
					data: {}
				});
			});

		});
	
	}
}