import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import { applyMiddleware, createStore } from 'redux';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import { rootReducer } from './reducers';



export const configureStore = (history) => {

	const store = createStore(rootReducer, composeWithDevTools(
		applyMiddleware(
			thunk, 
			routerMiddleware(history),
		)
	));

	if (module.hot) {
		module.hot.accept('./reducers', () => {
			store.replaceReducer(require('./reducers').rootReducer);
		});
	}

	return store;
}


