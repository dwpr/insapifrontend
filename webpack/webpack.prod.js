const webpack = require('webpack');
const config = require('./webpack.base');
const merge = require('webpack-merge');
const path = require('path');
// plugins
const Visualizer = require('webpack-visualizer-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const { root } = require('./webpack');

const {
	getStyleLoaders
} = require('./webpack.js');


const uglifyJs = () => new webpack.optimize.UglifyJsPlugin({
	output: {
		comments: false
	},
	minimize: true,
	sourceMap: false
});

const prodConfig = {
	devtool: false,
	module: {
		rules: [{
			test: /(\.css|\.scss)/,
			exclude: /node_modules/,
			use: ExtractTextPlugin.extract({
				fallback: "style-loader",
				use: getStyleLoaders(true)
			})
		}]
	},
	plugins: [
		// uglifyJs(),
		new webpack.optimize.UglifyJsPlugin({
			output: {
				comments: false
			},
			drop_console: true,
			minimize: true,
			sourceMap: false
		}),
		new ExtractTextPlugin('style.css'),
		new CleanWebpackPlugin(['dist'], {
			root: process.cwd()
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			minChunks: (module, count) => {
				return (
					module.resource &&
					/\.js$/.test(module.resource) &&
					module.resource.indexOf(
						path.join(__dirname, '../node_modules')
					) === 0
				)
			}
		}),
		// new Visualizer()
	]
}


const iframeConfig = {
	entry: './widget/iframe.ts',
	output: {
		path: root('widget/dist'),
		filename: 'strahovki-integration.js',
		publicPath: '/',
		devtoolModuleFilenameTemplate: '[absolute-resource-path]',
		devtoolFallbackModuleFilenameTemplate: '[absolute-resource-path]?[hash]'
	},
	resolve: {
		extensions: ['.ts', '.css', '.scss', '.tsx', '.js', '.json', '.pug', '.png', '.svg', '.jpg', '.*']
	},
	module: {
		rules: [
			{
				test: /\.(ts|tsx)$/,
				use: ['awesome-typescript-loader'],
				include: root('widget'),
				// exclude: /node_modules/
			}
		]
	},
	plugins: [
		// uglifyJs()
		new webpack.optimize.UglifyJsPlugin({
			output: {
				comments: false
			},
			drop_console: true,
			minimize: true,
			sourceMap: false
		}),
	],
	watch: true
};



module.exports = merge(config, prodConfig);

