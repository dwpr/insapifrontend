const webpack = require('webpack');
const {
	root
} = require('./webpack');

const SimpleProgressPlugin = require('webpack-simple-progress-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
// const SimpleProgressPlugin = require('webpack-simple-progress-plugin');

const iframeConfig = {
	entry: './widget/iframe.ts',
	output: {
		path: root('widget/dist'),
		filename: 'iframe.js',
		publicPath: '/',
		devtoolModuleFilenameTemplate: '[absolute-resource-path]',
		devtoolFallbackModuleFilenameTemplate: '[absolute-resource-path]?[hash]'
	},
	resolve: {
		extensions: ['.ts', '.css', '.scss', '.tsx', '.js', '.json', '.pug', '.png', '.svg', '.jpg', '.*']
	},
	module: {
		rules: [{
			test: /\.(ts|tsx)$/,
			use: ['awesome-typescript-loader'],
			include: root('widget'),
		}]
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
			'PROD': JSON.stringify(process.env.PROD),
			'TEST': JSON.stringify(process.env.TEST)
		}),
		new webpack.optimize.UglifyJsPlugin({
			output: {
				comments: false
			},
			drop_console: true,
			minimize: true,
			sourceMap: false
		}),
		new FriendlyErrorsWebpackPlugin(),
		new SimpleProgressPlugin({
			progressOptions: {
				clear: true
			}
		})
	],
	watch: true
};



module.exports = iframeConfig;