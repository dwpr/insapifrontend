const webpack = require('webpack');
const config = require('./webpack.base');
const merge = require('webpack-merge');
const {
	getStyleLoaders
} = require('./webpack.js');

const devConfig = {
	devtool: 'source-map',
	module: {
		rules: [{
	        test: /\.(css|scss)$/,
			// exclude: /node_modules/,
			use: [
				'style-loader'
			].concat(getStyleLoaders())
		}]
	}
};







module.exports = merge(config, devConfig);