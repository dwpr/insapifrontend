const webpack = require('webpack');
const path = require('path');
const chalk = require('chalk');

// plugins 
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const SimpleProgressPlugin = require('webpack-simple-progress-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');


const {
    root
} = require('./webpack.js');


const isProduction = process.env.NODE_ENV === 'production';


const config = {};


config.stats = 'none';


config.entry = [
    root('src/app/vendor.ts'),
    root('src/app/app.tsx')
];


config.output = {
    path: root('dist'),
    filename: 'js/[name].js',
    publicPath: '/',
    devtoolModuleFilenameTemplate: '[absolute-resource-path]',
    devtoolFallbackModuleFilenameTemplate: '[absolute-resource-path]?[hash]'
};


config.resolve = {
    alias: {
        'img': root('src/public/img'),
        'fonts': root('src/public/fonts'),
        styles: root('src/styles'),
        components: root('src/app/components'),
        routes: root('src/app/routes')
    },
    modules: [
        root('src/app'),
        'node_modules'
    ],
    extensions: ['.ts', '.css', '.scss', '.tsx', '.js', '.json', '.pug', '.png', '.svg', '.jpg', '.*']
};



config.module = {};


const imageLoader = (dir = '') => ({
    test: /\.(jpe?g|png|gif|svg)$/i,
    use: [{
            loader: 'file-loader',
            options: {
                name: `${dir}img/[name].[ext]`,
            }
        },
        {
            loader: 'image-webpack-loader',
            options: {
                pngquant: {
                    quality: '90',
                    speed: 4
                },
                mozjpeg: {
                    progressive: true,
                    quality: 70
                },
                svgo: {

                }
            }
        }
    ]
});

const fontsLoader = (dir = '') => ({
    test: /\.(woff|woff2|otf|eot|ttf)(\?[a-z0-9=.]+)?$/,
    use: [{
        loader: 'file-loader',
        options: {
            name: `${dir}fonts/[name].[ext]`
        }
    }]
});
config.module.rules = [{
        test: /\.(ts|tsx)$/,
        use: ['react-hot-loader/webpack', 'awesome-typescript-loader'],
        include: root('src'),
        exclude: /node_modules/
    },
    {
        test: /\.hbs$/,
        use: ['handlebars-loader']
    },
    // imageLoader(isProduction  ? 'iframe-integration/' : ''),
    // fontsLoader(isProduction  ? 'iframe-integration/' : ''),
    // fontsLoader(),
];

if (isProduction) {
    
    config.module.rules.push(
        // imageLoader('iframe-integration/' ),
        imageLoader(),
        fontsLoader('iframe-integration/' ),
        fontsLoader( )
    );
} else {
    config.module.rules.push(
        fontsLoader( ),
        imageLoader()
    );
}


config.plugins = [
    new webpack.ProvidePlugin({
        React: 'react',
        ReactDOM: 'react-dom',
        Utils: [root('src/app/utils/index.ts'), 'default'],
        frameManager: [root('src/app/frameManager'), 'configuration']
    }),
    new HtmlWebpackPlugin({
        template: root('src/public/index.hbs'),
        filename: 'index.html',
        title: 'Страховой партнер',
        inject: false,
        css:isProduction ? [ 
            `${isProduction ? '/iframe-integration' : ''}/style.css`
            // `/style.css`
        ] : [],
        js: [
            `${isProduction ? '/iframe-integration/js' : './js'}/vendor.js`,
            `${isProduction ? '/iframe-integration/js' : './js'}/main.js`
            // 'js/vendor.js',
            // `js/main.js`
        ],
        minify: process.env.NODE_ENV === 'production' && {
            removeComments: true,
            collapseWhitespace: true,
            removeRedundantAttributes: true,
            useShortDoctype: true,
            removeEmptyAttributes: true,
            removeStyleLinkTypeAttributes: true,
            keepClosingSlash: true,
            minifyJS: true,
            minifyCSS: true,
            minifyURLs: true,
        },
        gtm: `${isProduction ? 'gtm_auth=q3ugx8xCjtRi-idnoK85kw&gtm_preview=env-2' : 'gtm_auth=vpgv-ztupAaQV6OMhffHQg&gtm_preview=env-21'}`,
        gtmFrame: `${isProduction ? 'https://www.googletagmanager.com/ns.html?id=GTM-K4G9L42&gtm_auth=q3ugx8xCjtRi-idnoK85kw&gtm_preview=env-2&gtm_cookies_win=x' : 'https://www.googletagmanager.com/ns.html?id=GTM-K4G9L42&gtm_auth=vpgv-ztupAaQV6OMhffHQg&gtm_preview=env-21&gtm_cookies_win=x'}`,
        dev: !isProduction
    }),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),
    new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        '__DEV__': process.env.__DEV__,
        '__LOCAL__': process.env.__LOCAL__,
        '__TEST__': process.env.__TEST__,
        'PROD': process.env.PROD,
        'TEST': process.env.TEST
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new FriendlyErrorsWebpackPlugin(),
    new SimpleProgressPlugin({
        progressOptions: {
            clear: true
        }
    })
]






module.exports = config;