const { root } = require('./webpack');
const webpack = require('webpack');


const iframeConfig = {
	entry: './widget/iframe.ts',
	output: {
		path: root('widget/dist'),
		filename: 'strahovki-integration.js',
		publicPath: '/',
		devtoolModuleFilenameTemplate: '[absolute-resource-path]',
		devtoolFallbackModuleFilenameTemplate: '[absolute-resource-path]?[hash]'
	},
	module: {
		rules: [
			{
				test: /\.(ts|tsx)$/,
				use: ['awesome-typescript-loader'],
				include: root('widget'),
			}
		]
	},
	plugins: [
		new webpack.optimize.UglifyJsPlugin({
			output: {
				comments: false
			},
			minimize: true,
			sourceMap: false
		})
	],
watch: true
};


module.exports = iframeConfig